# ----------------------------------------------------------------------------
# Copyright 2019 Regis Cattenoz, regis@ip666.org
#
# This file is part of pyrift : https://gitlab.com/regisc/pyrift
#
# pyrift is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pyrift is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pyrift.  If not, see <https://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------------
# File: pyrift_register.py
# Python Register InterFace Translation
# ----------------------------------------------------------------------------

from pyrift_field import PyriftBase, Field, Pad_Field, Cdc
from enum import Enum, auto
import itertools

# if available docx import already in registermap
# possible error is ModuleNotFoundError: if not already imported
try:
    from docx.shared import Cm
except ModuleNotFoundError:
    pass # if no docx import done already in registermap, not needed
         # (no word generation will take place)

# ----------------------------------------------------------------------------

class MapTableType(Enum):
    REG = auto()
    REG_MAP = auto()
    PRE_PAD = auto()
    POST_PAD = auto()
    ALIGN_PAD = auto()
    REG_ARRAY = auto()
    REG_MAP_ARRAY = auto()

class MapTableEntry():
    """
        Pseudo class use as data structure for map_table construction in reg_map
    """
    def __init__(self):
        self.type = None
        self.address = None
        self.register = None
        self.size = None # address occupation
        self.array_range = None
        self.array_index = None
    def __str__(self):
        return f"type={self.type}, @=[{self.address}:+{self.size}:{self.address+self.size}[ Arr={self.array_index}/{self.array_range}\n"
    def __repr__(self):
        return self.__str__()
    def is_first_of_array(self):
        # first of multi dimensional array have all index as 0 (is False)
        return not any(self.array_index)
    def get_array_linear_size(self):
        # Get 1 if no array, or product of all dimension (count of register in array)
        array_size = 1
        for dim_size in self.array_range:
            array_size = array_size * dim_size
        # print ("array_size", array_size)
        return array_size
    def get_suffix_array_name(self):
        # verilog signal name have a special suffix when part of array signal
        # print ("array_index", self.array_index)
        return "".join(f"_x{id}" for id in self.array_index)
    def get_array_range_definition(self):
        # verilog signal name instanciation have a range defined
        # return [3:0][1:0] for array_range==(4,2,)
        return "".join(f"[{id-1}:0]" for id in self.array_range)
    def get_array_c_range_definition(self):
        # verilog signal name instanciation have a range defined
        # return [4][2] for array_range==(4,2,)
        return "".join(f"[{id}]" for id in self.array_range)
    def get_array_index_bracket(self):
        # verilog signal name instanciation is an array in interface
        # return [2][1] for array_index==(2,1,)
        return "".join(f"[{id}]" for id in self.array_index)
    def get_all_register_array_entry(self):
        # given one register, find all array entry that it is part of
        if self.type != MapTableType.REG:
            raise  RuntimeError("Entry should be register, unexpected error !")
        all_reg_entry = [entry for entry in self.register.get_root_parent().map_table \
                         if entry.type==MapTableType.REG]
        return [entry for entry in all_reg_entry if entry.register == self.register]

    @staticmethod
    def static_array_linear_offset(array_range, array_index):
        # Get linear offset within multi dim array
        # example: range is (4,5,6) and index is (1,2,3)
        # offset is 1*(5*6) + 2*6 + 3
        offset = 0
        for dim_size,index in zip(array_range, array_index):
            offset *= dim_size
            offset += index
            # print (f"dim_size={dim_size} index={index} offset={offset}")
        # print ("offset", offset)
        return offset
    @staticmethod
    def static_array_linear_size(array_range):
        # Get 1 if no array, or product of all dimension (count of register in array)
        array_size = 1
        for dim_size in array_range:
            array_size = array_size * dim_size
        # print ("array_size", array_size)
        return array_size

# ----------------------------------------------------------------------------

class Register(PyriftBase):
    """
        Register is collection of Field
        To create register : constructor, then add Fields, finish by validate_check()
        field_list is the collection of usefull added fields
        pad_list is filled with all padding needed around field_list.
        pad_list is populated on validate_check
        (previous validate_check pad_list is clear, if validate_check called again)
    """

    def __init__(self, name=None, shortname=None, desc=None, array=None, ispresent=True):
        super(Register, self).__init__()
        if (isinstance(name, Field)):
            # name is provided as field
            # This is a shortcut for a register with single field,
            # using same name for register as the provided field
            # shortname of register is name of Field
            self.__init__(name=None, shortname=name.name, desc=desc, array=array, ispresent=ispresent)
            self.add(name) # add field provided as name to the constructing register
        else:
            # normal constructor
            if shortname:
                # shortname provided in __init__, make default name
                self.name = name if name else shortname
                self.shortname = shortname
            else:
                if name:
                    self.name = name
                else:
                    raise RuntimeError("Register constructor makes name or shortname mandatory !")
                self.shortname = None # shortcut for verilog naming (interface and rtl)
                                      # /!\warning, if used this limit to single instance of register
                                      # with this shortname in all regmap
                                      # (otherwise you get name collision)
            # if shortname is defined, this is a short alias for full register hierarchy
            # This is allowed only if no collision with other register instance can occur

            # description is to be provided either in constructor or later
            if desc:
                # print("setting description to ", desc)
                self.desc = desc
            else:
                # not desc from constructor, just keep empty string
                self.desc = ""
            self.array = array
            self.ispresent = ispresent

            # Main goal of register is to collect list of field
            self.field_list = []
            self.pad_list = []
            self.width = 32  # default bus size
            self.external = False  # no internal rtl implementation if external

            # map attribute : pad and align
            # Alignement is done before padding
            # - alignment padding
            # - pre_pad
            # - actual register
            # - post_pad
            self.pre_pad = 0
            self.post_pad = 0
            self.align = 4 # default for 32 bits architecture

            # parent is used when register is included in a parent container
            # allow check for single parent inclusion
            self.parent = None

    def add(self, field):
        if(field.parent is not None):
            # field is already owned by another parent register
            # field sharing by register is not supported
            raise RuntimeError("Adding field which already have a parent register: Sharing is not supported")
        # print(f"adding field {field.name} to {self.name} parent register.")
        field.parent = self

        if (not isinstance(field, Field)):
            raise RuntimeError("Unexpected field added : not a Field")
        self.field_list.append(field)

    def get_all_field(self):
        all_field_list = self.get_all_actual_field() + self.pad_list
        all_field_list.sort(key=lambda x: x.lsb_bit)
        return all_field_list

    def get_all_actual_field(self):
        all_field_list = [f for f in self.field_list if f.ispresent] # exclude self.pad_list and not present field
        all_field_list.sort(key=lambda x: x.lsb_bit)
        return all_field_list

    def get_field_by_name(self, name):
        for f in self.field_list: # exhaustive list, include not ispresent
            if name == f.name or name == f.shortname:
                return f
        return None

    def get_encode_list(self):
        return [field.encode for field in self.get_all_actual_field() if field.encode]

    def is_volatile(self):
        # register is considered volatile if any field is volatile
        # print(f"reg {self.name} is volatile : {[f.is_volatile() for f in self.get_all_field()]}")
        return any([f.is_volatile() for f in self.get_all_field()])

    def is_sw_read_only(self):
        # register is considered volatile if no field is writable
        # read only field is self.sw = Access.R (access.NA is not writable either (not legal ?))
        return all([not f.sw.is_writable() for f in self.get_all_field()])

    def get_interface_name(self):
        """
            register is part of a register hierarchy,
            Search first ancestor that have an interface_name
            (to be used for verilog interface connection)
            root parent may have several interface, so find out where it belong
        """
        parent = self # initial search point for interface_name is current register
        # print("IF search starting: for field",self.name, "p->", parent.name )
        while not getattr(parent, 'interface_name', None):
            if parent.parent:
                parent = parent.parent
            else:
                # still no interface name, and no more parent (reach root !!?)
                raise RuntimeError(f"unable to find interface name for register {self.name}")
            # print("IF search: ",parent.name)
        # print(f"Found interface {parent.interface_name}")
        return parent.interface_name

    def get_root_parent(self):
        parent = self
        while parent.parent:
            # progress through parent tree to root parent (parent is None)
            parent = parent.parent
        return parent

    def get_map_table(self, current_address, current_range, current_index):
        # map table is a list of register mapped at correct address for this register
        # list can contain multiple entry in case of array attribute on register, or pad or align
        map_table = []
        # ALIGN
        if(self.align != 0):
            #check align is multiple of 4 (only 32 bits architecture is supported)
            if self.align % 4 != 0:
                raise RuntimeError(
                    f"register is aligned with non multiple of 4 (align={self.align})\n"
                    f"Only 32 bit architecture is supported !")
            # get sure the current address is aligned
            if(current_address % self.align != 0):
                # mis-aligned, first re-align before
                aligned_address = current_address - current_address % self.align
                # then add one align step to re-align properly
                aligned_address += self.align
                # re-align from current_address to aligned_address
                entry = MapTableEntry()
                entry.type = MapTableType.ALIGN_PAD
                entry.address = current_address
                entry.register = self # relative register for pad
                entry.size = aligned_address - current_address
                entry.array_range = current_range
                entry.array_index = current_index
                map_table.append(entry)
                current_address += entry.size
        # Pre-PAD
        if(self.pre_pad != 0):
            #check align is multiple of 4 (only 32 bits architecture is supported)
            if self.pre_pad % 4 != 0:
                raise RuntimeError(
                    f"register with pad non multiple of 4 (pad={self.pre_pad})\n"
                    f"Only 32 bit architecture is supported")
            entry = MapTableEntry()
            entry.type = MapTableType.PRE_PAD
            entry.address = current_address
            entry.register = self # relative register for pad
            entry.size = self.pre_pad
            entry.array_range = current_range
            entry.array_index = current_index
            map_table.append(entry)
            # print(f"Adding pad entry @={current_address} size={entry.size}")
            current_address += entry.size
        # REGISTER entry
        if self.array:
            array_entry = MapTableEntry()
            array_entry.type = MapTableType.REG_ARRAY
            array_entry.register = self
            array_entry.address = current_address
            # array_entry.size = TBD after end of array map

            # if array is specified as integer, it is converted to a one dimension tuple in validate()
            if not type(self.array) is tuple:
                raise RuntimeError(f"array {self.array} expected to be of type tuple (not {type(self.array)}) (or converted from int for user)")
            if any([s < 2 for s in self.array]):
                raise RuntimeError(f"any array dimension is required larger than 2 : not OK for {self.array} of {self.name}")

            # prepare iteration (but only for current level of register (ignore upper layer regmap))
            reg_array_range_list = [range(i) for i in self.array]
            array_entry.reg_array_range = self.array
            array_entry.reg_array_index = (0,)*len(self.array)
            # print(f"array range=, {array_entry.reg_array_range}, array_index=, {array_entry.reg_array_index}")

            # REG_ARRAY entry have the full range of array, and (0,0,...) as array_index
            array_entry.array_range = tuple(current_range+array_entry.reg_array_range)
            array_entry.array_index = tuple(current_index+array_entry.reg_array_index)
            map_table.append(array_entry)

            # build linear index in 1d while browsing the multidim array
            # find start linear index in regmap[]xReg[]
            # base for linear depend on current_range;current_index if within an array of regmap
            linear_index = MapTableEntry.static_array_linear_size(self.array) * \
                           MapTableEntry.static_array_linear_offset(current_range, current_index)
            for array_index in itertools.product(*reg_array_range_list):
                # print("REGISTER array_index=", array_index)
                entry = MapTableEntry()
                entry.type = MapTableType.REG
                entry.address = current_address
                entry.register = self
                entry.size = 4
                entry.reg_array_range = array_entry.reg_array_range
                entry.reg_array_index = array_entry.reg_array_index
                entry.array_range = tuple(current_range+array_entry.reg_array_range)
                entry.array_index = tuple(current_index+array_index)
                map_table.append(entry)
                current_address += entry.size
                entry.linear_index = linear_index
                linear_index += 1
            # fix array_entry size (already inside map_table)
            array_entry.size = current_address - array_entry.address
        else:
            entry = MapTableEntry()
            entry.type = MapTableType.REG
            entry.address = current_address
            entry.register = self
            entry.size = 4
            entry.array_range = current_range
            entry.array_index = current_index
            map_table.append(entry)
            current_address += entry.size
        # Post-PAD
        if(self.post_pad != 0):
            #check align is multiple of 4 (only 32 bits architecture is supported)
            if self.post_pad % 4 != 0:
                raise RuntimeError(
                    f"register with pad non multiple of 4 (pad={self.post_pad})\n"
                    f"Only 32 bit architecture is supported")
            entry = MapTableEntry()
            entry.type = MapTableType.POST_PAD
            entry.address = current_address
            entry.register = self # relative register for pad
            entry.size = self.post_pad
            entry.array_range = current_range
            entry.array_index = current_index
            map_table.append(entry)
            current_address += entry.size
        #
        # print(f"(Reg.add : {self.name} {map_table})")
        return map_table, current_address

    def to_html(self, map_table_entry):
        html = ""
        html +=  "<style>table, th, td {border: 1px solid black;}</style>\n"
        html += f"<caption>Register name: {self.name}</caption><br>\n"
        html += f"<caption>Register fullname: {self.get_full_name()}</caption><br>\n"
        html += f"<caption>Register reset value: 0x{self.get_reset_value(map_table_entry):08X}</caption><br>\n"
        html += f"<caption>Reset valid mask: 0x{self.get_reset_valid_mask():08X}</caption><br>\n"
        # check for array with special non constant reset
        if map_table_entry.get_array_linear_size() > 1 and\
                any([f.reset and not isinstance(f.reset, int) for f in self.get_all_field()]):
            reset_table = [ f"0x{map_entry.register.get_reset_value(map_entry):x}" \
                            for map_entry in map_table_entry.get_all_register_array_entry()]
            html += f"<caption>Register array of reset value is : "
            html += ', '.join(reset_table)
            html += f"</caption><br>\n"

        reg_description = self.desc.replace("\r\n", "\n").replace("\n","<br>")
        if self.desc:
            # print("desc=", reg_description)
            html += f"Register description: {reg_description}\n"
        html +=  "<table>\n"
        html +=  "  <tr>\n"
        html +=  "    <th>Bit</th>\n"
        html +=  "    <th>Name</th>\n"
        html +=  "    <th>SW</th>\n"
        html +=  "    <th>HW</th>\n"
        html +=  "    <th>Reset</th>\n"
        html +=  "    <th>Store</th>\n"
        html +=  "    <th>Cdc</th>\n"
        html +=  "    <th>Desc</th>\n"
        html +=  "  </tr>\n"
        for f in self.get_all_field():
            html +=  "  <tr>\n"
            html += f"    <td>{f.bit_range_str()}</td>\n" # bit column in table
            html += f"    <td>{f.name}</td>\n" # Name column in table
            sw_option = ""
            if f.woclr: sw_option+=", woclr"
            if f.woset: sw_option+=", woset"
            if f.wot: sw_option+=", wot"
            if f.wzc: sw_option+=", wzc"
            if f.wzs: sw_option+=", wzs"
            if f.wzt: sw_option+=", wzt"
            if f.wclr: sw_option+=", wclr"
            if f.wset: sw_option+=", wset"
            if f.rclr: sw_option+=", rclr"
            if f.rset: sw_option+=", rset"
            html += f"    <td>{f.sw.name+sw_option}</td>\n" # SW column in table
            html += f"    <td>{f.hw.name}</td>\n" # HW column in table
            # Reset column in table
            if f.is_valid_reset_value():
                html += f"    <td>0x{f.get_reset_value(map_table_entry):0x}</td>\n"
            else:
                html += f"    <td>X</td>\n"
            html += f"    <td>{f.get_storage_type_str()}</td>\n" # Storage type for field
            cdc_option = []
            if f.cdc_in!=Cdc.NA: cdc_option.append(f"IN={f.cdc_in.name}")
            if f.cdc_out!=Cdc.NA: cdc_option.append(f"OUT={f.cdc_out.name}")
            cdc_option = ",<br>".join(cdc_option)

            html += f"    <td>{cdc_option}</td>\n" # HW column in table
            description = f.desc.replace("\r\n", "\n").replace("\n","<br>")
            if f.encode:
                enum_description = ""
                enum_description += f"{f.name} in using enum encoding: {f.encode.__name__}\n"
                if hasattr(f.encode, 'get_desc'):
                    enum_description += f.encode.get_desc()
                description += "<br>"
                description += enum_description.replace("\r\n", "\n").replace("\n","<br>")
            if map_table_entry.get_array_linear_size() > 1 and f.reset and not isinstance(f.reset, int):
                description += "<br>"
                description += "Array have reset value table :<br>"
                rst_str = ', '.join([f"0x{rst:0x}" for rst in f.reset])
                description += f"Reset : {rst_str}<br>"

            html += f"    <td>{description}</td>\n" # description column in table
            html += f"  </tr>\n"
        html += "</table>\n"
        return html

    def to_verilog(self, map_table_entry):
        """
            convert a register to it verilog implementation string
            Method of Register, it is provided a MapTableEntry to provide address, array
            (the extra information about current instanciation)
        """
        array_name_suffix = map_table_entry.get_suffix_array_name() # empty unless actual array in use
        reg_name = self.get_full_name()+array_name_suffix
        verilog = ""
        verilog += f"\n"
        verilog += f"// ##############################################################\n"
        verilog += f"// Register is : {self.name}, fullname={reg_name}\n"
        verilog += f"//    map@:{map_table_entry.address}, array={map_table_entry.array_index}/{map_table_entry.array_range}\n"
        verilog += f"// ##############################################################\n"
        verilog += f"\n"
        verilog += f"// Register {reg_name} control signals\n"
        verilog += f"// logic {reg_name}_r_swwr;\n"
        verilog += f"// logic {reg_name}_r_swrd;\n"
        verilog += f"// logic [31:0] {reg_name}_swrdata;\n"
        verilog += f"\n"
        verilog += f"// Register {reg_name} field implementation\n"
        for f in self.get_all_field():
            verilog += f.to_verilog(reg_name, map_table_entry)
        verilog += f"\n"
        verilog += f"// --------------------------------------------------------------\n"
        verilog += f"\n"
        verilog += f"// Register {reg_name} field aggregation for sw rdata\n"
        verilog += f"assign {reg_name}_swrdata = {{\n"
        field_name_list = [
            # get name from field (!! short_name can squeeze the reg name)
            f"{f.get_full_verilog_name(reg_name, array_name_suffix)}[{f.bit_width - 1}:0]"
            for f in self.get_all_field()] # sorted LSB first
        # aggregate starting MSB
        verilog += f"    " + ',\n    '.join(reversed(field_name_list)) + "\n"
        verilog += f"}};\n"
        verilog += f"\n"
        verilog += f"// --------------------------------------------------------------\n"
        verilog += f"\n"
        verilog += f"// Register {reg_name} ready aggregation for all field\n"
        verilog += f"assign {reg_name}_r_ready ="

        # register ready depend on field ready from each field
        # A field without used ready will have the signal created and tie to 1 in the field
        # So the register ready is just the logic and between all field
        # external field already have _extready demuxed for either read or write
        # A slow hw with hwce will have the hwce input already integrated in field ready signal
        field_rdy_list = [
            f"{f.get_full_verilog_name(self.name, array_name_suffix)}_ready"
            for f in self.get_all_actual_field()
        ]
        if len(field_rdy_list)==0:
            # normally at least one field per register, so report error
            raise  RuntimeError(f"Register {self.name} seems to have 0 field !?")
        verilog += f"\n    " + ' & \n    '.join(field_rdy_list) + ";\n"
        verilog += f"// --------------------------------------------------------------\n"
        verilog += f"\n"
        return verilog

    def validate_check(self):
        """
            Register expected to be fully defined now
            Run validation check before allowing any translation (html doc, rtl verilog)
            Add pad fields around actual useful fields
        """

        # normalize array : integer are converted to tuple
        # 3 =>(3,)
        if isinstance(self.array, int):
            self.array = (self.array,)

        # First check that no overlapping occurs between different fields
        covered_bit = set()
        for field in self.get_all_actual_field():
            # First check validaty of parameter setup at field level
            field.validate_check()

            field_bit = set(range(field.lsb_bit, field.lsb_bit + field.bit_width))
            # print("Field range is :", field_bit)
            # check non overlapping of this field
            if field_bit & covered_bit:
                # print(field_bit, covered_bit, field_bit & covered_bit)
                raise RuntimeError(
                    "Overlapping bit_range at field checking" +
                    str(field_bit) + "<>" + str(covered_bit) + " in reg: " + self.name)
            covered_bit = covered_bit | field_bit
        # print("Cover=", covered_bit)
        full_bit = set(range(0, self.width))
        pad_bit = full_bit - covered_bit
        following_pad_bit = set({i + 1 for i in pad_bit})
        # print("Pad=", pad_bit)
        # print("FollowingPad=", following_pad_bit)
        # A first pad bit of multi-bit padding is : a pad bit, after a non pad bit
        start_pad = sorted(pad_bit - following_pad_bit)
        # print("Pad start=", start_pad)
        # A last pad bit of multi-bit padding is : a pad bit, before a non pad bit
        end_pad = sorted({i - 1 for i in (following_pad_bit - pad_bit)})
        # print("Pad end=", end_pad)

        # cleanup existing pad_list (refresh by new call of validate_check)
        self.pad_list = []

        for pad_s, pad_e in zip(start_pad, end_pad):
            # print(f"PAD={pad_e}:{pad_s}")
            pad = Pad_Field(
                name=f"pad_{pad_e}_{pad_s}",
                bit_range=f"{pad_e}:{pad_s}"
            )
            pad.desc = "Pad bit (reserved, Read 0, write ignored)"
            self.pad_list.append(pad)

        # propagate the default hwce signal to each field if top regmap define it and
        # if not overloaded by field itself
        for field in self.get_all_actual_field():
            if field.hwce_signal_name != None:
                # this field have hwce clock enabling feature
                # locate the signal in root regmap to get signal attribute
                field.hwce_signal = self.parent.get_root_parent().get_signal_by_name(field.hwce_signal_name)
            else:
                # no explicit hwce signal for this field, use default reset
                field.hwce_signal = self.parent.get_root_parent().default_hwce_signal
            if field.cdc_out_signal_name != None:
                # this field have cdc clock enabling feature
                # locate the signal in root regmap to get signal attribute
                field.cdc_out_signal = self.parent.get_root_parent().get_signal_by_name(field.cdc_out_signal_name)
            if field.cdc_in_signal_name != None:
                # this field have cdc clock enabling feature
                # locate the signal in root regmap to get signal attribute
                field.cdc_in_signal = self.parent.get_root_parent().get_signal_by_name(field.cdc_in_signal_name)

    def get_full_name(self):
        """
            full name of register is hierarchical through all parent register map
            Get like: rm_[regmap_name1]_rm_[regmap_name2]_reg_[regname]
            if shortname is defined, then it replace the full name
        """
        if self.shortname is None:
            full_name = "reg_" + self.name
            parent = self.parent
            while (parent is not None):
                full_name = "rm_" + parent.name + "__" + full_name
                parent = parent.parent
        else:
            # full_name = "reg_" + self.shortname
            full_name = self.shortname
        return full_name

    def to_verilog_interface_by_name(self, if_name, array_range_definition, parent_regmap):
        """
            Generate the verilog interface of rif for the field of current register to rest of hw design
            Filter by interface name : if_name (check if at least one field is member of if_name)
            parent_regmap is root regmap, where to collect interface_io_list (for modport generation)
        """
        verilog = ""
        for f in self.get_all_actual_field():
            if f.get_interface_name() == if_name:
                verilog += f"      //  {f.name}\n"
                verilog += f.to_verilog_interface(
                    reg_name=self.get_full_name(),
                    array_range_definition=array_range_definition,
                    if_name=if_name,
                    parent_regmap=parent_regmap)

        return verilog

    def to_verilog_connect_interface(self, parent_regmap, map_table_entry):
        """
            Generate the verilog connection to interface of all field of current register
            parent register map passed for multipart resolution
        """
        verilog = ""
        for f in self.get_all_actual_field():
            verilog += f"      //  {f.name}\n"
            verilog += f.to_verilog_connect_interface(self.get_full_name(), parent_regmap, map_table_entry)

        return verilog

# ----------------------------------------------------------------------------

    def to_verilog_package(self, map_table_entry):
        """
            put constant definition about the register in verilog package
            (provide similar information as put inside cheader)
        """
        pkg = ""
        for f in self.get_all_actual_field(): # sorted LSB first, excluding PAD
            pkg += f"localparam {self.get_full_name().upper()}__"\
                     f"{f.get_name().upper()}__LSB = {f.lsb_bit};\n"
            pkg += f"localparam {self.get_full_name().upper()}__"\
                     f"{f.get_name().upper()}__MSB = {f.lsb_bit+f.bit_width-1};\n"
            pkg += f"localparam {self.get_full_name().upper()}__"\
                     f"{f.get_name().upper()}__BITWIDTH = {f.bit_width};\n"
            pkg += f"localparam {self.get_full_name().upper()}__"\
                     f"{f.get_name().upper()}__BITMASK = "\
                     f"32'h{((1<<f.bit_width)-1)<<f.lsb_bit:08x};\n"
            pkg += f"localparam {self.get_full_name().upper()}__"\
                     f"{f.get_name().upper()}__RESET = "\
                     f"32'h{f.get_reset_value(map_table_entry):x};\n"
            pkg += f"localparam {self.get_full_name().upper()}__"\
                     f"{f.get_name().upper()}__RESET_VALUE_IS_VALID = "\
                     f"{1 if f.is_valid_reset_value() else 0};\n"

        pkg += f"// Register reset value ...\n"
        pkg += f"localparam {self.get_full_name().upper()}__RESET = "\
                f"32'h{self.get_reset_value(map_table_entry):x};\n"
        pkg += f"localparam {self.get_full_name().upper()}__RESET_VALID_MASK = "\
                f"32'h{self.get_reset_valid_mask():x};\n"
        return pkg

# ----------------------------------------------------------------------------

    def to_chead(self, map_table_entry, with_define_constant=False, define_prefix=""):
        chead = ""
        chead += f"// --------------------------------------------------------------\n"
        chead += f"\n"
        chead += f"// register is volatile : {self.is_volatile()}\n"
        chead += f"// register is const : {self.is_sw_read_only()}\n"
        chead += f"typedef struct\n"
        chead += f"{{\n"
        for f in self.get_all_field(): # sorted LSB first
            chead += f"    unsigned int {f.get_name()}:{f.bit_width};\n"
        chead += f"}} TS_{self.get_root_parent().module_name}_Reg_{self.get_full_name()};\n"
        chead += f"\n"
        chead += f"typedef union\n"
        chead += f"{{\n"

        chead_volatile_option = "volatile " if self.is_volatile() else ""
        chead_const_option = "const " if self.is_sw_read_only() else ""
        modifier_option = chead_volatile_option + chead_const_option
        chead += f"    // Register is normally {'' if self.is_volatile() else 'non '}volatile\n"
        chead += f"    TS_{self.get_root_parent().module_name}_Reg_{self.get_full_name()} {modifier_option}field;\n" # map register to field structure
        chead += f"    unsigned long {modifier_option}reg;\n" # map register to exactly 32 bit by value
        chead += f"    // Alternate Register view : non volatile and volatile\n"
        # provide forced non volatile access to register
        chead += f"    TS_{self.get_root_parent().module_name}_Reg_{self.get_full_name()} {chead_const_option}field_nv;\n" # map register to field structure
        chead += f"    unsigned long {chead_const_option}reg_nv;\n" # map register to exactly 32 bit by value
        # non volatile register, provide a way to force access as volatile
        chead += f"    TS_{self.get_root_parent().module_name}_Reg_{self.get_full_name()} volatile {chead_const_option}field_v;\n" # map register to field structure
        chead += f"    unsigned long volatile {chead_const_option}reg_v;\n" # map register to exactly 32 bit by value
        #
        chead += f"}} TU_{self.get_root_parent().module_name}_Reg_{self.get_full_name()};\n"
        chead += f"\n"
        if with_define_constant:
            for f in self.get_all_actual_field(): # sorted LSB first, excluding PAD
                chead += f"#define {define_prefix}{self.get_full_name().upper()}__"\
                         f"{f.get_name().upper()}__LSB {f.lsb_bit}\n"
                chead += f"#define {define_prefix}{self.get_full_name().upper()}__"\
                         f"{f.get_name().upper()}__MSB {f.lsb_bit+f.bit_width-1}\n"
                chead += f"#define {define_prefix}{self.get_full_name().upper()}__"\
                         f"{f.get_name().upper()}__BITWIDTH {f.bit_width}\n"
                chead += f"#define {define_prefix}{self.get_full_name().upper()}__"\
                         f"{f.get_name().upper()}__BITMASK "\
                         f"0x{((1<<f.bit_width)-1)<<f.lsb_bit:08x}\n"
                chead += f"#define {define_prefix}{self.get_full_name().upper()}__"\
                         f"{f.get_name().upper()}__RESET "\
                         f"0x{f.get_reset_value(map_table_entry):x}\n"
                chead += f"#define {define_prefix}{self.get_full_name().upper()}__"\
                         f"{f.get_name().upper()}__RESET_VALUE_IS_VALID "\
                         f"{1 if f.is_valid_reset_value() else 0}\n"

            chead += f"// Register reset value ...\n"
            chead += f"#define {define_prefix}{self.get_full_name().upper()}__RESET "\
                    f"0x{self.get_reset_value(map_table_entry):x}\n"
            chead += f"#define {define_prefix}{self.get_full_name().upper()}__RESET_VALID_MASK "\
                    f"0x{self.get_reset_valid_mask():x}\n"

            chead += f"\n"
        return chead

    def to_chead_reset_detail(self, map_table_entry, define_prefix=""):
        chead = ""
        any_field_is_special = False

        for f in self.get_all_actual_field(): # sorted LSB first, excluding PAD
            if f.reset and not isinstance(f.reset, int):
                # each item of array with non constant reset value table is detailed here
                if map_table_entry.is_first_of_array():
                    # Array with custom per array item reset value, warning about special info added
                    chead += f"// array have different reset value for some item of array\n"
                    chead += f"// Provide table of reset value for each item\n"
                    chead += f"// previous ...__RESET is valid for first entry only\n"
                    chead += f"// Give constant for field and Register reset value of all array ...\n"
                chead += f"#define {define_prefix}{self.get_full_name().upper()}__"\
                         f"{f.get_name().upper()}__RESET_{map_table_entry.get_suffix_array_name().upper()} "\
                         f"0x{f.get_reset_value(map_table_entry):x}\n"
                any_field_is_special = True

        # display register value per register, but only if any field is special reset case
        if any_field_is_special:
            chead += f"#define {define_prefix}{self.get_full_name().upper()}__RESET"\
                        f"_{map_table_entry.get_suffix_array_name().upper()} "\
                        f"0x{self.get_reset_value(map_table_entry):x}\n"
        return chead

# ----------------------------------------------------------------------------

    def to_regmodel_uvm(self, map_table_entry):
        """
            Generate the register model in uvm (for rif verification)
        """
        regmodel = ""
        regmodel += f"\n"
        regmodel += f"class {self.get_full_name()}_reg extends uvm_reg;\n"
        regmodel += f"\n"
        regmodel += f"   `uvm_object_utils({self.get_full_name()}_reg)\n"
        regmodel += f"\n"

        regmodel += f"   function new (string name =\"{self.get_full_name()}_reg\");\n"
        regmodel += f"      super.new(name, 32, UVM_NO_COVERAGE);\n"
        regmodel += f"   endfunction: new\n"
        regmodel += f"\n"

        regmodel += f"   // Field declaration\n"
        for field in self.get_all_actual_field():
            field_name = field.get_full_verilog_name(self.get_full_name(), '') # TODO, array_name_suffix
            regmodel += f"   rand uvm_reg_field {field_name};\n"
        regmodel += f"\n"

        regmodel += f"   virtual function void build();\n"
        for field in self.get_all_actual_field():
            field_name = field.get_full_verilog_name(self.get_full_name(),'') # TODO, array_name_suffix
            regmodel += f"      {field_name} = uvm_reg_field::type_id::create(\"{field_name}\",,get_full_name());\n"
            regmodel += f"      {field_name}.configure(this,"
            regmodel += f" {field.bit_width}," # int unsigned size,
            regmodel += f" {field.lsb_bit}," # int unsigned lsb_pos,
            regmodel += f" \"{field.get_uvm_access()}\"," # string   access,
            regmodel += f" {1 if field.is_volatile() else 0}," # bit  volatile,
            regmodel += f" 'h{field.get_reset_value(map_table_entry):0x}," # uvm_reg_data_t reset,
            regmodel += f" {1 if field.reset else 0},"# // bit has_reset,
            regmodel += f"  1, 0);\n" # // is_rand, individually_accessible

        regmodel += f"\n"
        regmodel += f"   endfunction: build\n"

        regmodel += f"endclass : {self.get_full_name()}_reg\n"
        return regmodel

# ----------------------------------------------------------------------------

    def get_bit_reset_value(self, bit_id, map_table_entry):
        """
            A register is 32 bits, for bit_id in 31:0 find the field covering this bit
            and return the reset value associated
            (used in document_to_word to fill reset bit table)
            return a string, "-" if no field with reset value found
        """
        reset_value = "-"
        # Check if a field is addressing the bit
        for field in self.get_all_actual_field():
            if bit_id >= field.lsb_bit and bit_id < field.lsb_bit+field.bit_width:
                field_reset_value = field.get_reset_value(map_table_entry)
                reset_value = str((field_reset_value >> (bit_id - field.lsb_bit)) & 1)
                if not field.is_valid_reset_value():
                    # field as no valid reset value, the integer value 0 is trnasform to X
                    reset_value = "X"
        return reset_value

# ----------------------------------------------------------------------------

    def get_reset_value(self, map_table_entry):
        """
            A register is 32 bits, return the reset value of full register
            return an integer
            get value to read just after reset
        """
        reset_value = 0
        # only actual field contribute to register reset value
        # pad register are known to be 0 when reading register
        for f in self.get_all_actual_field():
            reset_value += f.get_reset_value(map_table_entry) << f.lsb_bit
        return reset_value

# ----------------------------------------------------------------------------

    def get_reset_valid_mask(self):
        """
            A register is 32 bits,
            return the reset valid mask to detect field without known reset value
            return an integer
        """
        reset_mask = 0xFFFFFFFF # will reset mask on invalid reset field
        # only actual field contribute to register reset mask
        # pad register are known to be 0 when reading register
        for f in self.get_all_actual_field():
            if not f.is_valid_reset_value():
                field_mask = ((1 << f.bit_width)-1) << f.lsb_bit
                reset_mask =  reset_mask & ~field_mask
        return reset_mask

# ----------------------------------------------------------------------------

    def document_to_word(self, document, map_table_entry):
        """
            Generate the register documentation in provided word document
            document is a python-docx already open document
            map_table_entry is the register in maptable (extra info : address, ...)
        """
        # print(f"Documenting register {self.name}")
        name_paragraph = document.add_paragraph(f"Register Name: {self.get_full_name()}")
        name_paragraph.style = document.styles['Pyrift_Reg_Header']
        document.add_paragraph(f"Address: 0x{map_table_entry.address:04x}")
        document.add_paragraph(f"Reset value:0x{self.get_reset_value(map_table_entry):08X}")
        document.add_paragraph(f"Reset valid mask:0x{self.get_reset_valid_mask():08X}")
        # reset value formatted as 32 bit bitfields
        table = document.add_table(rows=2, cols=33)
        for i in range(33):
            table.rows[0].cells[i].width=Cm(0.5)
            table.rows[1].cells[i].width=Cm(0.5)
        table.rows[0].cells[0].text="bit#"
        table.rows[1].cells[0].text="Reset"
        for i in range(32):
            table.rows[0].cells[1+i].text=str(31-i)
            table.rows[1].cells[1+i].text=self.get_bit_reset_value(31-i, map_table_entry)
            # cell margin can be customized here if needed with following (comment below)
            # table.rows[0].cells[1+i].paragraphs[0].paragraph_format.left_indent=Cm(-0.15)
            # table.rows[0].cells[1+i].paragraphs[0].paragraph_format.right_indent=Cm(-0.15)
        # print("par-fmt=",dir(table.rows[0].cells[0])) # .paragraphs[0].paragraph_format.left_indent))
        table.style = document.styles['Pyrift_Bitfield']
        document.add_paragraph(f"")
        #
        table = document.add_table(rows=1, cols=7)
        # table.columns[0].width=Cm(0.5) # column width not honored
        table.rows[0].cells[0].width=Cm(0.5)
        table.rows[0].cells[1].width=Cm(0.5)
        table.rows[0].cells[2].width=Cm(0.5)
        table.rows[0].cells[3].width=Cm(2.0)
        table.rows[0].cells[4].width=Cm(2.0)
        table.rows[0].cells[5].width=Cm(1.0)
        table.rows[0].cells[6].width=Cm(7.0)
        table.rows[0].cells[0].text = "Bit"
        table.rows[0].cells[1].text = "RW"
        table.rows[0].cells[2].text = "Field"
        table.rows[0].cells[3].text = "Value Id"
        table.rows[0].cells[4].text = "Value"
        table.rows[0].cells[5].text = "Reset"
        table.rows[0].cells[6].text = "Description"

        for field in self.get_all_actual_field():
            field_name = field.get_full_verilog_name(self.get_full_name(),'') # TODO, array_name_suffix
            row_cells = table.add_row().cells
            row_cells[0].width=Cm(0.5)
            row_cells[1].width=Cm(0.5)
            row_cells[2].width=Cm(0.5)
            row_cells[3].width=Cm(2.0)
            row_cells[4].width=Cm(2.0)
            row_cells[5].width=Cm(1.0)
            row_cells[6].width=Cm(7.0)
            row_cells[0].text = field.bit_range_str()
            row_cells[1].text = field.sw.name
            row_cells[2].text = field_name
            if field.encode:
                row_cells[3].text = f"{field.encode.__name__}:" +\
                                    "".join(["\n"+e.name for e in field.encode])
                row_cells[4].text = f":" +\
                                    "".join(["\n"+f"0x{e.value:0x}" for e in field.encode])
            else:
                row_cells[3].text = "-"
                row_cells[4].text = "-"
            if field.is_valid_reset_value():
                row_cells[5].text = f"0x{field.get_reset_value(map_table_entry):0x}"
            else:
                row_cells[5].text = "X"
            row_cells[6].text = field.desc

        table.style = document.styles['Pyrift_Register']

        document.add_paragraph("")

# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# Copyright 2019 Regis Cattenoz, regis@ip666.org
#
# This file is part of pyrift : https://gitlab.com/regisc/pyrift
#
# pyrift is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pyrift is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pyrift.  If not, see <https://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------------
# File: pyrift_registermap.py
# Python Register InterFace Translation
# ----------------------------------------------------------------------------

# import support for word file generation
# this a non default module in typical python install
# So handle error case if missing, just warn but continue
# (If user don't generate word file, no reason to stop on error)
# if import sucessed, then special feature will be included in
try:
    from docx import Document
    from docx.shared import Cm # unused import here, just to check for error in register import
    docx_import_error = "" # False is python-docx is installed and available
except ImportError:
    docx_import_error = \
        "Failed importing docx support to generate word file\n"\
        "word docx generation is not available"

from pyrift_field import PyriftBase, Field, Access, Cdc, FieldEncode
from pyrift_register import Register, MapTableEntry, MapTableType
from pyrift_signal import Signal
from pyrift_param import Param
from pyrift_apb import host_apb_to_verilog, host_apb_read_to_verilog, host_apb_unwrap_port, host_apb_unwrap_connect
# from pyrift_field import Pad_Field

# Keep declaration for backward compatibility, deprecated type usage for enum
from enum import IntEnum # Please use FieldEncode instead of IntEnum for enum fields

import re
import os
import math
from operator import attrgetter
import itertools


# ----------------------------------------------------------------------------


class RegisterMap(PyriftBase):
    """
        RegisterMap is collection of Register with notion of address location
        Each register is stored in reg_list in increasing address order
        base_address is the start address of collection or register (for registermap consider standalone)
    """

    def __init__(self, name, array=None):
        super(RegisterMap, self).__init__()
        self.name = name

        self.array = array

        # Main goal of register is to collect list of field
        self.reg_list = []
        # module generated may use extra signal (like user reset signal, ...)
        # extra_signal_list collect all these extra signals
        self.extra_signal_list = []
        self._validated_signal_list = [] # extra signal list after validation
                                         # may be corrected around default reset signal
        # module generated may use constant parameter that propagate to verilog package
        # param_list collect all these defined parameter
        self.param_list = []

        # define default reset signal for field that do not overload
        # It is significant only if registermap is used as root level
        # (not included in other registermap)
        # On validate_check, it is added to _validated_signal_list if no other default signal is defined
        # and at least one field use reset
        self.default_reset_signal = Signal("rst_n", activelow=True, sync=False)

        # define default hwce : clock enable for hw interfacing
        # To be used to slow down the host RIF interfacing (ready pause on host bus)
        # to allow hw to detect short pulse like swmod, wr pulse for external register, ...
        # Each field will check after validate_check if it is defined
        # (can be overloaded directly inside Field definition)
        # if defined, must be active high and synchronous
        self.default_hwce_signal = None
        # ... = Signal("hwce", activelow=False, sync=True)

        # base address is initially 0, and unlikely to change
        # this is the base for local offset computation
        # Top level rif can use it to define the base address of rif decoding
        self.base_address = 0

        # offset_to_parent only if RegisterMap have a parent,
        # then the base_address should be parent.base_address+self.offset_to_parent
        self.offset_to_parent = 0

        # Select the host interface : default to APB
        self.rif_host_bus = "APB"

        # In case of APB, addr[31:16] is decoded to PSEL
        # addr[15:0] is decoded inside rif decoder
        self.rif_host_bus_msb = 15
        # rif_addr_msb : set to have enough bit to address decode every register
        # usually set as ceil(log2( size of regmap))
        self.rif_addr_msb = 0

        # map attribute : pad and align
        # Alignement is done before padding
        # - alignment padding
        # - pre_pad
        # - actual registermap
        # - post_pad
        self.align = 4 # default for 32 bits architecture
        self.pre_pad = 0
        self.post_pad = 0

        self.interface_name = "IF_RIF"
        self.module_name = "rif"
        self.uvm_regmodel_classname = None # default, otherwise overload the uvm regmodel class name

        # parent is used when register is included in a parent container
        # allow check for single parent inclusion
        self.parent = None

        self.pyrift_dirname = os.path.dirname(os.path.abspath(__file__))
        # print(f"pyrift running from {self.pyrift_dirname}")
        self.user_notice = self.pyrift_dirname+"/lib/default_user_notice.txt"

    def add(self, register, align=0, pad=0):
        """
            Add either Register or registerMap
            Assume registerMap added have already all register
        """
        if (not isinstance(register, Register)):
            if (not isinstance(register, RegisterMap)):
                raise RuntimeError("Unexpected register added : not a Register or RegisterMap")

        if(register.parent is not None):
            # register is already owned by another parent
            # register sharing is not supported
            raise RuntimeError("Adding register which already have a parent : Sharing is not supported")
        # print("adding reg %s to %s, parent = " % (register.name, self.name), register.parent)

        register.parent = self
        # pad and align are recorded inside register
        if(pad != 0):
            register.pre_pad = pad
            # post-pad is to be handled directly when register is created
            # print ("Adding pad to register as pre-padding")
        if(align != 0):
            register.align = align
        self.reg_list.append(register)


    def add_signal(self, signal):
        """
            Add signal and keep track in extra_signal_list
            Assume registerMap added have already all register
        """
        if (not isinstance(signal, Signal)):
            raise RuntimeError("Unexpected signal added : not a Signal instance")
        self.extra_signal_list.append(signal)

    def add_param(self, param):
        """
            Add param and keep track in param_list
        """
        if (not isinstance(param, Param)):
            raise RuntimeError("Unexpected param added : not a Param instance")
        self.param_list.append(param)

    def get_signal_by_name(self, signal_name):
        for signal in self._validated_signal_list:
            if signal.name == signal_name:
                return signal
        msg = (f"Unable to locate signal '{signal_name}' into regmap {self.name}",
               f"Know list is {[signal.name for signal in self._validated_signal_list]}")
        raise  RuntimeError(msg)

    def get_all_register_or_regmap(self):
        """
            get register list (can be a true register or a regmap as well)
        """
        return [r for r in self.reg_list if r.ispresent]

    def get_all_register_in_hierarchy(self):
        """
            get register list (only true register, regmap are expanded)
        """
        register_list = []
        for register in self.get_all_register_or_regmap():
            if (isinstance(register, Register)):
                register_list.append(register)
            elif (isinstance(register, RegisterMap)):
                # sub register map, add the registerMap hierarchy
                register_list.extend(register.get_all_register_in_hierarchy())
            else:
                raise RuntimeError("Unexpected register class : not a Register or RegisterMap")

        return register_list

    def get_root_parent(self):
        parent = self
        while parent.parent:
            # progress through parent tree to root parent (parent is None)
            parent = parent.parent
        return parent

    def validate(self):
        """
            validate is 2 step:
              - step 1: validate_check : hierarchy check processing in nested regmap and register
              - step 2: generate map table at root level through hierarchy
        """
        print(f"Validating regmap '{self.name}' now ...")

        self.validate_check()

        # construct the map table, starting at root reg_map base address
        self.map_table, current_address = self.get_map_table(
                current_address=self.base_address,
                current_range=(), # top regmap validate() is root for array nesting
                current_index=()) # top regmap validate() is root for array nesting
        self.reg_map_size = current_address - self.base_address
        #print(self.map_table)
        print(f"Regmap size = {self.reg_map_size}")

        # update the addressing size required to fit the rif
        self.rif_addr_msb = math.ceil(math.log2(self.reg_map_size))

        # Flag completion of interface generation
        # (not yet done, expected after to_verilog_interface() called)
        self.interface_generation_complete_flag = False

        if self.rif_host_bus_msb < self.rif_addr_msb:
            raise  RuntimeError(f"'rif_host_bus_msb' not large enough to support current amount of register\n"
                                f"at least [{self.rif_addr_msb}:0] required, where [{self.rif_host_bus_msb}:0] provided !\n"
                                f"Please increase {self.rif_host_bus_msb}\n")

        # print("RegMap addr range set to %d\n" % self.rif_addr_msb)
        print(f"Validating regmap '{self.name}' Done")


    def validate_check(self):
        """
            Register expected to be fully defined now
            Run validation check before allowing any translation (html doc, rtl verilog)
            Add pad fields around actual useful fields
        """

        print(f"Checking regmap '{self.name}' now ...")

        # normalize array : integer are converted to tuple
        # 3 =>(3,)
        if isinstance(self.array, int):
            self.array = (self.array,)

        # finetune the extra_signal_list around reset definition
        # This is required to be done at regmap level before
        # the validate_check can be call on register (check signal availability for reset or ...)
        self._validated_signal_list = []
        self._validated_signal_list.extend(self.extra_signal_list)
        # search for a user defined default reset
        list_user_default_reset = [s for s in self._validated_signal_list if s.field_reset]
        if len(list_user_default_reset)>1:
            print("Offending multiple User_default_reset are ",[s.name for s in list_user_default_reset])
            raise RuntimeError("Only single user default reset signal supported")
        elif  len(list_user_default_reset)==1:
            # exactly one reset defined from user etra signal
            # This overload the defaut reset signal (likely to replace rst_n default)
            self.default_reset_signal = list_user_default_reset[0]
        else:
            # no user defined reset, get sure to have the reset signal in signal list
            # for port creation
            # but only if the default is not set as None (no reset of flop by default)
            if self.default_reset_signal:
                self._validated_signal_list.append(self.default_reset_signal)

        # Now can validate_check sub ressource (register, registermap)
        for reg in self.get_all_register_or_regmap():
            # sub ressource may need validate and mapping propagation (if registerMap)
            reg.validate_check()

        # update list of enum used (directly set self.encode_list)
        self.encode_list = self.get_encode_list()

        # check for deprecated apb msb definition
        if hasattr(self, 'rif_apb_bus_psel_msb'):
            raise  RuntimeError("rif_apb_bus_psel_msb attribute replace by rif_host_bus_msb,\n"
                                "Please update your script accordingly. Sorry for inconvenience ...\n"
                                "Host interface planned to be supported to other bus than APB")
        print(f"Checking regmap '{self.name}' Done")


    def get_map_table(self, current_address, current_range, current_index):
        """
            Build map_table list
            This collects all item in increasing address order (keeping herarchy information)
            item entry can be of different flavor:
             - register (one entry for each array index)
             - reg_map
             - sub reg_map (or lower level)
            Each entry collect the info:
             - adress
             - array_range : tuple with size for each dimension
               (2,3) for reg_map.array=2, and register.array=3
             - array_index : index in array_range
               (0,0),(0,1),(0,2),(1,0),(1,1),(1,2) to cover of entry of (2,3) for array_range
            parameter is current_address, current_range, current_index:
             - current_range, current_index is base location for nesting of regmap array
               range start at () for root parent regmap, then tupple is extended at each nesting
               index start at () for root parent regmap, then tupple is extended at each nesting array entry
            return - the table map extension (as list) and
                   - the next address (current address + size of entry table)
        """
        map_table = []

        # REGISTER MAP entry can be single entry or array ...
        if self.array:
            regmap_array_entry = MapTableEntry()
            regmap_array_entry.type = MapTableType.REG_MAP_ARRAY
            regmap_array_entry.register = self
            regmap_array_entry.address = current_address
            # regmap_array_entry.size = TBD after end of array map

            # if array is specified as integer, it is converted to a one dimension tuple in validate()
            if not type(self.array) is tuple:
                raise RuntimeError(f"array {self.array} expected to be of type tuple (not {type(self.array)}) (or converted from int for user)")
            if any([s < 2 for s in self.array]):
                raise RuntimeError(f"any array dimension is required larger than 2 : not OK for {self.array} of {self.name}")

            # REG_ARRAY entry have the full range of array, and (0,0,...) as array_index
            regmap_array_entry.array_range = self.array
            regmap_array_entry.array_index = (0,)*len(self.array)
            map_table.append(regmap_array_entry)

            array_range_list = [range(i) for i in regmap_array_entry.array_range]
            array_range = regmap_array_entry.array_range
            array_index = regmap_array_entry.array_index
            print(f"array range=, {array_range}, array_index=, {array_index}")

            for array_index in itertools.product(*array_range_list):
                print(f"REGISTER-MAP array_index={array_index} of {self.name}")
                table_extension, current_address = self.get_map_table_single_array_item(
                    current_address,
                    current_range=tuple(current_range+array_range),
                    current_index=tuple(current_index+array_index))
                map_table.extend(table_extension)

            # fix regmap_array_entry size (already inside map_table)
            regmap_array_entry.size = current_address - regmap_array_entry.address
        #
        else:
            # no array on this regmap
            table_extension, current_address = self.get_map_table_single_array_item(
                current_address,
                # just propagate same upper layer array information to current regmap
                current_range=current_range,
                current_index=current_index)
            map_table.extend(table_extension)
        return map_table, current_address

    def get_map_table_single_array_item(self, current_address, current_range, current_index):
        map_table = []

        # ALIGN
        if(self.align != 0):
            #check align is multiple of 4 (only 32 bits architecture is supported)
            if self.align % 4 != 0:
                raise RuntimeError(
                    f"registerMap is aligned with non multiple of 4 (align={self.align})\n"
                    f"Only 32 bit architecture is supported !")
            # get sure the current address is aligned
            if(current_address % self.align != 0):
                # mis-aligned, first re-align before
                aligned_address = current_address - current_address % self.align
                # then add one align step to re-align properly
                aligned_address += self.align
                # re-align from current_address to aligned_address
                entry = MapTableEntry()
                entry.type = MapTableType.ALIGN_PAD
                entry.address = current_address
                entry.register = self # relative regmap for pad (not actually a register)
                entry.size = aligned_address - current_address
                entry.array_range = current_range
                entry.array_index = current_index
                map_table.append(entry)
                current_address += entry.size
        # Pre-PAD
        if(self.pre_pad != 0):
            #check align is multiple of 4 (only 32 bits architecture is supported)
            if self.pre_pad % 4 != 0:
                raise RuntimeError(
                    f"registermap with pad non multiple of 4 (pad={self.pre_pad})\n"
                    f"Only 32 bit architecture is supported")
            entry = MapTableEntry()
            entry.type = MapTableType.PRE_PAD
            entry.address = current_address
            entry.register = self # relative regmap for pad (not actually a register)
            entry.size = self.pre_pad
            entry.array_range = current_range
            entry.array_index = current_index
            map_table.append(entry)
            # print(f"Adding pad entry @={current_address} size={entry.size}")
            current_address += entry.size

        regmap_entry = MapTableEntry()
        regmap_entry.type = MapTableType.REG_MAP
        regmap_entry.address = current_address
        regmap_entry.register = self # regmap (not actually a register)
        regmap_entry.size = 0 # to be fill when regmap table is completed
        regmap_entry.array_range = current_range
        regmap_entry.array_index = current_index
        map_table.append(regmap_entry)


        for register in self.get_all_register_or_regmap():
            # print(f"Adding ressource to map_table {register.name}")
            if not (isinstance(register, Register) or isinstance(register, RegisterMap)):
                raise RuntimeError("Unexpected register class : not a Register or RegisterMap")
            # One register/registermap can create one or more (if array) entry for map_table
            # print(f"Adding register/sub regmap to map_table {register.name}")
            table_extension, current_address = register.get_map_table(
                    current_address,
                    # just propagate same upper layer array information to current regmap
                    current_range=current_range,
                    current_index=current_index)
            map_table.extend(table_extension)
            # print(f"Sub reg map add reach @={current_address}")

        # Post-PAD
        if(self.post_pad != 0):
            #check align is multiple of 4 (only 32 bits architecture is supported)
            if self.post_pad % 4 != 0:
                raise RuntimeError(
                    f"registermap with pad non multiple of 4 (pad={self.post_pad})\n"
                    f"Only 32 bit architecture is supported")
            entry = MapTableEntry()
            entry.type = MapTableType.POST_PAD
            entry.address = current_address
            entry.register = self # relative regmap for pad (not actually a register)
            entry.size = self.post_pad
            entry.array_range = current_range
            entry.array_index = current_index
            map_table.append(entry)
            current_address += entry.size
        # fix reg map entry size (already inside map_table)
        regmap_entry.size = current_address - regmap_entry.address

        return map_table, current_address

    def get_encode_list(self):
        """
            Build the list of enum encode used in register field
        """
        encode_list = []
        for register in self.get_all_register_or_regmap():
            encode_list.extend(register.get_encode_list())

        # convert list to set (unique)
        # set([]) # use set to keep unique (enum expect only once)
        # get sorted by class __name__ so the generated output file are reproductible
        encode_list = sorted(set(encode_list), key=attrgetter('__name__'))
        return encode_list

    def get_root_multipart_from_slave(self, slave_field):
        # print (f"Searching root for slave = {slave_field.name}")
        # find any multipart root field where current slave belong
        for register in self.get_all_register_or_regmap():
            for field in register.get_all_field():
                if field.multipart_if_pattern: # possible root field ?
                    # print(f"root candidate is {field.name}")
                    m = re.match(field.multipart_if_pattern, slave_field.name)
                    if m: # match, this is the root field found
                        return field
        raise RuntimeError(f"Unable to match root field for slave field : {slave_field.name}")
        return None

    def get_field_multipart_info(self, pattern, root_field):
        field_list = []
        m = re.match(pattern, root_field.name)
        if not m.group(1):
            raise RuntimeError(f"Unable to match root name in multipart field <{pattern}> in {root_field.name}")
        root_name = m.group(1)
        # print (f"Searching multipart info for root_name = {root_name}")
        for register in self.get_all_register_or_regmap():
            for field in register.get_all_field():
                if field.multipart_if_slave or field.multipart_if_pattern:
                    m = re.match(pattern, field.name)
                    if m: # match, but still filter to match the root_name
                        if root_name == m.group(1):
                            field_list.append(field)

        if(len(field_list)==0):
            raise RuntimeError("Unable to locate peer multipart field for ",pattern)
        if(len(field_list)>0):
            # field list are sorted by name (incremental part #ID)
            field_list =sorted(field_list, key=attrgetter('name'))
            # print("Found peer multipart sorted:",field_list)
        bit_width = sum([f.bit_width for f in field_list])
        return {'root': root_name, 'field_list' : field_list, 'bit_width' : bit_width}

    def to_html(self):
        html = ""
        for entry in self.map_table:
            print(f"[0x{entry.address:08x}]:{entry.type}:{entry.register.name}"\
                  f":{entry.get_array_index_bracket()}/{entry.get_array_c_range_definition()}"\
                  f":idx={entry.linear_index if hasattr(entry, 'linear_index') else 0}")
            if entry.type == MapTableType.REG_ARRAY:
                html += f"<h2><p>Reg Array : {entry.register.name}{entry.get_array_c_range_definition()}</p></h2>\n"
            if entry.type == MapTableType.REG_MAP_ARRAY:
                html += f"<h2><p>Regmap Array : {entry.register.name}{entry.get_array_c_range_definition()}</p></h2>\n"
            if entry.type == MapTableType.REG:
                if entry.is_first_of_array():
                    html += f"<hr>"
                    html += f"<h2><p>0x{entry.address:04x}: {entry.register.name}"
                    html += f"{entry.get_array_index_bracket()}</p></h2>\n"
                    html += entry.register.to_html(entry)
                else:
                    html += f"<hr><p>0x{entry.address:04x}: {entry.register.name}"
                    html += f"{entry.get_array_index_bracket()}</p>\n"
            if entry.type == MapTableType.REG_MAP:
                html += f"<h1><caption>RegisterMap: {entry.register.name}{entry.get_array_index_bracket()}</caption></h1>\n"
                html += f"<p>Addr = 0x{entry.address:04x}:0x{entry.address+entry.size:04x}<br>\n"
                html += f"Reg map size = {entry.size}</p>\n"
            if entry.type == MapTableType.PRE_PAD:
                html += f"<hr>"
                html += f"<h2><p> 0x{entry.address:04x}:0x{entry.address+entry.size:04x}: Pre-Pad:size={entry.size}"\
                        f" on {entry.register.name}{entry.get_array_index_bracket()}</p></h2>\n"
            if entry.type == MapTableType.POST_PAD:
                html += f"<hr>"
                html += f"<h2><p> 0x{entry.address:04x}:0x{entry.address+entry.size:04x}: Post-Pad:size={entry.size}"\
                        f" on {entry.register.name}{entry.get_array_index_bracket()}</p></h2>\n"
            if entry.type == MapTableType.ALIGN_PAD:
                html += f"<hr>"
                html += f"<h2><p> 0x{entry.address:04x}:0x{entry.address+entry.size:04x}: Align Pad:size={entry.size}"\
                        f" on {entry.register.name}{entry.get_array_index_bracket()}</p></h2>\n"

        html += "<hr>"
        timestamp_and_version = self.pyrift_generation_message(self.user_notice,comment_header='').replace('\n', '<br>\n')
        html += f"<p>{timestamp_and_version}</p>\n"
        return html

    def to_verilog_register(self):
        """
            Generate the verilog register implementation for all register
        """
        verilog = ""
        # iterate over all register inside map_table:
        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                # print (f"Verilog instanciate register : {entry.register.name}")
                verilog += entry.register.to_verilog(entry)

        return verilog

    def to_verilog_decoder(self):
        """
            Generate the verilog decoding from generic host interface (APB or ...)
            Generate the verilog address decoding logic for each register
            generate @ decoding to register in the current register map
            mux _swrdata from each register to rif_rdata
            use the generic "host_access_wr" signal to gate the write decoding
        """
        verilog = ""
        verilog += "// host interface bus cycle decoder\n"
        verilog += "logic [31:0] rif_base_address;\n"
        verilog += "assign rif_base_address = 32'h%08X;\n" % self.base_address
        verilog += "logic rif_access_swwr;\n"
        verilog += "logic rif_access_swrd;\n"
        verilog += "logic rif_access_ready;\n"
        verilog += "assign host_access_ready = rif_access_ready;\n"
        verilog += "assign rif_access_swwr = host_access_wr  &&\n"
        verilog += "                         (host_addr[%d:%d] == rif_base_address[%d:%d]);\n" %\
            ((self.rif_host_bus_msb, self.rif_addr_msb) * 2)
        verilog += "assign rif_access_swrd = host_access_rd  &&\n"
        verilog += "                         (host_addr[%d:%d] == rif_base_address[%d:%d]);\n" %\
            ((self.rif_host_bus_msb, self.rif_addr_msb) * 2)
        verilog += "\n"

        # iterate over all register inside map_table:
        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                # print (f"Verilog decode register : {entry.register.name}")
                array_name_suffix = entry.get_suffix_array_name() # empty unless actual array in use
                reg_name = entry.register.get_full_name()+array_name_suffix
                verilog += f"// Decoding register {entry.register.name} {reg_name}:@=0x{entry.address:0x}\n"

                # generate address decode verilog for each _swwr/_swrd
                verilog += f"logic {reg_name}_r_swwr;\n"
                verilog += f"logic {reg_name}_r_swrd;\n"
                verilog += f"logic {reg_name}_r_ready;"\
                               f"// ready only for external register (or unused)\n"
                verilog += f"logic [31:0] {reg_name}_swrdata;\n"

                verilog += f"assign {reg_name}_r_swwr = rif_access_swwr"
                if self.rif_addr_msb > 2:
                    verilog += f"\n   && (host_addr[{self.rif_addr_msb - 1}:2] == "\
                                   f"{self.rif_addr_msb-2}'('h{entry.address:0x}/4));\n"
                else:
                    # Handle single register case ith 0 bit address decoding size
                    verilog += f";\n"
                verilog += f"assign {reg_name}_r_swrd = rif_access_swrd"
                if self.rif_addr_msb > 2:
                    verilog += f"\n   && (host_addr[{self.rif_addr_msb - 1}:2] == "\
                                   f"{self.rif_addr_msb-2}'('h{entry.address:0x}/4));\n"
                else:
                    # Handle single register case ith 0 bit address decoding size
                    verilog += f";\n"

                #####################" verilog += entry.register.to_verilog(entry)
        verilog += f"// -------------------------------\n"

        verilog += f"// generated data muxing _r_swrd for all register\n"

        # iterate over all register inside address map:
        # generate data muxing _r_swrd
        verilog += f"always_comb begin : proc_host_rdata_for_register\n"
        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                # print (f"Verilog data muxing rfor egister : {entry.register.name}")
                array_name_suffix = entry.get_suffix_array_name() # empty unless actual array in use
                reg_name = entry.register.get_full_name()+array_name_suffix
                verilog += f"    if( {reg_name}_r_swrd) begin\n"
                verilog += f"       rif_rdata = {reg_name}_swrdata;\n"
                verilog += f"    end\n"
                verilog += f"    else\n"
        verilog += f"   begin\n"
        verilog += f"      rif_rdata = 0;\n"
        verilog += f"   end\n"
        verilog += f"end\n"

        verilog += f"// -------------------------------\n"
        verilog += f"// generated ready muxing for all register\n"
        # rif_access_ready = 1 unless the accessed register is not giving ready
        verilog += f"always_comb begin : proc_host_ready\n"
        verilog += f"   rif_access_ready = 1;\n"
        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                #print (f"Verilog ready muxing for register : {entry.register.name}")
                array_name_suffix = entry.get_suffix_array_name() # empty unless actual array in use
                reg_name = entry.register.get_full_name()+array_name_suffix
                verilog += f"   // generate ready selection from register ready, either read or write\n"
                verilog += f"   if( ({reg_name}_r_swrd || {reg_name}_r_swwr) &&\n"
                verilog += f"       !{reg_name}_r_ready) begin\n"
                verilog += f"      rif_access_ready = 0; // force to 0 due to accessed register not ready\n"
                verilog += f"   end\n"
        verilog += f"end\n"
        verilog += f"// -------------------------------\n"
        return verilog


    def get_verilog_interface_list(self):
        """
            explore all register/field to list all interface involved
        """
        interface_list = set([])
        # print(f"############ ------Searching for all interfaces")

        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                # print (f"register : {entry.register.name}, if={entry.register.get_interface_name()}")
                for field in entry.register.get_all_actual_field():
                    if_name = field.get_interface_name()
                    # print(f"Found interface {if_name}")
                    interface_list.add(if_name)
        return sorted(interface_list)

    def get_reg_entry_list_from_if_name(self, if_name):
        result_list = []
        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                if len([f for f in entry.register.get_all_actual_field() if (f.get_interface_name() == if_name)])>0:
                    # Register that is part of if_name now confirmed, so add to return list
                    result_list.append(entry)
        return result_list

    def to_verilog_interface_by_name(self, if_name):
        """
            Generate the verilog interface filtering by interface name
            (to be called once for each interface name used)
        """

        verilog = ""
        verilog += f"interface {if_name};\n"
        # Find all register that have field as member of if_name (register only on other IF are ignored)
        for entry in self.get_reg_entry_list_from_if_name(if_name):
            # only first register of array generate a signal in this interface
            if entry.is_first_of_array():
                reg_name = entry.register.get_full_name()
                # print (f"register inIF: {reg_name}, if={if_name} range={entry.array_range}")
                verilog += f"   //  [{entry.address:08X}]:{reg_name}:range={entry.get_array_range_definition()}\n"
                # for verilog interface, parent register map passed for multipart resolution
                verilog += entry.register.to_verilog_interface_by_name(
                    if_name=if_name,
                    array_range_definition=entry.get_array_range_definition(),
                    parent_regmap=self)

                verilog += f"   //  [{entry.address:08X}]:{reg_name}\n"
                verilog += f"   // ----------------------------------\n"
            else:
                verilog += f"   //  [{entry.address:08X}]:{reg_name}{entry.get_array_index_bracket()}\n"
                verilog += f"   // ----------------------------------\n"

        # here input and output list should be ready for current interface,
        # possibly other interface cn be collected as well
        # so generate the modport
        current_if_input_list = [io for io in self.interface_io_list  if io['rif_out_dir']==False and io['if_name']==if_name]
        current_if_output_list = [io for io in self.interface_io_list  if io['rif_out_dir']==True and io['if_name']==if_name]
        # print("if io =", [modport_io for modport_io in self.interface_io_list] )
        # print("if input =", [modport_in for modport_in in current_if_input_list] )
        # print("if output =", [modport_out for modport_out in current_if_output_list] )
        verilog += f"\n"
        verilog += f"   // ----------------------------------\n"
        verilog += f"\n"
        verilog += f"   modport rif(\n"
        verilog += ",\n".join(["      input  "+ port['name'] for port in current_if_input_list] +
                              ["      output "+ port['name'] for port in current_if_output_list]) + "\n"
        verilog += f"   );\n"
        verilog += f"\n"
        verilog += f"   // ----------------------------------\n"
        verilog += f"\n"
        verilog += f"   modport hw(\n"
        verilog += ",\n".join(["      output "+ port['name'] for port in current_if_input_list] +
                              ["      input  "+ port['name'] for port in current_if_output_list]) + "\n"
        verilog += f"   );\n"
        verilog += f"\n"
        verilog += f"   // ----------------------------------\n"
        verilog += f"\n"

        verilog += f"endinterface // {if_name}\n"

        return verilog

    def to_verilog_unwrap_interface_declare_by_name(self, if_name):
        """
            Generate the verilog interface signal declaration for port of rif_unwrap
            (to be called once for each interface name used)
            This reuse the interface definition collected during module interface generation
            (during to_verilog_interface() ---> to_verilog_interface_by_name() )
        """
        verilog = ""
        verilog += f"   // declare ports for interface : {if_name}.rif {if_name.lower()}\n"
        # while generating interface, all type and name of signal of interface have been
        # collected in self.interface_io_list (done for all interface, so need to filter by if_name)
        # Then need to dump all entry
        for io in self.interface_io_list:
            if io['if_name']==if_name:
                verilog += f"   {'output' if io['rif_out_dir'] else 'input '} var {io['type']} {io['name']},\n"
        verilog += f"\n"

        return verilog


    def to_verilog_unwrap_interface_connect_by_name(self, if_name):
        """
            Generate the verilog interface signal declaraonnectiontion for port of rif_unwrap
            (to be called once for each interface name used)
            This reuse the interface definition collected during module interface generation
            (during to_verilog_interface() ---> to_verilog_interface_by_name() )
        """
        verilog = ""
        verilog += f"// connect ports for interface : {if_name}.hw {if_name.lower()}.hw\n"
        # while generating interface, all type and name of signal of interface have been
        # collected in self.interface_io_list (done for all interface, so need to filter by if_name)
        # Then need to dump all entry
        for io in self.interface_io_list:
            if io['if_name']==if_name:
                if io['rif_out_dir']:
                    # output
                    verilog += f"assign {io['name']} = {if_name.lower()}.{io['name']};\n"
                else:
                    # input
                    verilog += f"assign {if_name.lower()}.{io['name']} = {io['name']};\n"
        verilog += f"\n"

        return verilog


    def to_verilog_interface(self):
        """
            Generate the verilog interface of rif to rest of hw design
        """
        verilog = ""
        verilog += self.pyrift_generation_message(self.user_notice)
        verilog += "// Autogeneration may overwrite this file :\n"
        verilog += "// You should probably avoid modifying this file directly\n"
        verilog += "// ------------------------------------------------------\n"

        if len(self.encode_list):
            verilog += f"import {self.module_name}_pkg::*; // get enum definitions\n"
        else:
            verilog += f"// no import of empty package // import {self.module_name}_pkg::*; // get enum definitions\n"
        verilog += "\n"

        # while generating the interface the input and output list are collected at regmap level
        # field.to_verilog_interface_by_name will add directly while running
        # interface_io_list element expected to be dict as : {
        #   'rif_out_dir':True(output to hw)/False(input from hw) #
        #   'name': ... #
        #   'if_name': ... #
        #   'type': at interface level (include array, multipart, enum, ...) #
        #}
        self.interface_io_list = []

        for if_name in self.get_verilog_interface_list():
            verilog += self.to_verilog_interface_by_name(if_name)

        # Flag completion of interface generation
        self.interface_generation_complete_flag = True
        return verilog

    def to_verilog_connect_interface(self):
        """
            Generate the verilog connection of internal register to interface
        """
        verilog = ""
        verilog += "// Connect register to interface %s\n" % self.interface_name
        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                # print (f"CI register : {entry.register.name}, if={entry.register.get_interface_name()}")
                reg_name = entry.register.get_full_name()
                array_name_suffix = entry.get_suffix_array_name() # empty unless actual array in use

                verilog += f"   //  [{entry.address:08x}]:{reg_name}\n"
                # for verilog interface, parent register map passed for multipart resolution
                verilog += entry.register.to_verilog_connect_interface(self, entry)
                verilog += f"   //  [{entry.address:08x}]:{reg_name}\n"
                verilog += f"   // ----------------------------------\n"

        return verilog

    def to_verilog_clk_and_reset_port(self):
        """
            Generate the verilog module clock and reset port
            Use for both module and module_unwrap
        """

        verilog = ""
        verilog += f"   // Clock and Reset\n"
        verilog += f"   input  var logic clk,\n"
        # rst_n will be generated as part of extra signal list
        if len(self._validated_signal_list)>0:
            verilog += f"   // extra user input signal ...\n"
            for signal in self._validated_signal_list:
                signal_width = ""
                signal_width = f"[{signal.width-1}:0]" if signal.width>1 else ""
                verilog += f"   input  var logic {signal_width} {signal.name}, // "
                verilog += f"Synch " if signal.sync else "async "
                verilog += f"activelow " if signal.activelow else "activehigh "
                verilog += f"+ Default field RESET " if signal.field_reset else ""
                verilog += f"\n"
        return verilog

# ----------------------------------------------------------------------------

    #     ##     ##  #######  ########  ##     ## ##       ########
    #     ###   ### ##     ## ##     ## ##     ## ##       ##
    #     #### #### ##     ## ##     ## ##     ## ##       ##
    #     ## ### ## ##     ## ##     ## ##     ## ##       ######
    #     ##     ## ##     ## ##     ## ##     ## ##       ##
    #     ##     ## ##     ## ##     ## ##     ## ##       ##
    #     ##     ##  #######  ########   #######  ######## ########
    def to_verilog_module(self):
        """
            Generate the verilog module implementation of rif
        """

        verilog = ""
        verilog += self.pyrift_generation_message(self.user_notice)
        verilog += "// Autogeneration may overwrite this file :\n"
        verilog += "// You should probably avoid modifying this file directly\n"
        verilog += "// ------------------------------------------------------\n"
        verilog += "// RIF module verilog implementation\n"

        verilog += "\n"
        if len(self.encode_list):
            verilog += f"import {self.module_name}_pkg::*; // get enum definitions\n"
        else:
            verilog += f"// no import of empty package // import {self.module_name}_pkg::*; // get enum definitions\n"
        verilog += "\n"
        verilog += "// enforce check for net type (var or wire is mandatory)\n"
        verilog += "`default_nettype none\n"
        verilog += "\n"
        verilog += f"module {self.module_name} (\n"
        verilog += self.to_verilog_clk_and_reset_port()
        verilog += "\n"

        verilog += f"   // RIF to user HW interface port\n"
        for if_name in self.get_verilog_interface_list():
            verilog += f"   {if_name}.rif {if_name.lower()},\n"
        verilog += "\n"

        if self.rif_host_bus == "APB":
            verilog += f"   // APB use to control all peripherals (synchronous to clk)\n"
            verilog += f"   IF_APB_BUS.slave    if_apb\n"
        elif self.rif_host_bus == "APB_CDC":
            verilog += f"   // APB with CDC clocking used to control all peripherals\n"
            verilog += f"   // if_apb is synchronous to apb_cdc_clk\n"
            verilog += f"   input  var logic apb_cdc_clk,\n"
            verilog += f"   IF_APB_BUS.slave if_apb_cdc\n"
        else:
            raise RuntimeError("Unsupported 'rif_host_bus' requested:", self.rif_host_bus)
        verilog += ");\n"
        verilog += "\n"

        verilog += "// ----------------------------------------------------------------------------\n"
        verilog += "\n"
        if self.rif_host_bus == "APB_CDC":
            verilog += "// rif_host_bus is APB_CDC\n"
            verilog += "// APB at external port is 'if_apb_cdc' on 'apb_cdc_clk'\n"
            verilog += "// instanciate pyrift_apb_bridge_cdc to cross the clock domain boundary\n"
            verilog += "\n"
            verilog += "IF_APB_BUS if_apb(clk);\n"
            verilog += "\n"
            verilog += f"pyrift_apb_bridge_cdc i_pyrift_apb_bridge_cdc (\n"
            invert_reset = ("" if self.default_reset_signal.activelow else "!")
            if self.default_reset_signal.sync:
                raise RuntimeError("Sync reset used is not compatible with APB CDC option\n"
                                   "Can you try to use Async reset ?")
            verilog += f"   .rst_n             ({invert_reset}{self.default_reset_signal.name}      ),\n"
            verilog += f"   .ck_cpu_side_i     (apb_cdc_clk),\n"
            verilog += f"   .ck_periph_side_i  (clk        ),\n"
            verilog += f"   .if_apb_cpu_side   (if_apb_cdc ),\n"
            verilog += f"   .if_apb_periph_side(if_apb     )\n"
            verilog += f");\n"
            verilog += "\n"

            verilog += "// ----------------------------------------------------------------------------\n"
            verilog += "\n"

        verilog += "// host_apb_to_verilog()\n"
        verilog += host_apb_to_verilog()  # convert APB to generic signals
        verilog += "//\n"
        verilog += "// to_verilog_decoder()\n"
        verilog += self.to_verilog_decoder()
        verilog += "//\n"
        verilog += "// host_apb_read_to_verilog()\n"
        verilog += host_apb_read_to_verilog()  # convert APB to generic signals
        verilog += "//\n"
        verilog += "// ----------------------------------------------------------------------------\n"
        verilog += "//\n"
        verilog += "// to_verilog_register()\n"
        verilog += "//\n"
        verilog += self.to_verilog_register()
        verilog += "//\n"
        verilog += "// ----------------------------------------------------------------------------\n"
        verilog += "//\n"
        verilog += "// to_verilog_connect_interface()\n"
        verilog += "//\n"
        verilog += self.to_verilog_connect_interface()
        verilog += "//\n"
        verilog += "// ----------------------------------------------------------------------------\n"
        verilog += "endmodule\n"
        return verilog

# ----------------------------------------------------------------------------
#     ##     ## ##    ## ##      ## ########     ###    ########
#     ##     ## ###   ## ##  ##  ## ##     ##   ## ##   ##     ##
#     ##     ## ####  ## ##  ##  ## ##     ##  ##   ##  ##     ##
#     ##     ## ## ## ## ##  ##  ## ########  ##     ## ########
#     ##     ## ##  #### ##  ##  ## ##   ##   ######### ##
#     ##     ## ##   ### ##  ##  ## ##    ##  ##     ## ##
#      #######  ##    ##  ###  ###  ##     ## ##     ## ##

    def to_verilog_unwrap(self):
        """
            Generate the verilog module wrapper around rif
            This unwrap the interface (to APB and RIF) so no interface are used externaly
            This should allow instantiation of RIF where interface are not supported
            (could be in mixed mode vhdl/verilog, where verilog instantiation is severely limted to basic port)
            (Other use case is for verification in verilator,
            which has no support for interface at top level at the moment)
            You get a module with lots of ports (instead of the couple of interface + few signal)
            but you can co-exist with basic environment
        """
        verilog = ""
        verilog += self.pyrift_generation_message(self.user_notice)
        verilog += "// Autogeneration may overwrite this file :\n"
        verilog += "// You should probably avoid modifying this file directly\n"
        verilog += "// ------------------------------------------------------\n"
        verilog += "// RIF module verilog unwrapper implementation\n"
        verilog += "// Port should have only basic type : logic, enum, ... (no interface, struct, ...)\n"
        verilog += "\n"
        if len(self.encode_list):
            verilog += f"import {self.module_name}_pkg::*; // get enum definitions\n"
        else:
            verilog += f"// no import of empty package // import {self.module_name}_pkg::*; // get enum definitions\n"
        verilog += "\n"
        verilog += "// enforce check for net type (var or wire is mandatory)\n"
        verilog += "`default_nettype none\n"
        verilog += "\n"
        verilog += f"module {self.module_name}_unwrap (\n"
        verilog += self.to_verilog_clk_and_reset_port()
        verilog += "\n"

        #
        if not self.interface_generation_complete_flag:
            raise  RuntimeError("Unwrap module generation require the interface to have been processed already\n"\
                                "Please call write_verilog_interface() before write_verilog_unwrap()");
        verilog += f"   // RIF to user HW interface port\n"
        for if_name in self.get_verilog_interface_list():
            verilog += self.to_verilog_unwrap_interface_declare_by_name(if_name)

        # Host bus interface declaration
        if self.rif_host_bus == "APB":
            with_cdc=False
        elif self.rif_host_bus == "APB_CDC":
            with_cdc=True
        else:
            raise RuntimeError("Unsupported 'rif_host_bus' requested:", self.rif_host_bus)

        # Host bus interface declaration
        verilog += host_apb_unwrap_port(with_cdc=with_cdc)

        verilog += ");\n"
        verilog += "\n"

        verilog += "// ----------------------------------------------------------------------------\n"
        verilog += "\n"

        verilog += f"// Interface declaration for RIF instanciation\n"
        for if_name in self.get_verilog_interface_list():
            verilog += f"{if_name} {if_name.lower()}();\n"
        verilog += "\n"
        apb_cdc_ext = '_cdc' if with_cdc else ''
        clk_cdc_prefix = 'apb_cdc_' if with_cdc else ''
        verilog += f"IF_APB_BUS if_apb{apb_cdc_ext}({clk_cdc_prefix}clk);\n"
        verilog += f"\n"
        verilog += f"// RIF instanciation\n"
        verilog += f"{self.module_name} i_{self.module_name} (\n"
        verilog += f"   .clk(clk),\n"
        # rst_n will be generated as part of extra signal list
        if len(self._validated_signal_list)>0:
            verilog += f"   // reset, extra user input signal ...\n"
            for signal in self._validated_signal_list:
                verilog += f"   .{signal.name} ({signal.name}), // "
                verilog += f"Synch " if signal.sync else "async "
                verilog += f"activelow " if signal.activelow else "activehigh "
                verilog += f"+ Default field RESET " if signal.field_reset else ""
                verilog += f"\n"
        verilog += f"\n"
        for if_name in self.get_verilog_interface_list():
            verilog += f"   .{if_name.lower()}({if_name.lower()}.rif),\n"
        if with_cdc:
            verilog += "\n"
            verilog += f"   .apb_cdc_clk(apb_cdc_clk),\n"
        verilog += f"   .if_apb{apb_cdc_ext}(if_apb{apb_cdc_ext})\n"
        verilog += f");\n"

        # Host bus interface connection
        if self.rif_host_bus == "APB":
            verilog += host_apb_unwrap_connect()
        elif self.rif_host_bus == "APB_CDC":
            verilog += host_apb_unwrap_connect(with_cdc=True)
        else:
            raise RuntimeError("Unsupported 'rif_host_bus' requested:", self.rif_host_bus)

        verilog += f"// interface connection (modport .hw) to unwrap module port\n"
        for if_name in self.get_verilog_interface_list():
            verilog += self.to_verilog_unwrap_interface_connect_by_name(if_name)

        verilog += "\n"

        verilog += f"endmodule // {self.module_name}_unwrap\n"
        return verilog

# ----------------------------------------------------------------------------

    def to_verilog_package(self):
        """
            Generate the verilog package file (containing enum definition)
        """
        pkg = ""
        pkg += self.pyrift_generation_message(self.user_notice)
        pkg += "// Autogeneration may overwrite this file :\n"
        pkg += "// You should probably avoid modifying this file directly\n"

        pkg += f"// ---------------------------------------------------------------------------\n"
        pkg += f"// package definition for {self.module_name} and {self.interface_name}\n"
        pkg += f"// ---------------------------------------------------------------------------\n"
        pkg += f"package {self.module_name}_pkg;\n"
        pkg += f"\n"
        # generate the typedef for all enum used
        for e in self.encode_list:
            # one typedef for each enum
            # first evaluate the number of bit needed
            if not issubclass(e, FieldEncode):
                raise RuntimeError(f"Field.encode is required to use FieldEncode (IntEnum not supported anymore)"\
                                   f" encode={e.__name__}:{e.__bases__}")
            enum_bit_length = e.get_bit_length()
            pkg += f"typedef enum logic [{enum_bit_length-1}:0] {{\n"
            pkg += ",\n".join([f"    {val.name} = {val.value}" for val in e])
            pkg += f"\n"
            pkg += f"}} Tenum_{e.__name__};\n"
            pkg += f"\n"

        pkg += f"// --------------------------------------------------------------\n"
        for param in self.param_list:
            if param.desc != None:
                pkg += f"// {param.desc}\n"
            pkg += f"localparam {param.name} = 'h{param.value:0x};\n"
            pkg += f"\n"

        pkg += f"// --------------------------------------------------------------\n"
        pkg += f"\n"
        pkg += f"// Address mapping information for regmap:\n"
        for entry in self.map_table:
            if entry.type == MapTableType.REG_MAP:
                pkg += f"// RegisterMap: {entry.register.name}{entry.get_array_index_bracket()}\n"
                pkg += f"localparam {entry.register.name.upper()}{entry.get_suffix_array_name().upper()}__BASE_ADDRESS = 32'h{entry.address:08X};\n"
                pkg += f"localparam {entry.register.name.upper()}{entry.get_suffix_array_name().upper()}__SIZE = 32'h{entry.size:08X};\n"
        pkg += f"\n"
        # decoding address range for top level : rif_host_bus_msb
        pkg += f"localparam {self.name.upper()}__HOST_BUS_DECODE_MSB = {self.rif_host_bus_msb};\n"
        pkg += f"localparam {self.name.upper()}__HOST_BUS_DECODE_MASK = 32'h{(1<<self.rif_host_bus_msb)-4:0x};\n"
        pkg += f"\n"
        pkg += f"// --------------------------------------------------------------\n"
        pkg += f"\n"
        pkg += f"// Extra constant information on registers:\n"
        # iterate over all register inside map_table
        # (only first register of array is consider (other of same array have identical type)):
        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                # pkg details are provided only once for first item of array
                if entry.is_first_of_array():
                    pkg += f"// Register is : {entry.register.name}, fullname={entry.register.get_full_name()}\n"
                    pkg += entry.register.to_verilog_package(entry)
                    pkg += f"// - - - - -\n"

        pkg += f"\n"
        pkg += f"// --------------------------------------------------------------\n"
        pkg += f"\n"
        pkg += f"endpackage\n"
        return pkg

# ----------------------------------------------------------------------------
#      ######  ##     ## ########    ###    ########
#     ##    ## ##     ## ##         ## ##   ##     ##
#     ##       ##     ## ##        ##   ##  ##     ##
#     ##       ######### ######   ##     ## ##     ##
#     ##       ##     ## ##       ######### ##     ##
#     ##    ## ##     ## ##       ##     ## ##     ##
#      ######  ##     ## ######## ##     ## ########

    def to_chead(self, wrap_define=True, with_define_constant=False):
        """
            Generate the C header file
        """

        # generate the structure mapping to ease C code writing using the RIF
        chead = ""
        chead += self.pyrift_generation_message(self.user_notice)
        chead += "// Autogeneration may overwrite this file :\n"
        chead += "// You should probably avoid modifying this file directly\n"
        chead += "// ------------------------------------------------------\n"
        if wrap_define:
            chead += f"#ifndef __CHEAD_DEF_{self.module_name.upper()}\n"
            chead += f"#define __CHEAD_DEF_{self.module_name.upper()}\n"
            chead += f"\n"
        chead += f"// C header definition for register map : {self.name}\n"
        chead += f"// module name :  {self.module_name}\n"
        chead += f"\n"
        chead += f"// typedef enum for all enum field used in any register\n"

        # generate the typedef for all enum used
        for e in self.encode_list:
            chead += f"typedef enum te_{e.__name__} {{\n"
            chead += ",\n".join([f"    {val.name} = {val.value}" for val in e])
            chead += f"\n"
            chead += f"}} Tenum_{e.__name__};\n"


        # generate C header typedef for each register
        # iterate over all register inside map_table
        # (only first register of array is consider (other of same array have identical type)):
        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                # cheader details are provided only once for first item of array
                if entry.is_first_of_array():
                    chead += f"\n"
                    chead += f"// ##############################################################\n"
                    chead += f"// Register is : {entry.register.name}, fullname={entry.register.get_full_name()}\n"
                    chead += f"// ##############################################################\n"
                    chead += f"\n"
                    chead += entry.register.to_chead(
                        entry,
                        with_define_constant=with_define_constant,
                        define_prefix=self.module_name.upper()+"__")
                # for each item or array give a chance to add detail if special rest table require it
                if entry.get_array_linear_size() > 1 and with_define_constant:
                    chead += entry.register.to_chead_reset_detail(
                            entry, define_prefix=self.module_name.upper()+"__")


        # generate C header typedef for full regmap
        chead += f"\n"
        chead += f"// --------------------------------------------------------------\n"
        chead += f"\n"
        chead += f"typedef struct\n"
        chead += f"{{\n"

        # iterate over all register/pad entry inside map_table
        # (only first register of array is consider (remaining of array are duplicated by array)):
        # Pad entry are expanded
        for entry in self.map_table:
            if entry.type == MapTableType.REG_MAP:
                chead += f"    // ---------------------------------------------\n"
                chead += f"    // RegisterMap: {entry.register.name}{entry.get_array_index_bracket()}\n"
                chead += f"    // Addr = 0x{entry.address:04x}:0x{entry.address+entry.size:04x}\n"
                chead += f"    // Reg map size = {entry.size}\n"
            if entry.type == MapTableType.REG and entry.is_first_of_array():
                chead += f"    // Register is : {entry.register.name}, addr={entry.address}=0x{entry.address:04x}, "\
                                                f"Array{entry.array_range}\n"
                chead += f"    TU_{self.module_name}_Reg_{entry.register.get_full_name()} "
                # volatile option is now handled inside "register.to_chead()" call
                chead += f"{entry.register.get_full_name()}"
                chead += f"{entry.get_array_c_range_definition()};\n"
            if entry.type == MapTableType.PRE_PAD:
                chead += f"    // Pre-Pad : addr={entry.address}=0x{entry.address:04x}, Size=+{entry.size}B => "\
                         f"new addr=0x{entry.address+entry.size:0x}\n"
                if entry.size % 4 != 0:
                    raise RuntimeError(f"Non 32bit pad @{entry.address:04x} : size={entry.size} bytes")
                chead += f"    unsigned long pad_{entry.address:04x}[{entry.size//4}];\n"
            if entry.type == MapTableType.POST_PAD:
                chead += f"    // Post-Pad : addr={entry.address}=0x{entry.address:04x}, Size=+{entry.size}B => "\
                         f"new addr=0x{entry.address+entry.size:0x}\n"
                if entry.size % 4 != 0:
                    raise RuntimeError(f"Non 32bit pad @{entry.address:04x} : size={entry.size} bytes")
                chead += f"    unsigned long pad_{entry.address:04x}[{entry.size//4}];\n"
            if entry.type == MapTableType.ALIGN_PAD:
                chead += f"    // Align pad : addr={entry.address}=0x{entry.address:04x}, Size=+{entry.size}B => "\
                         f"new addr=0x{entry.address+entry.size:0x}\n"
                if entry.size % 4 != 0:
                    raise RuntimeError(f"Non 32bit pad @{entry.address:04x} : size={entry.size} bytes")
                chead += f"    unsigned long pad_{entry.address:04x}[{entry.size//4}];\n"

        # can add {self.get_root_parent().module_name}_ into struct name ?
        # Normally no name conflict accross different top reg map
        chead += f"}} TS_{self.name};\n"
        chead += f"\n"
        chead += f"// --------------------------------------------------------------\n"
        chead += f"\n"
        chead += f"// Parameter also put in verilog package are dump here\n"
        for param in self.param_list:
            if param.desc != None:
                chead += f"// {param.desc}\n"
            chead += f"#define {param.name}  0x{param.value:0x}\n"
            chead += f"\n"

        chead += f"\n"
        chead += f"// --------------------------------------------------------------\n"
        chead += f"\n"
        chead += f"// Address mapping information for regmap:\n"
        for entry in self.map_table:
            if entry.type == MapTableType.REG_MAP:
                chead += f"// RegisterMap: {entry.register.name}{entry.get_array_index_bracket()}\n"
                chead += f"#define {entry.register.name.upper()}{entry.get_suffix_array_name().upper()}__BASE_ADDRESS 0x{entry.address:08X}\n"
                chead += f"#define {entry.register.name.upper()}{entry.get_suffix_array_name().upper()}__SIZE 0x{entry.size:08X}\n"
        chead += f"\n"
        chead += f"// --------------------------------------------------------------\n"
        chead += f"\n"
        if wrap_define:
            chead += f"#endif // __CHEAD_DEF_{self.module_name.upper()}\n"
            chead += f"\n"

        return chead

# ----------------------------------------------------------------------------
#     ##     ## ##     ## ##     ##
#     ##     ## ##     ## ###   ###
#     ##     ## ##     ## #### ####
#     ##     ## ##     ## ## ### ##
#     ##     ##  ##   ##  ##     ##
#     ##     ##   ## ##   ##     ##
#      #######     ###    ##     ##

    def to_regmodel_uvm(self):
        """
            Generate the register model in uvm (for rif verification)
        """
        regmodel = ""
        regmodel += self.pyrift_generation_message(self.user_notice)
        regmodel += "// Autogeneration may overwrite this file :\n"
        regmodel += "// You should probably avoid modifying this file directly\n"
        regmodel += "// ------------------------------------------------------\n"
        regmodel += (
                "// uvm_reg_field:\n"
                "//\n"
                "//function void configure(      uvm_reg     parent,\n"
                "//  int     unsigned    size,\n"
                "//  int     unsigned    lsb_pos,\n"
                "//      string      access,\n"
                "//      bit     volatile,\n"
                "//      uvm_reg_data_t      reset,\n"
                "//      bit     has_reset,\n"
                "//      bit     is_rand,\n"
                "//      bit     individually_accessible     )\n"
                "\n"
                "//set_access\n"
                "//'RO'      W: no effect, R: no effect\n"
                "//'RW'      W: as-is, R: no effect\n"
                "//'RC'      W: no effect, R: clears all bits\n"
                "//'RS'      W: no effect, R: sets all bits\n"
                "//'WRC'     W: as-is, R: clears all bits\n"
                "//'WRS'     W: as-is, R: sets all bits\n"
                "//'WC'      W: clears all bits, R: no effect\n"
                "//'WS'      W: sets all bits, R: no effect\n"
                "//'WSRC'    W: sets all bits, R: clears all bits\n"
                "//'WCRS'    W: clears all bits, R: sets all bits\n"
                "//'W1C'     W: 1/0 clears/no effect on matching bit, R: no effect\n"
                "//'W1S'     W: 1/0 sets/no effect on matching bit, R: no effect\n"
                "//'W1T'     W: 1/0 toggles/no effect on matching bit, R: no effect\n"
                "//'W0C'     W: 1/0 no effect on/clears matching bit, R: no effect\n"
                "//'W0S'     W: 1/0 no effect on/sets matching bit, R: no effect\n"
                "//'W0T'     W: 1/0 no effect on/toggles matching bit, R: no effect\n"
                "//'W1SRC'   W: 1/0 sets/no effect on matching bit, R: clears all bits\n"
                "//'W1CRS'   W: 1/0 clears/no effect on matching bit, R: sets all bits\n"
                "//'W0SRC'   W: 1/0 no effect on/sets matching bit, R: clears all bits\n"
                "//'W0CRS'   W: 1/0 no effect on/clears matching bit, R: sets all bits\n"
                "//'WO'      W: as-is, R: error\n"
                "//'WOC'     W: clears all bits, R: error\n"
                "//'WOS'     W: sets all bits, R: error\n"
                "//'W1'      W: first one after HARD reset is as-is, other W have no effects, R: no effect\n"
                "//'WO1'     W: first one after HARD reset is as-is, other W have no effects, R: error\n"
                "\n"
            )

        classname = self.uvm_regmodel_classname if self.uvm_regmodel_classname else f"{self.name}_regmodel"

        # iterate over all register inside map table:
        # generate uvmreg model class for each register (only first of array is used)
        for entry in self.map_table:
            if entry.type == MapTableType.REG and entry.is_first_of_array():
                regmodel += f"\n"
                regmodel += f"// ##############################################################\n"
                regmodel += f"// Register is : {entry.register.name}, fullname={entry.register.get_full_name()}\n"
                regmodel += f"// ##############################################################\n"
                regmodel += f"\n"
                regmodel += entry.register.to_regmodel_uvm(entry)

        regmodel += f"// --------------------------------------------------------------\n"
        regmodel += f"//                  REGISTER MODEL\n"
        regmodel += f"// --------------------------------------------------------------\n"
        regmodel += f"\n"
        regmodel += f"class {classname} extends uvm_reg_block;\n"
        regmodel += f"\n"
        for entry in self.map_table:
            if entry.type == MapTableType.REG and entry.is_first_of_array():
                # TODO: array should expand here
                regmodel += f"   rand {entry.register.get_full_name()}_reg "\
                            f"{entry.register.get_full_name()} "\
                            f"{entry.get_array_range_definition()};\n"
        regmodel += f"\n"
        regmodel += f"   `uvm_object_utils({classname})\n"
        regmodel += f"\n"
        regmodel += f"   function new(string name = \"{classname}\");\n"
        regmodel += f"      super.new(name, UVM_NO_COVERAGE);\n"
        regmodel += f"   endfunction: new\n"
        regmodel += f"\n"
        regmodel += f"   virtual function void build();\n"
        regmodel += f"\n"
        regmodel += f"      default_map = create_map(      \"reg_model_map\", 0, 4, UVM_LITTLE_ENDIAN);\n"
        regmodel += f"\n"
        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                regname= entry.register.get_full_name()
                regmodel += f"      {regname}{entry.get_array_index_bracket()} = {regname}_reg::type_id::create(\"{regname}\",,get_full_name());\n"
                regmodel += f"      {regname}{entry.get_array_index_bracket()}.configure(this, null, "");\n"
                # TODO: will have to call *.set_reset to define the reset value of register is changing through array of register
                # if entry.get_array_linear_size() > 1 and f.reset and not isinstance(f.reset, int):
                #     regmodel += f"      {regname}{entry.get_array_index_bracket()}.set_reset({entry.register.get_reset_value(map_table_entry):08X}, "HARD");\n"

                regmodel += f"      {regname}{entry.get_array_index_bracket()}.build();\n"
                regmodel += f"      default_map.add_reg({regname}{entry.get_array_index_bracket()},`UVM_REG_ADDR_WIDTH'h{entry.address:0x},\"RW\");\n"
                regmodel += f"\n"

        regmodel += f"   endfunction: build\n"
        regmodel += f"endclass : {classname}\n"

        return regmodel;

# ----------------------------------------------------------------------------
# Helper functions for user to write output file with auto creation of directory

    def write_chead(self, filename, wrap_define=True, with_define_constant=False):
        # create directory for file target if it does not exist
        os.makedirs(os.path.dirname(filename), exist_ok=True)

        with open(filename, "w") as text_file:
            text_file.write(self.to_chead(
                wrap_define=wrap_define, with_define_constant=with_define_constant))

    def write_html(self, filename):
        # create directory for file target if it does not exist
        os.makedirs(os.path.dirname(filename), exist_ok=True)

        with open(filename, "w") as text_file:
            text_file.write(self.to_html())

    def write_verilog_unwrap(self, filename):
        # create directory for file target if it does not exist
        os.makedirs(os.path.dirname(filename), exist_ok=True)

        with open(filename, "w") as text_file:
            text_file.write(self.to_verilog_unwrap())

    def write_verilog_module(self, filename):
        # create directory for file target if it does not exist
        os.makedirs(os.path.dirname(filename), exist_ok=True)

        with open(filename, "w") as text_file:
            text_file.write(self.to_verilog_module())

    def write_verilog_package(self, filename):
        # create directory for file target if it does not exist
        os.makedirs(os.path.dirname(filename), exist_ok=True)

        with open(filename, "w") as text_file:
            text_file.write(self.to_verilog_package())

    def write_verilog_interface(self, filename):
        # create directory for file target if it does not exist
        if len(os.path.dirname(filename))>0:
            os.makedirs(os.path.dirname(filename), exist_ok=True)

        with open(filename, "w") as text_file:
            text_file.write(self.to_verilog_interface())

    def write_regmodel_uvm(self, filename):
        # create directory for file target if it does not exist
        if len(os.path.dirname(filename))>0:
            os.makedirs(os.path.dirname(filename), exist_ok=True)

        with open(filename, "w") as text_file:
            text_file.write(self.to_regmodel_uvm())

    def write_word(self, filename, template=""):
        # Check for import error
        # at startup, if python-docx is missing, error is catch and
        # pyrift silently continue (import not needed if no word generated)
        # Now if trying to generate word document we are in trouble
        # So we report error now
        if(docx_import_error):
            print("#####")
            print("write_word() call requires python-docx library to be installed")
            print("You can try : 'pip install python-docx' or check pyrift wiki ...")
            print("#####")
            raise RuntimeError(docx_import_error)

        # create directory for file target if it does not exist
        if len(os.path.dirname(filename))>0:
            os.makedirs(os.path.dirname(filename), exist_ok=True)

        # default template is available from pyrift delivery
        default_word_template = self.pyrift_dirname+"/doc_template/pyrift_template.docx"

        if(not template):
            # no user template provided, used pyrift default
            template = default_word_template
        print(f"Generating word documentation from template :{default_word_template}")

        document = Document(template)

        # fill word document here ...
        # iterate over all register inside map_table:
        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                # print (f"word document register : {entry.register.name}")
                entry.register.document_to_word(document, entry)


        document.save(filename)

# ----------------------------------------------------------------------------

# basic test code for module if not imported
if __name__ == "__main__":
    import os
    os.makedirs("output", exist_ok=True)

    # Normal 1 bit, RW
    frw = Field(name="Frw", bit_range='0', desc="1 bit, RW")
    frw2 = Field(name="Frw_2", bit_range='3:2', desc="2 bit, RW")
    fw1c = Field(name="FW1C", bit_range='4')
    fw1c.desc = "1 bit, W1C"
    fw1c.woclr = True
    fw1c2 = Field(name="FW1C_2", bit_range='7:6')
    fw1c2.desc = "2 bit, W1C"
    fw1c2.woclr = True

    fw1s = Field(name="FW1S", bit_range='12')
    fw1s.desc = "1 bit, W1S"
    fw1s.woset = True
    fw1s2 = Field(name="FW1S_2", bit_range='15:14')
    fw1s2.desc = "2 bit, W1S"
    fw1s2.woset = True

    # Test register, mix of different bit flavour
    reg = Register("Rcheck")
    reg.add(frw)
    reg.add(frw2)
    reg.add(fw1c)
    reg.add(fw1c2)
    reg.add(fw1s)
    reg.add(fw1s2)

    pad_reg = Register("Empty_Pad_Register")

    # register will not validate_check if not inserted inside registermap
    # reg.validate_check()

    # reg2 is not validate_check before adding to reg map
    reg_tab = [None] * 10
    for i in range(1, 10):
        reg_tab[i] = Register("R%d" % i)
        reg_tab[i].add(Field(name="Frw", bit_range='0', desc="1 bit, RW"))
        reg_tab[i].add(Field(name="Frw_2", bit_range='3:2', desc="2 bit, RW"))
    reg_tab[3].shortname = "ShrtR3"

    reg3 = Register("R3")
    reg3.add(Field(name="Frw", bit_range='0', desc="1 bit, RW"))

    reg_map2_1 = RegisterMap("register_map_test_2_1")
    reg_map2_1.add(reg_tab[7])
    reg_map2_1.add(reg_tab[8])

    reg_map2 = RegisterMap("register_map_test_2")
    reg_map2.add(reg_tab[5])
    reg_map2.add(reg_tab[6])
    reg_map2.add(reg_map2_1)

    reg_map = RegisterMap("register_map_test")
    reg_map.add(reg_tab[1])
    reg_map.add(reg_tab[2], align=8)
    reg_map.add(reg_tab[3])
    reg_map.add(reg_map2)
    reg_map.validate()
    try:
        # check adding twice a register is illegal
        reg_map.add(reg_tab[5])
    except RuntimeError:
        # print("successfully detected expected exception")
        pass
    else:
        raise RuntimeError("Missing exception on multiple register added")

    reg_map.write_html("output/reg_map.html")

    # update reg map, then can iterate over the list of all registers
    reg_map.base_address = 0x100
    # with open("output/regmap.sv", "w") as text_file:
    #     text_file.write(reg_map.to_verilog_decoder())
    # with open("output/all_reg.sv", "w") as text_file:
    #     text_file.write(reg_map.to_verilog_register())
    #     text_file.write(reg_map.to_verilog_connect_interface())

    reg_map.write_verilog_interface("output/if_rif.sv")
    reg_map.write_verilog_module("output/rif.sv")
    reg_map.write_chead("output/rif.h")

# ----------------------------------------------------------------------------

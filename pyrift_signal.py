# ----------------------------------------------------------------------------
# Copyright 2019 Regis Cattenoz, regis@ip666.org
#
# This file is part of pyrift : https://gitlab.com/regisc/pyrift
#
# pyrift is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pyrift is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pyrift.  If not, see <https://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------------
# File: pyrift_signal.py
# Python Register InterFace Translation
# ----------------------------------------------------------------------------

from pyrift_field import PyriftBase
# ----------------------------------------------------------------------------


class Signal(PyriftBase):
    """
        Signal serve as declaration of extra input/output signal (like user reset signal)
    """

    def __init__(self, name,
        # bring all field to constructor parameter with default value
        # should help user for concise Field creation
        desc=None,
        width=1,
        sync=True,
        activelow=False,
        field_reset=False,
        ):
        super().__init__()
        self.name = name

        # desc is provided as constructor "desc" param
        # else carry default setting from base class
        if desc:
            self.desc = desc
        self.width = width

        #
        self.sync = sync # synchronous to clock
        self.activelow = activelow # active signal when low
        self.field_reset = field_reset # only one single signal can have field_reset set to True
                                       # If field_reset is True, then this signal is used as defaut
                                       # reset signal for all field not overloading this


# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# Copyright 2019 Regis Cattenoz, regis@ip666.org
#
# This file is part of pyrift : https://gitlab.com/regisc/pyrift
#
# pyrift is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pyrift is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pyrift.  If not, see <https://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------------
# File: pyrift_field.py
# Python Register InterFace Translation
# ----------------------------------------------------------------------------

from enum import Enum, auto, IntEnum
from re import match
import datetime
import platform;

# ----------------------------------------------------------------------------


class PyriftBase():
    """Base PyriftBase for other sub class"""

    # Add alternate version ID, planned to be incremented at each commit
    # better tracking for user which have no git tracking of the tool
    # to be incremented at each commit even if pyrift_version is unchanged
    # If branching from master without merging, to be defined how to handle ...
    # Please try to keep (pyrift_version, PYRIFT_REGMAP_VERSION) unique
    PYRIFT_REGMAP_VERSION = "15"

    def __init__(self):
        super(PyriftBase, self).__init__()
        self.name = "Uninitilized PyriftBase"
        self.desc = "Please provide description of class that inherit PyriftBase"
        self.ispresent = True  # present in final generation

    def pyrift_version(self):
        return f"Pyrift v0.3-rc"

    def pyrift_generation_message(self, user_notice, comment_header="//"):
        platform_summary = platform.sys.version.replace('\n', ' ') # can be multiline of some platform (linux)
        msg =  (
            f"{comment_header} ----------------------------------------------------------\n"
            f"{comment_header} Code generated automatically with {self.pyrift_version()}\n"
            f"{comment_header} Incremental version id: {self.PYRIFT_REGMAP_VERSION}\n"
            f"{comment_header} Python platform: {platform_summary}\n"
            f"{comment_header} System: {platform.platform()}\n"
            f"{comment_header} Generate Date: {datetime.datetime.now()}\n"
            f"{comment_header} Pyrift can be found at https://gitlab.com/regisc/pyrift\n"
            f"{comment_header} ----------------------------------------------------------\n"
        )
        with open(user_notice) as f:
            notice_text = f.readlines()
            # remove whitespace characters like `\n` at the end of each line
            # add header
            notice_text = [comment_header+" "+x.strip()+"\n" for x in notice_text]
        msg += ''.join(notice_text)
        msg += f"{comment_header} ----------------------------------------------------------\n"
        return msg

# ----------------------------------------------------------------------------


class Access(Enum):
    RW = auto()
    R = auto()
    W = auto()
    NA = auto()
    WR = RW  # Alias to RW

    def is_readable(self):
        return (self.value == Access.RW.value) or (self.value == Access.R.value)

    def is_writable(self):
        return (self.value == Access.RW.value) or (self.value == Access.W.value)

class Cdc(Enum):
    NA = auto()
    HOST_CE = auto() # resync in CDC as simple CE on host clock
    FULL_ASYNC = auto() # resync in CDC assuming full async clock

# Create subclass of IntEnum to handle encode of field enum
class FieldEncode(IntEnum):
    @classmethod
    def get_bit_length(cls):
        """
            helper method to evaluate the range of value used to encode field
        """
        enum_range = [x.value for x in cls]
        if min(enum_range) < 0:
            raise RuntimeError("Enum type for verilog package generation is only supported for positive values")
        # assume range 0 to 2^N - 1 supported in N bit logic vector
        # Find N from max
        bit_length = max(enum_range).bit_length()
        # print(f"bw={bit_length} for class={cls.__name__}")
        return bit_length

# ----------------------------------------------------------------------------


class Field(PyriftBase):
    """
        The most basic PyriftBase object.
        Fields serve as an abstraction of hardware storage elements.
    """

    # Field class variables
    # Used to set default values for any instance
    default_latch_transparent_state = 0 # latch transparent on 0 or 1
    default_latch_use_lib_ldce = False # latch generation use pyrift lib primitive (help in hold time closure in vivado)
    default_support_hwbe = False # support byte enable masking on host bus write if True

    def __init__(self, bit_range,
        name=None, shortname=None,
        interface_name=None,
        desc=None,
        # bring all field to constructor parameter with default value
        # should help user for concise Field creation
        hw=Access.RW,
        sw=Access.RW,
        ispresent=True,
        multipart_if_pattern = None,
        multipart_if_slave = False,
        if_reshape = None,
        external = False,
        extready_rd = False,
        extready_wr = False,
        latch = False,
        donttest = False,
        dontcompare = False,
        next = None,
        reset = 0, # field by default have a reset to 0, set to None to remove reset feature
        resetsignal = None,
        hwset = False,
        hwclr = False,
        swwe = False,
        swwel = False,
        swacc = False,
        swmod = False,
        delayed_swmod = False,
        rclr = False,
        rset = False,
        woclr = False,
        woset = False,
        wot = False,
        wzc = False,
        wzs = False,
        wzt = False,
        wclr = False,
        wset = False,
        encode = False,
        hw_have_wr_precedence_over_fw = False,
        support_hwbe = None, # if None, fallback to default_support_hwbe
                             # if False or True, use value for this current register
        singlepulse = False,
        hwce_signal_name = None, # None for not used as default,
                                 # use the string name of signal to use
                                 # if None fallback to default_hwce_signal of top regmap owner
                                 # hw is clock enabled when this signal is active
                                 # To be used to delay the host cycle accordingly
        cdc_out = Cdc.NA,
        cdc_swmod = Cdc.NA,
        cdc_swacc = Cdc.NA,
        cdc_out_signal_name = None, # None for not used as default,
                                    # use the string name of signal to use (clock or ce)
        cdc_in = Cdc.NA,
        cdc_in_signal_name = None, # None for not used as default,
                                   # use the string name of signal to use (clock or ce)
        ):
        super().__init__()
        if shortname:
            # shortname provided in __init__, make default name
            self.name = name if name else shortname
            self.shortname = shortname
        else:
            if name:
                self.name = name
            else:
                raise RuntimeError("Field constructor makes name or shortname mandatory !")
            self.shortname = None # shortcut for verilog naming (interface and rtl)
                                  # /!\warning, if used this limit to single instance of field in all register

        # parent is used when field is included in a parent register
        # allow check for single parent inclusion
        self.parent = None

        # desc is provided as constructor "desc" param
        # else carry default setting from base class
        if desc:
            self.desc = desc
                                 #

        # check bit field as either '2' or '4:3'
        m1 = match('^(\d*)$', bit_range)
        m2 = match('^(\d*):(\d*)$', bit_range)
        if m2:
            msb_bit = m2.group(1)
            lsb_bit = m2.group(2)
            self.lsb_bit = int(lsb_bit)
            self.bit_width = int(msb_bit) - self.lsb_bit + 1
        elif m1:
            self.lsb_bit = int(m1.group(1))
            self.bit_width = 1
        else:
            raise RuntimeError("Unexpected bit_range for field creation" + bit_range)
        self.hw = hw
        self.sw = sw
        self.ispresent = ispresent
        self.interface_name = interface_name

        self.latch = latch # if you want to generate latch based storage element
                           # Use with extreme caution, better you know what your are doing
        self.latch_transparent_state = Field.default_latch_transparent_state # from Field class var
        self.latch_use_lib_ldce = Field.default_latch_use_lib_ldce # from Field class var

        # directive to aggregate field as a slave of a large vector at interface level
        self.multipart_if_pattern = multipart_if_pattern # Field name pattern to identify other multipart slave
                                                         # should be defined only on master field
        self.multipart_if_slave = multipart_if_slave # slave multipart is to be mark true to be attached to master
        self.if_reshape = if_reshape # could be a list a extra dimension to reshape on
                                     # = (2,) to have [...:0][1:0]
        self.external = external
                      # no verilog/rtl generation for the register internal storage if external
                      # provide host bus signal in interface to allow implementation as external
                      # Note: using that prevent several option (hw properties or related)
                      #       No hwclr, hwset, woclr, woset, swwe, swmod, wz*, ...
        self.extready_rd = extready_rd # external ready signal for read access
        self.extready_wr = extready_wr # external ready signal for write access
        #
        self.donttest = donttest
        self.dontcompare = dontcompare
        # HW properties
        self.next = next
        self.reset = reset
        self.resetsignal = resetsignal # string, name of signal to be used for reset (otherwise "rst_n")
        self.hwset = hwset # makes hw writable (can be hw=R/NA)
        self.hwclr = hwclr # makes hw writable (can be hw=R/NA)
        self.swwe = swwe  # hw enable sw to write into register
        self.swwel = swwel
        self.swacc = swacc # pulse when sw is accessing register (read)
        self.swmod = swmod # pulse when sw is modifying register
                           # (write or read with side effect (rset, rclr))
        self.delayed_swmod = delayed_swmod # swmod is delayed by 1 clock on interface
                                           # modified field data already valid on pulse
        # SW properties
        self.rclr = rclr
        self.rset = rset
        #
        self.woclr = woclr  # write one clear
        self.woset = woset  # write one set
        self.wot = wot    # write one toggle
        self.wzc = wzc  # write zero clear
        self.wzs = wzs  # write zero set
        self.wzt = wzt  # write zero toggle
        #
        self.wclr = wclr  # write clear
        self.wset = wset  # write set
        self.encode = encode

        self.hw_have_wr_precedence_over_fw = hw_have_wr_precedence_over_fw
        self.singlepulse = singlepulse

        if(support_hwbe == None):
            # Constructor parameter not given, use default value
            self.support_hwbe = Field.default_support_hwbe
        else:
            # use constructor param
            self.support_hwbe = support_hwbe

        self.hwce_signal_name = hwce_signal_name # string name to find top regmap signal
        self.hwce_signal = None # Signal pointer set during validate_check if applicable
        self.cdc_out = cdc_out
        self.cdc_swmod = cdc_swmod
        self.cdc_swacc = cdc_swacc
        self.cdc_out_signal_name = cdc_out_signal_name
        self.cdc_out_signal = None # Signal pointer set during validate_check if applicable
        self.cdc_in = cdc_in
        self.cdc_in_signal_name = cdc_in_signal_name
        self.cdc_in_signal = None # Signal pointer set during validate_check if applicable

    def bit_range_str(self):
        if(self.bit_width == 1):
            result = str(self.lsb_bit)
        else:
            result = str(self.lsb_bit + self.bit_width - 1) + ':' + str(self.lsb_bit)
        return result

    def has_storage(self):
        """
            Check if the implementation have a storage element associated
            This is the case if:
                - SW is rw
                - SW is w and HW is r or rw
            SW=w and HW=w is illegal
            SW read with side effect (rclr/rset) are considerer as SW=W for storage needs
            For SWis r, HW special :hwset, hwclr force the storage
        """
        is_extended_sw_writable = (self.sw == Access.R) and (self.rclr or self.rset)
        return self.sw == Access.RW or \
            (self.sw == Access.W and self.hw.is_readable()) or \
            (is_extended_sw_writable and self.hw.is_readable()) or \
            (self.sw == Access.R and (self.hwset or self.hwclr or self.hw == Access.RW))

    def is_volatile(self):
        """
            Get the volatile feature of the field
            Volatile means the SW readback value have a chance to differ from SW written value
            Not the case if: not has_storage
            Is the case if:
                - not a sw read/write
                - rclr, rset, woclr, woset, wot, wzc, wzs, wzt, wclr, wset affect content not in usual way
                - hw has write access
                - swwe : prevent sw write to taken (make unpredictable, so is volatile)
                - external register (behaviour handle in hw is unknown, assume volatile)
        """
        volatile = (self.sw != Access.RW) or \
                self.rclr or self.rset or \
                self.woclr or self.woset or self.wot or \
                self.wzc or self.wzs or self.wzt or \
                self.wclr or self.wset or \
                self.hw.is_writable() or \
                self.hwset or \
                self.hwclr or \
                self.swwe or self.swwel or \
                self.external or \
                self.swmod # swmod force volatile, so get sure write access is not optimized by compiler
        # print (f"Register {self.name} is vol={volatile}")
        return volatile

    def get_name(self):
        """
            return name, possible alis to shortname
        """
        if self.shortname:
            return self.shortname
        else:
            return self.name

    def get_full_verilog_name(self, reg_name, array_name_suffix):
        """
            When field is used in verilog (hw rtl implmentation), the signal have the return name
            It is used either for to_verilog() or for interface generation
        """
        # shortname is a shortcut for field name in verilog and interface
        if self.shortname:
            # in case of shortname, the array suffix is required
            return self.shortname+array_name_suffix
        else:
            # in case of normal name, the array suffix is already included in regname
            return f"{reg_name}_field_{self.name}"

    def get_interface_name(self):
        """
            Field is part of a register hierarchy,
            Search first ancestor that have an interface_name
            (to be used for verilog interface connection)
            root parent may have several interface, so find out where it belong
        """
        if getattr(self, 'interface_name', None):
            return self.interface_name
        else:
            return self.parent.get_interface_name() # parent of field is a register

    def get_reset_value(self, map_table_entry):
        if self.reset:
            # self.reset is a non 0 integer or a tuple list of reset value
            if isinstance(self.reset, int):
                reset_value = self.reset
            elif isinstance(self.reset, tuple):
                # Reset value to be customized in array of register
                # Use map_table_entry to index self.reset as
                # array of reset value to apply
                # print(f"Look up reset in {self.reset} at {map_table_entry.linear_index}")
                array_size = map_table_entry.static_array_linear_size(map_table_entry.array_range)
                reg_array_size = map_table_entry.static_array_linear_size(map_table_entry.reg_array_range)
                if len(self.reset) == array_size:
                    reset_value = self.reset[map_table_entry.linear_index]
                elif len(self.reset) == reg_array_size:
                    reset_value = self.reset[map_table_entry.linear_index % reg_array_size]
                else:
                    raise  RuntimeError(
                        f"Field:{self.name}: mismatch in reset value size={len(self.reset)}"\
                        f" for {self.reset}, expect size of either 1, or {reg_array_size} or {array_size}")
            else:
                raise RuntimeError(
                    f"No support of reset value {self.reset} type field : " + self.get_name())
        else:
            # None or 0 as reset, default to 0 for result
            reset_value = 0
        return reset_value

    def is_valid_reset_value(self):
        """
            get_reset_value() always provide an integer value (default to 0 if unknown)
            is_valid_reset_value report False if get_reset_value() is inconsistent/unknwon
            Check the storage type and option, and self.reset to identify the different cases
        """
        if self.external:
            valid = False
        elif self.has_storage():
            # internal storage, flop or latch ?
            if self.latch:
                valid = (self.reset != None)
            else:
                valid = (self.reset != None)
        else: # no storage
            if self.hw.is_writable():
                # no storage, hw writable, this is status direct from hw
                valid = False
            else:
                # no storage, const to reset
                valid = True
        return valid

    def get_storage_type_str(self):
        # storage_type = "?"
        if self.external:
            storage_type = "Extern"
        elif self.has_storage():
            # internal storage, flop or latch ?
            if self.latch:
                storage_type = "Latch"
            else:
                storage_type = "F.Flop"
        else: # no storage
            if self.hw.is_writable():
                # no storage, hw writable, this is status direct from hw
                storage_type = "Hw.Wire"
            else:
                # no storage, const to reset
                storage_type = "Const"
        return storage_type

# ----------------------------------------------------------------------------

    def validate_check(self):
        # implement error checking to validate_check the field parameter are consistent
        # This is call for each field of register in Register.validate_check()

        # storage based field is already check for valid systemRdl sw/hw combination
        # check only for remaining combination of sw/hw access of internal register fields
        if not self.has_storage() and not self.external:
            # report invalid line for systemRdl standard
            if (self.sw.value==Access.W.value and self.hw.value==Access.W.value) or \
               (self.sw.value==Access.W.value and self.hw.value==Access.NA.value):
                raise RuntimeError(
                    f"sw={self.sw} and hw={self.hw} considered 'Error-Meaningless' on field : " + self.get_name())
            if (self.sw.value==Access.NA.value and self.hw.value==Access.RW.value) or \
               (self.sw==Access.NA and self.hw==Access.R):
                raise RuntimeError(
                    f"sw={self.sw} and hw={self.hw} considered 'Undefined' on field : " + self.get_name())
            if self.sw.value==Access.NA.value and self.hw.value==Access.W.value:
                raise RuntimeError(
                    f"sw={self.sw} and hw={self.hw} considered 'Unloaded net' on field : " + self.get_name())
            if self.sw.value==Access.NA.value and self.hw.value==Access.NA.value:
                raise RuntimeError(
                    f"sw={self.sw} and hw={self.hw} considered 'Non existent net' on field : " + self.get_name())
        if self.external:
            if self.cdc_out != Cdc.NA or self.cdc_swmod != Cdc.NA or self.cdc_swacc != Cdc.NA or\
               self.cdc_in != Cdc.NA:
                raise RuntimeError(
                    f"No support of CDC option on external field : " + self.get_name())
        if not self.has_storage() and self.cdc_out != Cdc.NA:
            raise RuntimeError(
                f"No support of CDC out option without storage for field : " + self.get_name())
        if not self.has_storage() and self.cdc_swmod != Cdc.NA:
            raise RuntimeError(
                f"No support of CDC swmod option without storage for field : " + self.get_name())
        if not self.has_storage() and self.cdc_swacc != Cdc.NA:
            raise RuntimeError(
                f"No support of CDC swacc option without storage for field : " + self.get_name())
        if self.singlepulse:
            if self.cdc_out != Cdc.NA and self.cdc_out != Cdc.HOST_CE:
                raise RuntimeError(
                    f"Using cdc_out={self.cdc_out} option with singlepulse is not supported on {self.get_name()}")
        if self.swmod and self.delayed_swmod:
            print (f"Warning: both swmod and delayed_swmod option used on {self.get_name()}")
            print (f"         This is deprecated use, please activate only one option")
            print (f"         Assuming delayed_swmod is needed.")
        if self.swmod:
            if self.cdc_swmod != Cdc.NA and self.cdc_swmod != Cdc.HOST_CE:
                print (f"Warning: using cdc_swmod option with swmod may be not supported on {self.get_name()}:cdc_swmod={self.cdc_swmod}")
                print (f"         Please double check the CDC on swmod signal on interface")
            if self.cdc_swmod == Cdc.HOST_CE and self.delayed_swmod:
                # delayed swmod and cdc out CE are 2 ways to generated swmod on internal :
                # required to select one or the other
                raise RuntimeError(
                    f"Delayed_swmod and cdc_swmod == Cdc.HOST_CE are exclusive options on field : " + self.get_name())
        if self.sw.is_readable() and self.swacc:
            if self.cdc_swacc != Cdc.NA and self.cdc_swacc != Cdc.HOST_CE:
                print (f"Warning: using cdc_swacc option with swacc may be not supported on {self.get_name()}:cdc_swacc={self.cdc_swacc}")
                print (f"         Please double check the CDC on swacc signal on interface")
        if self.hw.is_writable() and self.has_storage():
            if self.cdc_in != Cdc.NA and self.cdc_in != Cdc.HOST_CE:
                # writable storage have hwwe, this is not supported in full_async mode
                print (f"Warning: using cdc_in option with hwwe field may be not supported on {self.get_name()}:cdc_in={self.cdc_in}")
                print (f"         Please double check the CDC on hwwe signal on interface")
        if self.hwset or self.hwclr:
            if self.cdc_in != Cdc.NA and self.cdc_in != Cdc.HOST_CE:
                # hw writable storage have hwset/hwclr, this is not supported in full_async mode
                print (f"Warning: using cdc_in option with hwset/hwclr field may be not supported on {self.get_name()}:cdc_in={self.cdc_in}")
                print (f"         Please double check the CDC on hwset/hwclr signal on interface")
        # check reset type (None, 0, integer, or tuple of integer)
        if self.reset:
            if isinstance(self.reset, int) or isinstance(self.reset, tuple):
                pass
            else:
                raise RuntimeError(f"reset type unexpected for field : " + self.get_name())
            # Check all reset value (single value or table)
            reset_value_list = (self.reset,) if isinstance(self.reset, int) else self.reset
            for reset_value in reset_value_list:
                if reset_value < 0:
                    raise RuntimeError(f"{self.get_name()} reset expected as positive, found unsupported negative: {self.reset}")
                if reset_value.bit_length() > self.bit_width:
                    raise RuntimeError(f"{self.get_name()} reset={reset_value} too large (need {reset_value.bit_length()} bit)"\
                                       f" to fit field bitwidth ({self.bit_width} bit)")
        if self.encode:
            if not issubclass(self.encode, FieldEncode):
                raise RuntimeError(f"Field.encode for {self.name} is required to use FieldEncode (Please note IntEnum not supported anymore)"\
                                   f" encode={self.encode.__name__}:{self.encode.__bases__}")
            # Check that the encode bit width is matching the field bit width
            if self.encode.get_bit_length() > self.bit_width:
                raise RuntimeError(f"Field {self.name} bitwidth({self.bit_width}) too small to fit enum "\
                                   f"encode:{self.encode.__name__}, "\
                                   f"required : {self.encode.get_bit_length()} bit")
            elif self.encode.get_bit_length() < self.bit_width:
                print (f"Warning : field {self.name} is using {self.bit_width} bit where "\
                       f"only {self.encode.get_bit_length()} needed for encode:{self.encode.__name__}")


# ----------------------------------------------------------------------------

    def to_verilog_external(self, reg_name, map_table_entry):
        array_name_suffix = map_table_entry.get_suffix_array_name() # empty unless actual array in use
        fld_name = self.get_full_verilog_name(reg_name, array_name_suffix)

        if self.hwset and self.hwclr:
            raise RuntimeError("hwset and hwclr are exclusive on field : " + fld_name)
        # verilog_signal_swnext_name = fld_name + "_swnext"
        verilog = ""
        verilog += f"// --------------------------------------------------------------\n"
        verilog +=(f"// External Field in bit range of register : "
                       f"{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}\n")

        # field value to be read in register is field_name signal
        # generic name used to connect inside register (field either internal or external)
        verilog += f"logic [{self.bit_width - 1}:0] {fld_name};\n"
        # field value to be read in register is assigned in to_verilog_connect_interface
        if not self.sw.is_readable():
            # field is non readable, assign a default 0 value to replace non available external value
            verilog += f"assign {fld_name} = 0; // external non readable is tied to 0\n"

        if self.sw.is_writable():
            # signal for wdata at verilog implementation level (translate bit position in register to [*:0])
            verilog += f"logic [{self.bit_width - 1}:0] {fld_name}_extwdata;\n"
            verilog += f"assign {fld_name}_extwdata = sw_wrdata[{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}];\n"
            # (interface will connect directly to sw_wrdata)
            verilog += f"logic {fld_name}_extwr;\n"
            verilog += f"assign {fld_name}_extwr = {reg_name}_r_swwr;\n"
            if self.support_hwbe:
                # _wr_bmask provided as bitmask to external field
                verilog += f"logic [{self.bit_width - 1}:0] {fld_name}_extwr_bmask;\n"
                verilog += f"assign {fld_name}_extwr_bmask = host_access_wr_bmask[{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}];\n"

        # extready handling
        if (self.sw.is_readable() and self.extready_rd) or \
           (self.sw.is_writable() and self.extready_wr):
            # external register have either read or write ready option
            verilog += f"logic {fld_name}_extready_from_if; // signal connected from interface\n"
            verilog += f"logic {fld_name}_extready; // demuxed for applicable read/write\n"
            # Need demux for read or write cycle
            verilog += f"always_comb begin : proc_{fld_name}_extready\n"
            verilog += f"    {fld_name}_extready = 1; // default\n"
            if self.extready_rd:
                # force ready to 0 if !_extready && _extrd
                verilog += f"    if({reg_name}_r_swrd && !{fld_name}_extready_from_if)\n"
                verilog += f"        {fld_name}_extready = 0;\n"
            if self.extready_wr:
                # force ready to 0 if !_extready && _extwr
                verilog += f"    if({fld_name}_extwr && !{fld_name}_extready_from_if)\n"
                verilog += f"        {fld_name}_extready = 0;\n"
            verilog += f"end\n"

        verilog += f"logic {fld_name}_ready;\n" # ready either on read or write
        if self.hwce_signal != None:
            hwce_condition = f"{self.hwce_signal.name}"
        else:
            hwce_condition = "1"
        verilog += f"always_comb begin : proc_{fld_name}_ready\n"
        verilog += f"    {fld_name}_ready = 1; // ready unless found condition for not ready\n"
        # external r/w get ready condition from external, + may be mask with !hwce
        if (self.sw.is_readable() and self.extready_rd) or \
           (self.sw.is_writable() and self.extready_wr):
            verilog += f"    {fld_name}_ready = {fld_name}_ready && {hwce_condition} && {fld_name}_extready; // external ready\n"
        if (self.sw.is_readable() and not self.extready_rd) or \
           (self.sw.is_writable() and not self.extready_wr):
            verilog += f"    {fld_name}_ready = {fld_name}_ready && {hwce_condition}; // no extready, masked only with hwce\n"
        verilog += f"end\n"

        verilog += "\n"
        return verilog


    # // Typical storage implementation with most options:
    # logic [N:0] field; // current flop storage state
    # //
    # // hw related signals
    # logic [N:0] field_next; // next data from hardware
    # logic [N:0] field_hwset; // hwset from hardware (bitwise ored)
    # logic [N:0] field_hwclr; // hwclr from hardware (bitwise anded)
    # logic field_hwwe;
    # logic [N:0] field_hwnext; // next value for storage coming from hw
    # //
    # // sw related signals
    # // cpu wr data bus comes from sw_wrdata[N+L:L]
    # logic field_swwr; // wr pulse from cpu
    # logic field_swrd; // rd pulse from cpu
    # logic [N:0] field_swnext; // next value for storage coming from sw
    # // woclr, wzc is option to alter swwr effect
    # // woset, wzs is option to alter swwr effect
    # // wot, wzt is option to alter swwr effect
    # // wclr is option to alter swwr effect
    # // wset is option to alter swwr effect
    # //
    # // flop related signals
    # logic [N:0] field_ffnext;// next value for storage coming from either hwnext or swnext
    # // singlepulse activate auto return to 0 of flop signal the cycle after set
    # //
    # always_comb begin : proc_field_hwnext
    #     field_hwnext = field; // default
    #     // all 3 options are sequenced
    #     field_hwnext = (field_hwwe) ? field_next : field_hwnext; // normal hw write (trig by field_hwwe)
    #     field_hwnext = field_hwnext | field_hwset; // hwset
    #     field_hwnext = field_hwnext & ~field_hwclr; // hwclr
    # end
    # always_comb begin : proc_field_swnext
    #     // 3 exclusive options ...
    #     field_swnext = sw_wrdata[N+L:L]; // normal sw write : field_swwr
    #         field_swnext = 0; // rclr && field_swrd
    #         field_swnext = '1; // rset && field_swrd
    #     field_swnext = field | sw_wrdata[N+L:L]; // woset
    #     field_swnext = field & ~sw_wrdata[N+L:L]; // woclr
    #     field_swnext = field | ~sw_wrdata[N+L:L]; // wzs
    #     field_swnext = field & sw_wrdata[N+L:L]; // wzc
    #     field_swnext = '1; // wset
    #     field_swnext = 0; // wclr
    #     field_swnext = field ^ sw_wrdata[N+L:L]; // wot : toggle
    #     field_swnext = field ^ ~sw_wrdata[N+L:L]; // wzt : toggle
    # end
    # assign field_swwr = register_r_swwr; // field swwr comes from register level
    # always_comb begin : proc_field_ffnext
    #     // Default keep flop data unchanged (no sw wr, no hw write)
    #     field_ffnext = field;
    #     if(field_swwr) begin
    #         // data modification from SW
    #         field_ffnext = field_swnext;
    #     end else if (field_hwwe) begin
    #         // write data from HW
    #         field_ffnext = field_next;
    #     end
    # end
    # always_ff @(posedge clk or negedge rst_n) begin : proc_field
    #     if(~rst_n) begin
    #         field <= 0;
    #     end else begin
    #         field <= 0; // singlepulse auto reset to 0
    #         // Clock gate the flop write only if sw wr or hw wr
    #         if (field_swwr|| field_hwwe) begin
    #             field <= field_ffnext;
    #         end
    #     end
    # end


    def to_verilog_internal(self, reg_name, map_table_entry):
        array_name_suffix = map_table_entry.get_suffix_array_name() # empty unless actual array in use
        fld_name = self.get_full_verilog_name(reg_name, array_name_suffix)

        # in some side effect of field access, wait state can be added at register level
        # (hwce based field on slow hardware will postpone access to full register)
        # {reg_name}_r_ready is to be used to delay cycle on the field

        # hwce is not active if a slow hw is not available on every clock of the rif
        # the hwce signal is use to mask the field_ready condition to pause the host bus
        # This is needed only if the hw have special signal used (swmod, swacc)
        # a write to internal storage without swmod or swacc is not pausing the bus
        # hw change of storage (hwwe, hwset, hwclr) is also masked by hwce
        if self.hwce_signal != None:
            hwce_condition = f"{self.hwce_signal.name}"
            hwce_and_condition = f" && {self.hwce_signal.name}"
        else:
            hwce_condition = "1"
            hwce_and_condition = ""

        # Prepare reset info for Flop
        # This is used for core field, and also for cdc flop (in, out)
        if self.reset != None:
            # Field flops have a reset feature
            if self.resetsignal:
                # locate the signal in root regmap to get signal attribute
                rst_signal = self.parent.get_root_parent().get_signal_by_name(self.resetsignal)
            else:
                # no explicit reset signal for this field, use default reset
                rst_signal = self.parent.get_root_parent().default_reset_signal

            # collect reset info : reset_name and reset_edge
            # can be used for CDC or output flop insertion (share same reset scheme)
            if rst_signal.activelow:
                self.reset_name = f"~{rst_signal.name}"
                self.reset_edge = f" or negedge {rst_signal.name}"
            else:
                self.reset_name = f"{rst_signal.name}"
                self.reset_edge = f" or posedge {rst_signal.name}"
            if rst_signal.sync:
                self.reset_edge = "" # sync reset have no edge condition
                                # (only react to clock edge)

        if self.hwset and self.hwclr:
            print(f"Warning: hwset and hwclr are both active on field : " + fld_name)
            print(f"         Check current hwclr precedence over hwset is what you want.")
        # verilog_signal_swnext_name = fld_name + "_swnext"
        verilog = ""
        verilog += f"// --------------------------------------------------------------\n"
        verilog +=(f"// Field in bit range of internal register : "
                       f"{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}\n")
        verilog += f"logic [{self.bit_width-1}:0] {fld_name};\n"
        # Create parameter value for reset (create signal even if not reset used)
        # No storage can also use init value in tie to constant mode
        verilog += f"localparam [{self.bit_width}-1:0] {fld_name}_rst_value = {self.bit_width}'h{(self.get_reset_value(map_table_entry)):0x}; // reset value for storage\n"

        if self.hw.is_writable() and self.has_storage():
            verilog += f"logic [{self.bit_width - 1}:0] {fld_name}_next;\n"
            verilog += f"logic {fld_name}_hwwe;\n"
        if self.hw.is_writable() and not self.has_storage():
            verilog += f"logic [{self.bit_width - 1}:0] {fld_name}_next;\n"
        if self.sw.is_writable() or self.rclr or self.rset:
            verilog += f"logic [{self.bit_width - 1}:0] {fld_name}_swnext;\n"
            if self.swwe or self.swwel: # hardware enabling field modification
                verilog += f"logic {fld_name}_swwe;\n"
        if (self.has_storage() and self.hw.is_writable()) or self.hwset or self.hwclr:
            verilog += f"logic [{self.bit_width - 1}:0] {fld_name}_hwnext;\n"
        if self.hwset:
            verilog += f"logic [{self.bit_width - 1}:0] {fld_name}_hwset;\n"
        if self.hwclr:
            verilog += f"logic [{self.bit_width - 1}:0] {fld_name}_hwclr;\n"
        if self.sw.is_readable():
            verilog += f"logic {fld_name}_swrd;\n"
            verilog += f"assign {fld_name}_swrd = {reg_name}_r_swrd && {reg_name}_r_ready; // field read comes from register read\n"
            verilog += f"\n"
        if self.has_storage():
            verilog += f"// Field have storage\n"
            verilog += f"logic [{self.bit_width - 1}:0] {fld_name}_ffnext;\n"
            verilog += f"logic [{self.bit_width - 1}:0] {fld_name}_ffbmask; // for byte enable support\n"
            if self.hw.is_writable() or self.hwset or self.hwclr:
                # hwnext generation
                verilog += f"always_comb begin : proc_{fld_name}_hwnext\n"
                if self.latch:
                    # Avoid all form of feedback in case of latch (latch and hwset/hwclr is not valid combination)
                    verilog += f"    {fld_name}_hwnext = 0; // default to 0\n"
                else:
                    verilog += f"    {fld_name}_hwnext = {fld_name}; // default\n"
                # mux the hwwe if any
                if self.hw.is_writable():
                    verilog += f"    {fld_name}_hwnext = ({fld_name}_hwwe) ? {fld_name}_next : {fld_name}_hwnext; // normal hw write (trig by {fld_name}_hwwe)\n"
                if (self.hwset):
                    verilog += f"    {fld_name}_hwnext = {fld_name}_hwnext | {fld_name}_hwset; // hwset\n"
                if (self.hwclr):
                    verilog += f"    {fld_name}_hwnext = {fld_name}_hwnext & ~{fld_name}_hwclr; // hwclr\n"
                verilog += f"end\n"

            if (self.sw.is_writable() or self.rclr or self.rset):
                verilog += f"logic        {fld_name}_swwr;\n"
                verilog += f"assign {fld_name}_swwr = {reg_name}_r_swwr && {reg_name}_r_ready;\n"
                # _wr_bmask provided as bitmask to storage field
                verilog += f"logic [{self.bit_width - 1}:0] {fld_name}_swwr_bmask;\n"
                if self.support_hwbe:
                    verilog += f"assign {fld_name}_swwr_bmask = host_access_wr_bmask[{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}]; // support_hwbe : mask from byte strobe\n"
                else:
                    verilog += f"assign {fld_name}_swwr_bmask = '1; // No hw byte enable, all bit enable for write\n"
                # swnext generation
                verilog += f"always_comb begin : proc_{fld_name}_swnext\n"
                verilog += f"    {fld_name}_swnext = 0; // default to 0\n"
                if self.sw.is_writable() and \
                        not self.woset and not self.woclr and not self.wot and \
                        not self.wzs and not self.wzc and not self.wzt and \
                        not self.wset and not self.wclr:
                    verilog += f"    if({fld_name}_swwr)\n"
                    verilog += f"        {fld_name}_swnext = sw_wrdata[{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}]; // normal sw write\n"
                if self.sw.is_writable() and self.woset:
                    verilog += f"    {fld_name}_swnext = {fld_name} | sw_wrdata[{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}]; // woset\n"
                if self.sw.is_writable() and self.woclr:
                    verilog += f"    {fld_name}_swnext = {fld_name} & ~sw_wrdata[{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}]; // woclr\n"
                if self.sw.is_writable() and self.wot:
                    verilog += f"    {fld_name}_swnext = {fld_name} ^ sw_wrdata[{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}]; // wot\n"
                if self.sw.is_writable() and self.wzs:
                    verilog += f"    {fld_name}_swnext = {fld_name} | ~sw_wrdata[{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}]; // wzs\n"
                if self.sw.is_writable() and self.wzc:
                    verilog += f"    {fld_name}_swnext = {fld_name} & sw_wrdata[{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}]; // wzc\n"
                if self.sw.is_writable() and self.wzt:
                    verilog += f"    {fld_name}_swnext = {fld_name} ^ ~sw_wrdata[{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}]; // wzt\n"
                if self.sw.is_writable() and self.wset:
                    verilog += f"    {fld_name}_swnext = '1; // wset\n"
                if self.sw.is_writable() and self.wclr:
                    verilog += f"    {fld_name}_swnext = 0; // wclr\n"
                if (self.rclr):
                    verilog += f"    if({fld_name}_swrd) {fld_name}_swnext = 0; // rclr\n"
                if (self.rset):
                    verilog += f"    if({fld_name}_swrd) {fld_name}_swnext = '1; // rset\n"
                verilog += f"end\n"

            verilog_swwr = "    // swwr\n"
            if self.sw.is_writable(): # either swwr or woset or woclr or wot or
                                      # wzs or wzc or wzt or wset or wclr
                verilog_swwr += f"    if({fld_name}_swwr"
                if self.swwe or self.swwel:
                    verilog_swwr += f" && {fld_name}_swwe"
                verilog_swwr +=                         f") begin\n"
                verilog_swwr += f"        // data modification from SW write\n"
                verilog_swwr += f"        {fld_name}_ffnext = {fld_name}_swnext;\n"
                verilog_swwr += f"        {fld_name}_ffbmask = {fld_name}_swwr_bmask;\n"
                verilog_swwr += f"    end\n"
            if self.rclr or self.rset:
                verilog_swwr += f"    if({fld_name}_swrd"
                if self.swwe or self.swwel:
                    verilog_swwr += f" && {fld_name}_swwe"
                verilog_swwr +=                         f") begin\n"
                verilog_swwr += f"        // data modification from side effect read : rclr/rset\n"
                verilog_swwr += f"        {fld_name}_ffnext = {fld_name}_swnext;\n"
                #                         ffbmask is unchanged (read side effect is full 32 bit applied)
                verilog_swwr += f"    end\n"
            swwr_condition = []
            if self.sw.is_writable(): # either swwr or woset or woclr or wot or
                                      # wzs or wzc or wzt or wset or wclr
                if self.swwe or self.swwel:
                    swwr_condition.append(f"({fld_name}_swwr && {fld_name}_swwe)")
                else:
                    swwr_condition.append(f"{fld_name}_swwr")
            if self.rclr or self.rset:
                if self.swwe or self.swwel:
                    swwr_condition.append(f"({fld_name}_swrd && {fld_name}_swwe)")
                else:
                    swwr_condition.append(f"{fld_name}_swrd")
            swwr_condition = " || ".join(swwr_condition)

            hwwr_condition = []
            if self.hw.is_writable():
                hwwr_condition.append(f"{fld_name}_hwwe")
            if self.hwset:
                hwwr_condition.append(f"|{fld_name}_hwset")
            if self.hwclr:
                hwwr_condition.append(f"|{fld_name}_hwclr")
            hwwr_condition = " || ".join(hwwr_condition)

            verilog_hwwr = "    // hwwr\n"
            if self.hw.is_writable() or self.hwset or self.hwclr:
                verilog_hwwr += f"    if({hwwr_condition}) begin\n"
                verilog_hwwr += f"        // write data from HW\n"
                verilog_hwwr += f"        {fld_name}_ffnext = {fld_name}_hwnext;\n"
                verilog_hwwr += f"        {fld_name}_ffbmask = '1; // all bit to write for hw\n"
                verilog_hwwr += f"    end\n"


            # Generate the signal next for the field
            #     get either from swwr or hwwr, select order from precedence
            verilog += f"always_comb begin : proc_{fld_name}_ffnext\n"
            verilog += f"    // Default to SW, not used if no sw wr or no hw write\n"
            # default value used have impact on gate count in latch case
            # So try to have default value matching the swwr case, this help synthesis to optimize
            if self.sw.is_writable() or self.rclr or self.rset:
                verilog += f"    {fld_name}_ffnext = {fld_name}_swnext; // default to SW logic, overload in hw write case\n"
            else:
                verilog += f"    {fld_name}_ffnext = 0;\n // No default to SW logic available tie to 0"
            verilog += f"    {fld_name}_ffbmask = {fld_name}_swwr_bmask; // default to SW logic, overload in hw write case\n"
            if self.hw_have_wr_precedence_over_fw:
                # First fw then hw take precedence
                verilog += verilog_swwr
                verilog += verilog_hwwr
            else:
                # First hw then fw take precedence
                verilog += verilog_hwwr
                verilog += verilog_swwr
                #
            verilog += f"end\n"

            ff_condition = []
            if hwwr_condition != '':
                # mask the hwwr condition is hwce is not active
                ff_condition.append("(("+hwwr_condition+")"+hwce_and_condition+")")
            if swwr_condition != '':
                # hwce already included in swwr_condition
                ff_condition.append(swwr_condition)
            ff_condition = " || ".join(ff_condition)

            # Check error case for latch
            # must avoid any form of feedback from latch output to input
            if self.latch:
                if self.singlepulse:
                    raise RuntimeError(f"Unsupported singlepulse for latch based storage: {self.name}")
                if self.hwclr or self.hwset:
                    raise RuntimeError(f"Unsupported hwset/hwclr for latch based storage: {self.name}")
                if self.woclr or self.woset or self.wot:
                    raise RuntimeError(f"Unsupported wo* for latch based storage: {self.name}")
                if self.wzc or self.wzs or self.wzt:
                    raise RuntimeError(f"Unsupported wz* for latch based storage: {self.name}")
                print(f"Warning: using latch based storage for field: {self.name}")

            if self.latch and self.latch_use_lib_ldce:
                # storage is generated with latch as LDCE/LDPE lib primitive
                verilog += f"// /!\\ Field requested for LATCH implementation using LDCE lib !!!\n"
                for ff_bit in range(self.bit_width):
                    verilog += f"// bit {fld_name}_b{ff_bit}\n"
                    verilog += f"pyrift_latch_as_ldxe #(\n"
                    verilog += f"   .WIDTH(1),\n"
                    verilog += f"   .RESET_VALUE({fld_name}_rst_value[{ff_bit}])\n"
                    verilog += f") i_pyrift_latch_as_ldxe_{fld_name}_b{ff_bit} (\n"
                    if self.reset != None:
                        verilog += f"   .RESET({self.reset_name}),\n"
                    else:
                        verilog += f"   .RESET(1'b0), // No reset for this field\n"
                    verilog += f"   .G(clk=={self.latch_transparent_state}), "\
                                       f"// /!\\ latch is transparent if clk is {self.latch_transparent_state} !!!\n"
                    verilog += f"   .GE(({ff_condition}) && {fld_name}_ffbmask[{ff_bit}]), "\
                                       "// Clock gate the flop write only if sw wr or hw wr is active\n"
                    verilog += f"   .D({fld_name}_ffnext[{ff_bit}]),\n"
                    verilog += f"   .Q({fld_name}[{ff_bit}])\n"
                    verilog += f");\n"
            else:
                # storage is generated with verilog (either FF or latch)

                # Flop for the field
                if self.latch:
                    # Try to use latch for storage implementation
                    # could be better in IC for area implementation (/!\ put severe constraint on clocking)
                    verilog += f"// /!\\ Field requested for LATCH implementation !!!\n"
                    verilog += f"always_latch begin : proc_{fld_name}\n"
                    # sensitivity list is * because ffnext, or many hwwr/swwr condition can trig action
                else:
                    if self.reset != None:
                        # normal flop based implementation with reset in sensitivity
                        verilog += f"always_ff @(posedge clk{self.reset_edge}) begin : proc_{fld_name}\n"
                    else:
                        # normal flop based implementation with NO reset
                        verilog += f"// This flop is requested to have NO reset feature\n"
                        verilog += f"always_ff @(posedge clk) begin : proc_{fld_name}\n"

                # reset condition apply for both latch and flop case
                if self.reset != None:
                    verilog += f"    if({self.reset_name}) begin\n"
                    verilog += f"        {fld_name} <= {fld_name}_rst_value;\n"
                    verilog += f"    end else\n"

                verilog += f"    begin\n"
                if self.singlepulse:
                    if self.hwce_signal != None:
                        verilog += f"        if({hwce_condition}) // singlepulse auto reset only if hwce\n"
                    else:
                        verilog += f"        // if(1) // No hw ce condition, all clcok edge may reset pulse\n"
                    # if cdc_out is used (HOST_CE), the single pulse last until CDC use it
                    if self.cdc_out == Cdc.HOST_CE:
                        verilog += f"            if({self.cdc_out_signal.name}) {fld_name} <= 0; // singlepulse auto reset to 0 after CDC use it\n"
                    else:
                        verilog += f"            {fld_name} <= 0; // singlepulse auto reset to 0\n"

                verilog += f"        // Clock gate the flop write only if sw wr or hw wr is active\n"
                for ff_bit in range(self.bit_width):
                    verilog += f"        // bit {fld_name}_b{ff_bit}\n"
                    if self.latch:
                        verilog += f"        // /!\\ latch is transparent if clk is {self.latch_transparent_state} !!!\n"
                        verilog += f"        if (({ff_condition}) && {fld_name}_ffbmask[{ff_bit}] &&\n"
                        verilog += f"                (clk=={self.latch_transparent_state})) begin\n"
                    else:
                        # normal flop based implementation
                        verilog += f"        if ({ff_condition} && {fld_name}_ffbmask[{ff_bit}]) begin\n"
                    verilog += f"            {fld_name}[{ff_bit}] <= {fld_name}_ffnext[{ff_bit}];\n"
                    verilog += f"        end\n"

                verilog += f"    end\n"
                verilog += f"end\n"
        else: #  not self.has_storage()
            verilog += f"// Field have NO storage\n"
            if self.hw.is_writable():
                verilog += f"//No storage for the hw writable field : field = field_next in combinatorial\n"
                verilog += f"always_comb begin : proc_{fld_name}\n"
                verilog += f"    {fld_name} = {fld_name}_next;\n"
                verilog += f"end\n"
            else:
                # No storage for non hardware writable field :
                # this is a "Wire/Bus – constant value" as of systemRdl
                verilog += f"//No storage for constant field : field = reset value in combinatorial\n"
                verilog += f"always_comb begin : proc_{fld_name}\n"
                verilog += f"    {fld_name} = {fld_name}_rst_value;\n"
                verilog += f"end\n"

        verilog += f"logic {fld_name}_ready;\n" # ready either on read or write

        verilog += f"always_comb begin : proc_{fld_name}_ready\n"
        if(self.swmod or self.swacc):
            # wait state on host bus only needed is swmod or swacc is used
            verilog += f"    {fld_name}_ready = {hwce_condition}; // swacc/swmod : ready is copy of hwce\n"
        else:
            verilog += f"    {fld_name}_ready = 1; // no wait state without swmod or swacc\n"
        verilog += f"end\n"

        verilog += "\n"
        return verilog

    def to_verilog(self, reg_name, map_table_entry):
        if self.external:
            return self.to_verilog_external(reg_name, map_table_entry)
        else:
            return self.to_verilog_internal(reg_name, map_table_entry)


# ----------------------------------------------------------------------------

    # interface content in case the register is external
    # register is external:
    # interface if sw access is writable
    #   logic field_extwr; // wr pulse from cpu
    #   logic [N:0] field_extwdata; // data from cpu, valid on field_swwr
    # interface if sw access is readable
    # logic field_extrd; // rd pulse from cpu
    # logic [N:0] field_extrdata; // data to cpu, needed valid on field_swrd

    def to_verilog_interface_external(self, reg_name, array_range_definition, if_name, parent_regmap):
        # generate the verilog body of interface
        fld_name = self.get_full_verilog_name(reg_name,'') # array_name_suffix='' real array in interface
        verilog = ""
        if_bit_width = self.bit_width

        # print(f"to_interface external {self.name} reshape={self.if_reshape}")
        extra_dim = ""
        if self.if_reshape:
            # so far support only single extra dimension
            reshape_size = self.if_reshape[0]
            if if_bit_width % reshape_size != 0:
                raise RuntimeError(f"vector dimension is not multiple of reshape size in {self.name}")
            if_bit_width //= reshape_size
            extra_dim = f"[{reshape_size-1}:0]"
            # print(f"to_interface external {self.name} extra_dim={extra_dim} ifw={if_bit_width}")

        if self.multipart_if_pattern or self.multipart_if_slave:
            raise RuntimeError("multipart is not supported for external field")
        if self.encode:
            if self.sw.is_readable():
                verilog += f"      Tenum_{self.encode.__name__} {array_range_definition} {fld_name}_extrdata;\n"
                parent_regmap.interface_io_list.append( {
                    "rif_out_dir" : False,
                    "name" : f"{fld_name}_extrdata",
                    "if_name" : if_name,
                    "type" : f"Tenum_{self.encode.__name__} {array_range_definition}"
                })
            if self.sw.is_writable():
                verilog += f"      Tenum_{self.encode.__name__} {array_range_definition} {fld_name}_extwdata;\n"
                parent_regmap.interface_io_list.append( {
                    "rif_out_dir" : True,
                    "name" : f"{fld_name}_extwdata",
                    "if_name" : if_name,
                    "type" : f"Tenum_{self.encode.__name__} {array_range_definition}"
                })
        elif(self.bit_width <= 1):
            if self.sw.is_readable():
                verilog += f"      logic {array_range_definition} {fld_name}_extrdata;\n"
                parent_regmap.interface_io_list.append( {
                    "rif_out_dir" : False,
                    "name" : f"{fld_name}_extrdata",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}"
                })
            if self.sw.is_writable():
                verilog += f"      logic {array_range_definition} {fld_name}_extwdata;\n"
                parent_regmap.interface_io_list.append( {
                    "rif_out_dir" : True,
                    "name" : f"{fld_name}_extwdata",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}"
                })
        else:
            # more than 1 bit in the field, reshape support even for external register
            if self.sw.is_readable():
                verilog += f"      logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim} {fld_name}_extrdata;\n"
                parent_regmap.interface_io_list.append( {
                    "rif_out_dir" : False,
                    "name" : f"{fld_name}_extrdata",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim}"
                })
            if self.sw.is_writable():
                verilog += f"      logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim} {fld_name}_extwdata;\n"
                parent_regmap.interface_io_list.append( {
                    "rif_out_dir" : True,
                    "name" : f"{fld_name}_extwdata",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim}"
                })
        if self.sw.is_writable() and self.support_hwbe:
            # wr_bmask, ignore encode option, just consider bit width
            if(self.bit_width <= 1):
                if_field_type = f"logic {array_range_definition}"
            else:
                if_field_type = f"logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim}"
            verilog += f"      {if_field_type} {fld_name}_extwr_bmask; // hwbe supported\n"
            parent_regmap.interface_io_list.append( {
                "rif_out_dir" : True,
                "name" : f"{fld_name}_extwr_bmask",
                "if_name" : if_name,
                "type" : f"{if_field_type}"
            })

        if self.hw != Access.NA:
            raise RuntimeError(
                f"external register should have hw=Access.NA setup. Non consistent setup found in {self.name} "\
                f"(unexpect hw={self.hw} found !?)")

        if self.sw.is_readable():
            # only external register with read access is interested in read pulse
            verilog += f"      logic {array_range_definition} {fld_name}_extrd; // read pulse for readable external field\n"
            parent_regmap.interface_io_list.append( {
                "rif_out_dir" : True,
                "name" : f"{fld_name}_extrd",
                "if_name" : if_name,
                "type" : f"logic {array_range_definition}"
            })
        if self.sw.is_writable():
            verilog += f"      logic {array_range_definition} {fld_name}_extwr; // write pulse for writable external field\n\n"
            parent_regmap.interface_io_list.append( {
                "rif_out_dir" : True,
                "name" : f"{fld_name}_extwr",
                "if_name" : if_name,
                "type" : f"logic {array_range_definition}"
            })


        if self.extready_rd and not self.sw.is_readable():
            raise RuntimeError(
                f"'extready_rd' found without sw read access in {self.name} "\
                f"(sw as Access.R or Access.RW expected !?)")
        if self.extready_wr and not self.sw.is_writable():
            raise RuntimeError(
                f"'extready_wr' found without sw write access in {self.name} "\
                f"(sw as Access.W or Access.RW expected !?)")

        if (self.sw.is_readable() and self.extready_rd) or \
           (self.sw.is_writable() and self.extready_wr):
            # external register have either read or write ready option
            verilog += f"      logic {array_range_definition} {fld_name}_extready; // ready on _extrd/_extwr\n"
            parent_regmap.interface_io_list.append( {
                "rif_out_dir" : False,
                "name" : f"{fld_name}_extready",
                "if_name" : if_name,
                "type" : f"logic {array_range_definition}"
            })

        return verilog

    def to_verilog_interface_internal(self, reg_name, array_range_definition, if_name, parent_regmap):
        """\
            Generate field signal in verilog hw interface
            following signal are handled as single bit or vector[] root link shown
            (in multipart, root connection is special)
              ---------------------------------------------------------
              FIELD rtl level   # multi IF level (root)   # multi IF for slave
              ---------------------------------------------------------
            - field[:0]         |       root[:N]          |   Nothing
            - field_next[:0]    |       root_next[:N]     |   Nothing
            - field_hwset[:0]   |       root_hwset[:N]    |   Nothing
            - field_hwclr[:0]   |       root_hwclr[:N]    |   Nothing
            - field_hwwe        |       root_hwwe         |   Nothing
            - field_swmod       |       field_swmod       |   field_swmod
            - field_swacc       |       field_swacc       |   field_swacc
            if signal can be reshaped: .if_reshape = (2,) get [n-1:0][1:0]if_signal
            (from a 2*n packed field, possibly multipart)
        """
        # generate the verilog body of interface
        fld_name = self.get_full_verilog_name(reg_name,'')# array_name_suffix='' real array in interface
        verilog = ""
        if_bit_width = self.bit_width
        if self.multipart_if_pattern:
            # root field of multipart is found
            if self.encode:
                raise RuntimeError("multipart is not supported for enum encode field")
            if self.bit_width <= 1:
                raise RuntimeError("multipart is not supported for single bit master part")
            multipart_info = parent_regmap.get_field_multipart_info(
                pattern=self.multipart_if_pattern,
                root_field=self
                )
            #print (fld_name + " found as multipart in " + parent_regmap.name)
            #print ("lookup multipart field now:",[f.name+":"+str(f.bit_width) for f in multipart_info])
            if_bit_width = multipart_info['bit_width']
            root_name = multipart_info['root']
            #print ("found multipart bitwidth:",if_bit_width)

        extra_dim = ""
        if self.if_reshape:
            # so far support only single extra dimension
            reshape_size = self.if_reshape[0]
            if if_bit_width % reshape_size != 0:
                raise RuntimeError(f"vector dimension is not multiple of reshape size in {self.name}")
            if_bit_width //= reshape_size
            extra_dim = f"[{reshape_size-1}:0]"


        # --------------------------------------------------------------------
        # signal generation for internal field
        # verilog += f"// wr={self.hw.is_writable()}\n"
        if self.encode:
            if self.hw.is_readable():
                verilog += f"      Tenum_{self.encode.__name__} {array_range_definition} {fld_name};\n"
                parent_regmap.interface_io_list.append( {
                    "rif_out_dir" : True,
                    "name" : f"{fld_name}",
                    "if_name" : if_name,
                    "type" : f"Tenum_{self.encode.__name__} {array_range_definition}"
                })
            if self.hw.is_writable():
                verilog += f"      Tenum_{self.encode.__name__} {array_range_definition} {fld_name}_next;\n"
                parent_regmap.interface_io_list.append( {
                    "rif_out_dir" : False,
                    "name" : f"{fld_name}_next",
                    "if_name" : if_name,
                    "type" : f"Tenum_{self.encode.__name__} {array_range_definition}"
                })
        elif(self.bit_width <= 1):
            if self.hw.is_readable():
                verilog += f"      logic {array_range_definition} {fld_name};\n"
                parent_regmap.interface_io_list.append( {
                    "rif_out_dir" : True,
                    "name" : f"{fld_name}",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}"
                })
            if self.hw.is_writable():
                verilog += f"      logic {array_range_definition} {fld_name}_next;\n"
                parent_regmap.interface_io_list.append( {
                    "rif_out_dir" : False,
                    "name" : f"{fld_name}_next",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}"
                })
        elif self.multipart_if_pattern:
            if self.hw.is_readable():
                verilog += f"      logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim} {root_name};\n"
                parent_regmap.interface_io_list.append( {
                    "rif_out_dir" : True,
                    "name" : f"{root_name}",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim}"
                })
            if self.hw.is_writable():
                verilog += f"      logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim} {root_name}_next;\n"
                parent_regmap.interface_io_list.append( {
                    "rif_out_dir" : False,
                    "name" : f"{root_name}_next",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim}"
                })
        elif self.multipart_if_slave:
            # slave will attach inside multipart root
            verilog = f"      // multipart slave skipped : {fld_name}\n"
        else:
            if self.hw.is_readable():
                verilog += f"      logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim} {fld_name};\n"
                parent_regmap.interface_io_list.append( {
                    "rif_out_dir" : True,
                    "name" : f"{fld_name}",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim}"
                })
            if self.hw.is_writable():
                verilog += f"      logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim} {fld_name}_next;\n"
                parent_regmap.interface_io_list.append( {
                    "rif_out_dir" : False,
                    "name" : f"{fld_name}_next",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim}"
                })
        # signal hwset and hwclr declaration : enum encoded field are kept as logic vector
        # as bitwise set or clr don't make much sense for enum field
        # (likely an enum based field would not have hwset/hwclr, but if any keep raw logic definition)
        if(self.bit_width <= 1):
            if self.hwset:
                verilog += f"      logic {array_range_definition} {fld_name}_hwset;\n"
                parent_regmap.interface_io_list.append( {
                    "rif_out_dir" : False,
                    "name" : f"{fld_name}_hwset",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}"
                })
            if self.hwclr:
                verilog += f"      logic {array_range_definition} {fld_name}_hwclr;\n"
                parent_regmap.interface_io_list.append( {
                    "rif_out_dir" : False,
                    "name" : f"{fld_name}_hwclr",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}"
                })
        elif self.multipart_if_pattern:
            if (self.hwset):
                verilog += f"      logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim} {root_name}_hwset;\n"
                parent_regmap.interface_io_list.append( {
                    "rif_out_dir" : False,
                    "name" : f"{root_name}_hwset",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim}"
                })
            if (self.hwclr):
                verilog += f"      logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim} {root_name}_hwclr;\n"
                parent_regmap.interface_io_list.append( {
                    "rif_out_dir" : False,
                    "name" : f"{root_name}_hwclr",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim}"
                })
        elif self.multipart_if_slave:
            # slave will attach inside multipart root
            verilog = f"      // multipart slave hwset/hwclr skipped : {fld_name}\n"
        else:
            if (self.hwset):
                verilog += f"      logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim} {fld_name}_hwset;\n"
                parent_regmap.interface_io_list.append( {
                    "rif_out_dir" : False,
                    "name" : f"{fld_name}_hwset",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim}"
                })
            if (self.hwclr):
                verilog += f"      logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim} {fld_name}_hwclr;\n"
                parent_regmap.interface_io_list.append( {
                    "rif_out_dir" : False,
                    "name" : f"{fld_name}_hwclr",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim}"
                })
        if (self.hw.is_writable() and self.has_storage()):
            if self.multipart_if_pattern:
                verilog += f"      logic {array_range_definition} {root_name}_hwwe;\n"
                parent_regmap.interface_io_list.append( {
                    "rif_out_dir" : False,
                    "name" : f"{root_name}_hwwe",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}"
                })
            elif self.multipart_if_slave:
                # slave will attach inside multipart root
                verilog = f"      // multipart slave hwwe skipped : {fld_name}\n"
                pass # slave will attach inside multipart root
            else:
                verilog += f"      logic {array_range_definition} {fld_name}_hwwe;\n"
                parent_regmap.interface_io_list.append( {
                    "rif_out_dir" : False,
                    "name" : f"{fld_name}_hwwe",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}"
                })
        # swmod and swacc are not altered by multipart
        if (self.swmod or self.delayed_swmod):
            verilog += f"      logic {array_range_definition} {fld_name}_swmod;\n"
            parent_regmap.interface_io_list.append( {
                "rif_out_dir" : True,
                "name" : f"{fld_name}_swmod",
                "if_name" : if_name,
                "type" : f"logic {array_range_definition}"
            })
        if (self.swacc):
            verilog += f"      logic {array_range_definition} {fld_name}_swacc;\n"
            parent_regmap.interface_io_list.append( {
                "rif_out_dir" : True,
                "name" : f"{fld_name}_swacc",
                "if_name" : if_name,
                "type" : f"logic {array_range_definition}"
            })
        if self.swwe or self.swwel: # hardware enabling field modification
            if not(self.sw.is_writable() or self.rclr or self.rset):
                raise RuntimeError(f"found swwe option for field  {fld_name} that is"\
                                   f" not sw writable, nor rclr , nor rset ?")
        if self.swwe: # hardware enabling field modification
            verilog += f"      logic {array_range_definition} {fld_name}_swwe;\n"
            parent_regmap.interface_io_list.append( {
                "rif_out_dir" : False,
                "name" : f"{fld_name}_swwe",
                "if_name" : if_name,
                "type" : f"logic {array_range_definition}"
            })
        if self.swwel: # hardware enabling field modification (active low)
            verilog += f"      logic {array_range_definition} {fld_name}_swwel;\n"
        verilog += "\n"
        return verilog

    def to_verilog_interface(self, reg_name, array_range_definition, if_name, parent_regmap):
        if self.external:
            return self.to_verilog_interface_external(
                reg_name=reg_name,
                array_range_definition=array_range_definition,
                if_name=if_name,
                parent_regmap=parent_regmap)
        else:
            return self.to_verilog_interface_internal(
                reg_name=reg_name,
                array_range_definition=array_range_definition,
                if_name=if_name,
                parent_regmap=parent_regmap)

# ----------------------------------------------------------------------------

    def to_verilog_connect_interface_external(self, reg_name, parent_regmap, map_table_entry):
        array_name_suffix = map_table_entry.get_suffix_array_name()
        array_index_bracket = map_table_entry.get_array_index_bracket()
        fld_name = self.get_full_verilog_name(reg_name, array_name_suffix)
        fld_name_in_if = self.get_full_verilog_name(reg_name, '') # no array extension
        if_portname = self.get_interface_name().lower()
        verilog = ""

        reshape_size = 1
        if self.if_reshape:
            # so far support only single extra dimension
            reshape_size = self.if_reshape[0]

        verilog += (f"    // External Field interface connection : "
                       f"{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}\n")
        if self.sw.is_readable():
            # field value to be read in register is field_name signal
            # generic name used to connect inside register (field either internal or external)
            # no multipart support for external (just reshaping)
            verilog += f"      assign {fld_name} =\n"
            if self.encode:
                # internal signal is bit vector (interface is enum : cast needed)
                verilog += f"          {self.bit_width}'({if_portname}.{fld_name_in_if}_extrdata{array_index_bracket}); // cast enum to proper bitwidth integer\n"
            elif self.bit_width <= 1:
                verilog += f"          {if_portname}.{fld_name_in_if}_extrdata{array_index_bracket};\n"
            else:
                # non multipart vector
                verilog += f"          {if_portname}.{fld_name_in_if}_extrdata{array_index_bracket}[{self.bit_width//reshape_size-1}:0];\n"
            # external hw implementation requires the read pulse
            verilog += f"      assign {if_portname}.{fld_name_in_if}_extrd{array_index_bracket} = {reg_name}{array_name_suffix}_r_swrd;\n"

        if self.sw.is_writable():
            if self.bit_width <= 1:
                verilog += f"      assign {if_portname}.{fld_name_in_if}_extwdata{array_index_bracket} =\n"
            else:
                # non multipart vector
                verilog += f"      assign {if_portname}.{fld_name_in_if}_extwdata{array_index_bracket}[{self.bit_width//reshape_size-1}:0] =\n"
            if self.encode:
                # internal signal is vector to be cast to enum
                verilog += f"          Tenum_{self.encode.__name__}'({fld_name}_extwdata);\n"
            else:
                verilog += f"          {fld_name}_extwdata;\n"
            verilog += f"      assign {if_portname}.{fld_name_in_if}_extwr{array_index_bracket} = {fld_name}_extwr;\n"
            if self.support_hwbe:
                verilog += f"      assign {if_portname}.{fld_name_in_if}_extwr_bmask{array_index_bracket} = {fld_name}_extwr_bmask;\n"

        if (self.sw.is_readable() and self.extready_rd) or \
           (self.sw.is_writable() and self.extready_wr):
            # external register have _extready signal (used either for read or write)
            verilog += f"      assign {fld_name}_extready_from_if = {if_portname}.{fld_name_in_if}_extready{array_index_bracket};\n"

        verilog += "\n"
        return verilog

    def to_verilog_connect_interface_internal(self, reg_name, parent_regmap, map_table_entry):
        array_name_suffix = map_table_entry.get_suffix_array_name()
        array_index_bracket = map_table_entry.get_array_index_bracket()
        fld_name = self.get_full_verilog_name(reg_name, array_name_suffix)
        fld_name_in_if = self.get_full_verilog_name(reg_name, '') # no array extension
        # name in interface will be like  {if_name}.{fld_name_in_if}_swmod{array_index_bracket}
        if_portname = self.get_interface_name().lower()
        verilog = ""

        if_bit_width = self.bit_width
        if self.multipart_if_slave or self.multipart_if_pattern:
            # field is part of a multipart field
            # The field part are to be connected to interface at root signal level
            if self.encode:
                raise RuntimeError("multipart is not supported for enum encode field")
            if self.bit_width <= 1:
                raise RuntimeError("multipart is not supported for single bit master part")
            root_field = parent_regmap.get_root_multipart_from_slave(self)
            #print(f"Found {root_field.name} as root for slave: {self.name}")
            multipart_info = parent_regmap.get_field_multipart_info(
                pattern=root_field.multipart_if_pattern,
                root_field=root_field
                )
            if len(multipart_info['field_list']) == 0:
                raise RuntimeError(f"multipart field not found to match pattern<{self.multipart_if_pattern}> in {fld_name}")

            # now we have the multipart root signal info
            if_bit_width = multipart_info['bit_width']

            # locate the bit position of current field inside the multipart
            #print(f"Process multipart field connection for {self.name} to root={multipart_info['root']}")
            multipart_bit_lsb = 0
            for field in multipart_info['field_list']:
                if field == self:
                    # found our place in slave sequence
                    # reach the current lsb position in multipart_bit_lsb
                    break # exit for loop
                else:
                    multipart_bit_lsb += field.bit_width
            else: # for loop exit without break found
                raise RuntimeError(f"slave field {self.name} not found in {multipart_info['field_list']}")

            # print (f"slave field {self.name} found in lsb={multipart_bit_lsb}")
            root_name = multipart_info['root']

        # Now multipart info is fetch, try to generate the connection to interface

        reshape_size = 1
        # reshape condition is either from current field, or from root field if multipart element
        reshape_condition = self.if_reshape
        if (self.multipart_if_slave or self.multipart_if_pattern):
            reshape_condition = root_field.if_reshape

        if reshape_condition:
            # so far support only single extra dimension
            reshape_size = reshape_condition[0]
            if self.bit_width % reshape_size != 0:
                raise RuntimeError(f"vector dimension is not multiple of reshape size in {self.name}")
            if      (self.multipart_if_slave or self.multipart_if_pattern) and\
                    multipart_bit_lsb % reshape_size != 0:
                raise RuntimeError(f"multipart offset is not multiple of reshape size in {self.name}")

        # TODO: check how to handle array case and multipart field at same time

        # get field type expression (single item for array) for verilog coding "logic", "logic [...]"
        if self.multipart_if_slave or self.multipart_if_pattern:
            fld_type = f"logic [{self.bit_width-1}:0]"
        elif self.bit_width <= 1:
            fld_type = f"logic"
        else:
            # non multipart vector
            fld_type = f"logic [{self.bit_width-1}:0]"

        if self.hw.is_readable():
            verilog += f"      // fld_type={fld_type}\n"
            if self.cdc_out == Cdc.NA:
                # Non cdc, cdc field named is falling back to direct field name without cdc
                cdc_name = fld_name
            elif  self.cdc_out == Cdc.HOST_CE:
                if self.cdc_out_signal == None:
                    raise RuntimeError(f"HOST_CE cdc_out option on {fld_name} requires a valid cdc_out_signal to resync output")
                cdc_name = fld_name + "_cdc"
                verilog += f"      {fld_type} {cdc_name}; // cdc resync signal declaration\n"
                verilog += f"      // Cdc output is resync with flop.\n"
                if self.reset != None:
                    # normal flop based implementation with reset in sensitivity
                    verilog += f"      always_ff @(posedge clk{self.reset_edge}) begin : proc_{fld_name}_cdc_flop\n"
                else:
                    # normal flop based implementation with NO reset
                    verilog += f"      // This flop is requested to have NO reset feature\n"
                    verilog += f"      always_ff @(posedge clk) begin : proc_{fld_name}_cdc_flop\n"
                # reset condition if needed
                if self.reset != None:
                    verilog += f"         if({self.reset_name}) begin\n"
                    verilog += f"            // cdc reset value use same as field\n"
                    verilog += f"            {cdc_name} <= {fld_name}_rst_value;\n"
                    verilog += f"         end else\n"

                verilog += f"         if({self.cdc_out_signal.name}) begin\n"
                verilog += f"             {cdc_name} <= {fld_name};\n"
                verilog += f"         end\n"
                verilog += f"      end\n"
            elif  self.cdc_out == Cdc.FULL_ASYNC:
                if self.cdc_out_signal == None:
                    raise RuntimeError(f"FULL_ASYNC cdc_out option on {fld_name} requires a valid cdc_out_signal for resync clock of output")
                cdc_name = fld_name + "_cdc"
                meta_name = fld_name + "_meta"
                verilog += f"      {fld_type} {meta_name}; // metastable first stage cdc resync signal declaration\n"
                verilog += f"      {fld_type} {cdc_name}; // cdc resync signal declaration\n"
                verilog += f"      // Cdc output is resync with 2 stage of flop.\n"
                if self.reset != None:
                    # normal flop based implementation with reset in sensitivity
                    verilog += f"      always_ff @(posedge {self.cdc_out_signal.name}{self.reset_edge}) begin : proc_{fld_name}_fasync_flop\n"
                else:
                    # normal flop based implementation with NO reset
                    verilog += f"      // This flop is requested to have NO reset feature\n"
                    verilog += f"      always_ff @(posedge {self.cdc_out_signal.name}) begin : proc_{fld_name}_fasync_flop\n"
                # reset condition if needed
                if self.reset != None:
                    verilog += f"         if({self.reset_name}) begin\n"
                    verilog += f"            // cdc reset value use same as field\n"
                    verilog += f"            {meta_name} <= {fld_name}_rst_value;\n"
                    verilog += f"            {cdc_name} <= {fld_name}_rst_value;\n"
                    verilog += f"         end else\n"
                verilog += f"         begin\n"
                verilog += f"            {meta_name} <= {fld_name}; // first stage CDC flop\n"
                verilog += f"            {cdc_name} <= {meta_name}; // second stage CDC flop\n"
                verilog += f"         end\n"
                verilog += f"      end\n"
            else:
                raise RuntimeError(f"Unexpected Cdc_out option option on {fld_name}:cdc_out={self.cdc_out}")

            if self.multipart_if_slave or self.multipart_if_pattern:
                verilog += f"      assign {if_portname}.{root_name}"+\
                           f"[{self.bit_width//reshape_size+multipart_bit_lsb//reshape_size-1}:"+\
                           f"{multipart_bit_lsb//reshape_size}] =\n"
            elif self.bit_width <= 1:
                verilog += f"      assign {if_portname}.{fld_name_in_if}{array_index_bracket} =\n"
            else:
                # non multipart vector
                verilog += f"      assign {if_portname}.{fld_name_in_if}{array_index_bracket}"+\
                                 f"[{self.bit_width//reshape_size-1}:0] =\n"
            if self.encode:
                # internal signal is vector to be cast to enum
                verilog += f"          Tenum_{self.encode.__name__}'({cdc_name});\n"
            else:
                verilog += f"          {cdc_name};\n"
        # swmod and swacc are not altered by multipart
        if (self.swmod or self.delayed_swmod):
            swmod_condition = []
            if self.has_storage() and self.sw.is_writable():
                if self.support_hwbe:
                    # add condition from byte enable swmod
                    # any bit from wr enabled bmask is required to enable swmod generation
                    swmod_condition.append(f"({fld_name}_swwr && |{fld_name}_swwr_bmask)")
                else:
                    swmod_condition.append(f"{fld_name}_swwr")
            # assert swmod if read and rclr or rset (read have modifying side effect)
            if self.has_storage() and self.rclr or self.rset:
                swmod_condition.append(f"{fld_name}_swrd)")
            if len(swmod_condition)==0:
                swmod_condition.append('0')
            swmod_condition = " || ".join(swmod_condition)
            verilog += f"      logic {fld_name}_swmod_nodelay;\n"
            verilog += f"      assign {fld_name}_swmod_nodelay = {swmod_condition};\n"

            if self.delayed_swmod:
                verilog += f"      // delayed swmod output to interface.\n"
                if self.reset != None:
                    # normal flop based implementation with reset in sensitivity
                    verilog += f"      always_ff @(posedge clk{self.reset_edge}) begin : proc_{fld_name}_swmod_delay\n"
                else:
                    # normal flop based implementation with NO reset
                    verilog += f"      // This flop is requested to have NO reset feature\n"
                    verilog += f"      always_ff @(posedge clk) begin : proc_{fld_name}_swmod_delay\n"
                # reset conditionif applicable
                if self.reset != None:
                    verilog += f"         if({self.reset_name}) begin\n"
                    verilog += f"            {if_portname}.{fld_name_in_if}_swmod{array_index_bracket} <= 0;\n"
                    verilog += f"         end else\n"

                verilog += f"         begin\n"
                if self.hwce_signal != None:
                    # if hwce is in use, the clocking delay should be gated by hwce
                    verilog += f"            if({self.hwce_signal.name})\n"
                verilog += f"               {if_portname}.{fld_name_in_if}_swmod{array_index_bracket} <= {swmod_condition};\n"
                verilog += f"         end\n"
                verilog += f"      end // always ff\n"
            elif self.cdc_swmod == Cdc.HOST_CE:
                if self.cdc_out_signal == None:
                    raise RuntimeError(f"HOST_CE cdc_swmod option on {fld_name}+swmod requires a valid cdc_out_signal to resync output")
                verilog += f"      // HOST_CE cdc_swmod swmod output to interface.\n"
                verilog += f"      logic {fld_name}_swmod_pending; // record swmod until cdc CE\n"
                if self.reset != None:
                    # normal flop based implementation with reset in sensitivity
                    verilog += f"      always_ff @(posedge clk{self.reset_edge}) begin : proc_{fld_name}_swmod_pending\n"
                else:
                    # normal flop based implementation with NO reset
                    verilog += f"      // This flop is requested to have NO reset feature\n"
                    verilog += f"      always_ff @(posedge clk) begin : proc_{fld_name}_swmod_pending\n"
                # reset condition if applicable
                if self.reset != None:
                    verilog += f"         if({self.reset_name}) begin\n"
                    verilog += f"            {fld_name}_swmod_pending <= 0;\n"
                    verilog += f"            {if_portname}.{fld_name_in_if}_swmod{array_index_bracket} <= 0;\n"
                    verilog += f"         end else\n"
                verilog += f"         begin\n"
                verilog += f"            if({fld_name}_swmod_nodelay)\n" # precedence is to re-arm pending rather than reset
                verilog += f"                 {fld_name}_swmod_pending <= 1;\n"
                verilog += f"            else if({self.cdc_out_signal.name})\n"
                verilog += f"                 {fld_name}_swmod_pending <= 0;\n"
                verilog += f"            if({self.cdc_out_signal.name})\n"
                verilog += f"               {if_portname}.{fld_name_in_if}_swmod{array_index_bracket} <= {fld_name}_swmod_pending;\n"
                verilog += f"         end\n"
                verilog += f"      end\n"
            else:
                # if hwce is in use, the swmod_condition already include the gating
                verilog += f"      assign {if_portname}.{fld_name_in_if}_swmod{array_index_bracket} = {fld_name}_swmod_nodelay;\n"
        # swmod and swacc are not altered by multipart
        if (self.swacc):
            if(self.sw.is_readable()):
                if self.cdc_swacc == Cdc.HOST_CE:
                    if self.cdc_out_signal == None:
                        raise RuntimeError(f"HOST_CE cdc_swacc option on {fld_name}+swacc requires a valid cdc_out_signal to resync output")
                    verilog += f"      // HOST_CE cdc_swacc swacc output to interface.\n"
                    verilog += f"      logic {fld_name}_swacc_pending; // record swacc until cdc CE\n"
                    if self.reset != None:
                        # normal flop based implementation with reset in sensitivity
                        verilog += f"      always_ff @(posedge clk{self.reset_edge}) begin : proc_{fld_name}_swacc_pending\n"
                    else:
                        # normal flop based implementation with NO reset
                        verilog += f"      // This flop is requested to have NO reset feature\n"
                        verilog += f"      always_ff @(posedge clk) begin : proc_{fld_name}_swacc_pending\n"
                    # reset condition if applicable
                    if self.reset != None:
                        verilog += f"         if({self.reset_name}) begin\n"
                        verilog += f"            {fld_name}_swacc_pending <= 0;\n"
                        verilog += f"            {if_portname}.{fld_name_in_if}_swacc{array_index_bracket} <= 0;\n"
                        verilog += f"         end else\n"
                    verilog += f"         begin\n"
                    verilog += f"            if({fld_name}_swrd)\n" # precedence is to re-arm pending rather than reset
                    verilog += f"                 {fld_name}_swacc_pending <= 1;\n"
                    verilog += f"            else if({self.cdc_out_signal.name})\n"
                    verilog += f"                 {fld_name}_swacc_pending <= 0;\n"
                    verilog += f"            if({self.cdc_out_signal.name})\n"
                    verilog += f"               {if_portname}.{fld_name_in_if}_swacc{array_index_bracket} = {fld_name}_swacc_pending;\n"
                    verilog += f"         end\n"
                    verilog += f"      end\n"
                else:
                    # if hwce is in use, the _swrd already include the gating
                    verilog += f"      assign {if_portname}.{fld_name_in_if}_swacc{array_index_bracket} = {fld_name}_swrd; // access reported on any sw read\n"
            else:
                verilog += f"      assign {if_portname}.{fld_name_in_if}_swacc{array_index_bracket} = 0; // swacc for non readable field\n"

        if self.swwe and self.swwel:
            raise RuntimeError(f"Unexpected simultaneous options : swwe and swwel on {fld_name}. These are normally exclusive.")
        if self.swwe: # hardware enabling field modification
            verilog += f"      assign {fld_name}_swwe =\n"
            verilog += f"          {if_portname}.{fld_name_in_if}_swwe{array_index_bracket};\n"
        if self.swwel: # hardware enabling field modification
            verilog += f"      assign {fld_name}_swwe =\n"
            verilog += f"          ! {if_portname}.{fld_name_in_if}_swwel{array_index_bracket};\n"

        if (self.hwset):
            if self.cdc_in == Cdc.HOST_CE and self.cdc_in_signal == None:
                raise RuntimeError(f"HOST_CE cdc_in option on {fld_name} with hwset requires a valid cdc_in_signal to mask input")
            verilog += f"      assign  {fld_name}_hwset=\n"
            if self.multipart_if_slave or self.multipart_if_pattern:
                if self.cdc_in == Cdc.HOST_CE:
                    verilog += f"          {{{self.bit_width//reshape_size}{{{self.cdc_in_signal.name}}}}} &\n"
                verilog += f"          {if_portname}.{root_name}_hwset"+\
                           f"[{self.bit_width//reshape_size+multipart_bit_lsb//reshape_size-1}:{multipart_bit_lsb//reshape_size}];\n"
            elif self.bit_width <= 1:
                if self.cdc_in == Cdc.HOST_CE:
                    verilog += f"          {self.cdc_in_signal.name} &&\n"
                verilog += f"          {if_portname}.{fld_name_in_if}_hwset{array_index_bracket};\n"
            else:
                # non multipart vector
                if self.cdc_in == Cdc.HOST_CE:
                    verilog += f"          {{{self.bit_width//reshape_size}{{{self.cdc_in_signal.name}}}}} &\n"
                verilog += f"          {if_portname}.{fld_name_in_if}_hwset{array_index_bracket}[{self.bit_width//reshape_size-1}:0];\n"
        if (self.hwclr):
            if self.cdc_in == Cdc.HOST_CE and self.cdc_in_signal == None:
                raise RuntimeError(f"HOST_CE cdc_in option on {fld_name} with hwclr requires a valid cdc_in_signal to mask input")
            verilog += f"      assign  {fld_name}_hwclr=\n"
            if self.multipart_if_slave or self.multipart_if_pattern:
                if self.cdc_in == Cdc.HOST_CE:
                    verilog += f"          {{{self.bit_width//reshape_size}{{{self.cdc_in_signal.name}}}}} &\n"
                verilog += f"          {if_portname}.{root_name}_hwclr"+\
                           f"[{self.bit_width//reshape_size+multipart_bit_lsb//reshape_size-1}:{multipart_bit_lsb//reshape_size}];\n"
            elif self.bit_width <= 1:
                if self.cdc_in == Cdc.HOST_CE:
                    verilog += f"          {self.cdc_in_signal.name} &&\n"
                verilog += f"          {if_portname}.{fld_name_in_if}_hwclr{array_index_bracket};\n"
            else:
                # non multipart vector
                if self.cdc_in == Cdc.HOST_CE:
                    verilog += f"          {{{self.bit_width//reshape_size}{{{self.cdc_in_signal.name}}}}} &\n"
                verilog += f"          {if_portname}.{fld_name_in_if}_hwclr{array_index_bracket}[{self.bit_width//reshape_size-1}:0];\n"
        if self.hw.is_writable():
            verilog += f"      // fld_type={fld_type}\n"

            if self.cdc_in == Cdc.NA or (self.cdc_in == Cdc.HOST_CE and self.has_storage()):
                # Non cdc, _next is assign as normal signal directly from interface
                # In storage case, only the hwwe pulse are gated, data are comb assigned as well
                verilog += f"      assign {fld_name}_next =\n"
            elif  self.cdc_in == Cdc.HOST_CE:
                # HOST_CE and no storage, resync the data with single flop
                if self.cdc_in_signal == None:
                    raise RuntimeError(f"HOST_CE cdc_in option on {fld_name} requires a valid cdc_in_signal to resync input")
                cdc_name = fld_name + "_cdc"
                verilog += f"      {fld_type} {cdc_name}; // cdc resync signal declaration\n"
                verilog += f"      assign {fld_name}_next = {cdc_name};\n"
                verilog += f"      // Cdc input is resync with flop on clk. This uses same reset as field if any\n"
                if self.reset != None:
                    # normal flop based implementation with reset in sensitivity
                    verilog += f"      always_ff @(posedge clk{self.reset_edge})\n"
                else:
                    # normal flop based implementation with NO reset
                    verilog += f"      // This flop is requested to have NO reset feature\n"
                    verilog += f"      always_ff @(posedge clk)\n"
                # reset condition if applicable
                if self.reset != None:
                    verilog += f"         if({self.reset_name})\n"
                    verilog += f"             {cdc_name} <= 0; // reset to 0 the cdc in flop \n"
                    verilog += f"         else\n"
                verilog += f"         if({self.cdc_in_signal.name})\n"
                verilog += f"             {cdc_name} <= \n"
            elif  self.cdc_in == Cdc.FULL_ASYNC:
                if self.cdc_in_signal != None:
                    raise RuntimeError(f"FULL_ASYNC cdc_in option on {fld_name} provide unneeded cdc_in_signal input '{self.cdc_in_signal.name}'")
                if self.bit_width>1:
                    print(f"Warning: Detected CDC_IN as FULL_ASYNC on multibit for field: {self.name}, transient inconsistent value may propagate to host bus /!\\")
                    print(f"         Dual flop prevent metastable to propagate, but you can double check this is as intended !")
                cdc_name = fld_name + "_cdc"
                meta_name = fld_name + "_meta"
                verilog += f"      {fld_type} {meta_name}; // metastable first stage cdc resync signal declaration\n"
                verilog += f"      {fld_type} {meta_name}_next; // tmp intermediate signal for CDC\n"
                verilog += f"      {fld_type} {cdc_name}; // cdc resync signal declaration\n"
                verilog += f"      assign {fld_name}_next = {cdc_name};\n"
                verilog += f"      // Cdc input is resync with 2 stage of flop.\n"
                if self.reset != None:
                    # normal flop based implementation with reset in sensitivity
                    verilog += f"      always_ff @(posedge clk{self.reset_edge}) begin : proc_{fld_name}_cdcin_fasync\n"
                else:
                    # normal flop based implementation with NO reset
                    verilog += f"      // This flop is requested to have NO reset feature\n"
                    verilog += f"      always_ff @(posedge clk) begin : proc_{fld_name}_cdcin_fasync\n"
                # reset condition apply for both latch and flop case
                if self.reset != None:
                    verilog += f"         if({self.reset_name}) begin\n"
                    verilog += f"             {cdc_name} <= 0;\n"
                    verilog += f"             {meta_name} <= 0;\n"
                    verilog += f"         end else\n"

                verilog += f"         begin\n"
                verilog += f"            // second stage CDC flop\n"
                verilog += f"            {cdc_name} <= {meta_name};\n"
                verilog += f"            // first stage CDC flop\n"
                verilog += f"            {meta_name} <= {meta_name}_next;\n"
                verilog += f"         end\n"
                verilog += f"      end\n"
                verilog += f"      assign {meta_name}_next =\n"
            else:
                raise RuntimeError(f"Unexpected Cdc_in option option on {fld_name}:cdc_in={self.cdc_in}")

            # now complete the assign signal = ..., using signal from interface
            # assign target is either fld_name or cdc_name
            if self.encode:
                # internal signal is bit vector (interface is enum : cast needed)
                verilog += f"                 {self.bit_width}'({if_portname}.{fld_name_in_if}_next{array_index_bracket}); // cast enum to proper bitwidth integer\n"
            elif self.multipart_if_slave or self.multipart_if_pattern:
                verilog += f"                 {if_portname}.{root_name}_next"+\
                           f"[{self.bit_width//reshape_size+multipart_bit_lsb//reshape_size-1}:{multipart_bit_lsb//reshape_size}];\n"
            elif self.bit_width <= 1:
                verilog += f"                 {if_portname}.{fld_name_in_if}_next{array_index_bracket};\n"
            else:
                # non multipart vector
                verilog += f"                 {if_portname}.{fld_name_in_if}_next{array_index_bracket}[{self.bit_width//reshape_size-1}:0];\n"
            if self.has_storage():
                # hw writable and have storage
                if self.cdc_in == Cdc.NA:
                    # Non cdc, _next is assign as normal signal directly from interface
                    verilog += f"      assign {fld_name}_hwwe =\n"
                elif  self.cdc_in == Cdc.HOST_CE:
                    if self.cdc_in_signal == None:
                        raise RuntimeError(f"HOST_CE cdc_in option on {fld_name}_hwwe requires a valid cdc_in_signal to resync input")
                    verilog += f"      assign {fld_name}_hwwe = {self.cdc_in_signal.name} && // HOST_CE CDC_IN gating\n"
                # elif  self.cdc_in == Cdc.FULL_ASYNC: # write pulse is not supported in FULL Async mode
                else:
                    raise RuntimeError(f"Unexpected Cdc_in option option on {fld_name}_hwwe: cdc_in={self.cdc_in}")

                verilog += f"          {if_portname}.{fld_name_in_if}_hwwe{array_index_bracket};\n"
        verilog += "\n"
        return verilog

    def to_verilog_connect_interface(self, reg_name, parent_regmap, map_table_entry):
        if self.external:
            return self.to_verilog_connect_interface_external(reg_name, parent_regmap, map_table_entry)
        else:
            return self.to_verilog_connect_interface_internal(reg_name, parent_regmap, map_table_entry)

# ----------------------------------------------------------------------------

    def get_uvm_access(self):
        """
            get_uvm_access :Return the acces tpe for the field that match uvm reg model definition
                //set_access
                //'RO'      W: no effect, R: no effect
                //'RW'      W: as-is, R: no effect
                //'RC'      W: no effect, R: clears all bits
                //'RS'      W: no effect, R: sets all bits
                //'WRC'     W: as-is, R: clears all bits
                //'WRS'     W: as-is, R: sets all bits
                //'WC'      W: clears all bits, R: no effect
                //'WS'      W: sets all bits, R: no effect
                //'WSRC'    W: sets all bits, R: clears all bits
                //'WCRS'    W: clears all bits, R: sets all bits
                //'W1C'     W: 1/0 clears/no effect on matching bit, R: no effect
                //'W1S'     W: 1/0 sets/no effect on matching bit, R: no effect
                //'W1T'     W: 1/0 toggles/no effect on matching bit, R: no effect
                //'W0C'     W: 1/0 no effect on/clears matching bit, R: no effect
                //'W0S'     W: 1/0 no effect on/sets matching bit, R: no effect
                //'W0T'     W: 1/0 no effect on/toggles matching bit, R: no effect
                //'W1SRC'   W: 1/0 sets/no effect on matching bit, R: clears all bits
                //'W1CRS'   W: 1/0 clears/no effect on matching bit, R: sets all bits
                //'W0SRC'   W: 1/0 no effect on/sets matching bit, R: clears all bits
                //'W0CRS'   W: 1/0 no effect on/clears matching bit, R: sets all bits
                //'WO'      W: as-is, R: error
                //'WOC'     W: clears all bits, R: error
                //'WOS'     W: sets all bits, R: error
                //'W1'      W: first one after HARD reset is as-is, other W have no effects, R: no effect
                //'WO1'     W: first one after HARD reset is as-is, other W have no effects, R: error
        """
        if self.sw == Access.RW:
            if self.rclr:
                return "WRC"
            elif self.rset:
                return "WRS"
            elif self.woclr:
                return "W1C"
            elif self.woset:
                return "W1S"
            elif self.wot:
                return "W1T"
            elif self.wzc:
                return "W0C"
            elif self.wzs:
                return "W0S"
            elif self.wzt:
                return "W0T"
            elif self.wclr:
                return "WC" # readable, but not effect on read on field content
            elif self.wset:
                return "WS" # readable, but not effect on read on field content
            else:
                return "RW"
        elif  self.sw == Access.R:
            if self.rclr:
                return "RC"
            elif self.rset:
                return "RS"
            else:
                return "RO"
        elif  self.sw == Access.W:
            if self.rclr:
                return "WRC"
            elif self.rset:
                return "WRS"
            elif self.woclr:
                return "W1C"
            elif self.woset:
                return "W1S"
            elif self.wot:
                return "W1T"
            elif self.wzc:
                return "W0C"
            elif self.wzs:
                return "W0S"
            elif self.wzt:
                return "W0T"
            elif self.wclr:
                return "WOC" # write only clear
            elif self.wset:
                return "WOS" # write only set
            else:
                return "WO"
        elif  self.sw == Access.NA:
            print("Warning: generate uvm access model, with no software access\n")
            print("         Woud be error or undefined in systemrdl spec,\n")
            print("         mark uvm reg model as NA, please check register definition\n")
            return "NA" # no uvm way to say read as 0 ???

# ----------------------------------------------------------------------------

class Pad_Field(Field):
    """docstring for Pad_Field"""

    def __init__(self, name, bit_range):
        super().__init__(name=name, bit_range=bit_range)
        # Pad field is degenerated : readonly from sw (read constant reset value)
        self.hw = Access.NA
        self.sw = Access.R

        self.reset = 0

    def is_volatile(self):
        return False

    def to_verilog(self, reg_name, array_name_suffix):
        fld_name = f"{reg_name}_field_{self.name}"
        # verilog_signal_name is tie to 0
        verilog = ""
        verilog += f"logic [{self.bit_width}:0] {fld_name};\n"
        verilog += f"assign {fld_name} = 0;\n"
        verilog += "\n"
        return verilog


# ----------------------------------------------------------------------------

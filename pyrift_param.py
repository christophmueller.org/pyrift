# ----------------------------------------------------------------------------
# Copyright 2019 Regis Cattenoz, regis@ip666.org
#
# This file is part of pyrift : https://gitlab.com/regisc/pyrift
#
# pyrift is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pyrift is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pyrift.  If not, see <https://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------------
# File: pyrift_param.py
# Python Register InterFace Translation
# ----------------------------------------------------------------------------

from pyrift_field import PyriftBase
# ----------------------------------------------------------------------------


class Param(PyriftBase):
    """
        Param serve as declaration of extra define of constant parameter to registermap
        that should propagate to verilog package
    """

    def __init__(self, name,
        # bring all field to constructor parameter with default value
        # should help user for concise Param creation
        desc=None,
        value=0,
        ):
        super().__init__()
        self.name = name

        # desc is provided as constructor "desc" param
        # else carry default setting from base class
        if desc:
            self.desc = desc
        else:
            self.desc = "Parameter added to verilog package"
        self.value = value


# ----------------------------------------------------------------------------

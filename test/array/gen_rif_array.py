# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# ----------------------------------------------------------------------------
# R1 register
reg_r1 = Register(shortname="r1", desc="R1")
reg_r1.add(Field(shortname="f11", bit_range='3:0', desc="f11", sw=Access.RW, hw=Access.R))

# ----------------------------------------------------------------------------
# R1d register
reg_1d = Register(shortname="r1d", desc="R1d", array=6)
reg_1d.add(Field(shortname="f1d", bit_range='7:4', desc="f1d",
    reset = (1,2,3,4,5,6),
    # reset = 12,
    sw=Access.RW, hw=Access.RW))

# R2d register
reg_2d = Register(shortname="r2d", desc="R2d", array=(3,2))
reg_2d.add(Field(shortname="f2d", bit_range='7:4', desc="f2d",
    reset = ( # reset of 2d field can be set to 1d reset vector table
        15, # [0,0]
        14, # [0,1]
        13, # [1,0]
        12, # [1,1]
        11, # [2,0]
        10  # [2,1]
    ),
    sw=Access.RW, hw=Access.RW))

# ----------------------------------------------------------------------------
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant
test_regmap.add(reg_r1, pad=4)
test_regmap.add(reg_1d, align=32)
test_regmap.add(reg_2d, align=32)

test_regmap.validate()

test_regmap.write_html("output/rif_test.html")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
# test_regmap.write_verilog_package("output/test_pkg.sv") # pkg for enum definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")

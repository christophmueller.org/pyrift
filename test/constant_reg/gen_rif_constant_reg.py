# -----------------------------------------------------
# gen_rif_constant_reg.py
# this is a basic testing setup to check constant register
# These are register with read acces only (either Sw or hw)
# and no write option
# These register are initialized by reset value and stay
# constant
# -----------------------------------------------------

# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# ----------------------------------------------------------------------------
# Create an example register shortname "status"
reg_constant = Register(shortname="CONST", desc="constant register of my IP")
# field is read only from sw
# This makes a non storage, constant
reg_constant.add(Field(shortname="f_sw_const", bit_range='7:0', desc="sw const",\
                       reset=0x12,
                       sw=Access.R, hw=Access.NA))
# field is read only from both sw and hw
# This makes a non storage, constant
reg_constant.add(Field(shortname="f_sw_hw_const", bit_range='23:16', desc="hw const",\
                       reset=0x56,
                       sw=Access.R, hw=Access.R))

# ----------------------------------------------------------------------------
# create a register map to collect the available set of register
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
# set the address range to be decoded on APB
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant

test_regmap.add(reg_constant)
# validate consistency of register map content, compute the map table
# (address lookup for register in address space decoded on the host bus)
test_regmap.validate()

# generate output file
# if directory is missing, it will be created
# Existing file is to be overwriten without warning
test_regmap.write_html("output/rif_test.html")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
# test_regmap.write_verilog_package("output/test_pkg.sv") # pkg for enum definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")

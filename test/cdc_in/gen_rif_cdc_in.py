# -----------------------------------------------------
# gen_rif_cdc.py
# this is a basic testing for cdc options
# decoding in generated verilog
# -----------------------------------------------------

# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# Choice of CDC mode : Please uncomment exactly one line of the next 2 lines
CURRENT_CDC_MODE = Cdc.HOST_CE
# CURRENT_CDC_MODE = Cdc.FULL_ASYNC

if CURRENT_CDC_MODE == Cdc.HOST_CE:
    current_cdc_in_signal_name = "hwce"
    cdc_input_signal = Signal("hwce", activelow=False, sync=True)
elif CURRENT_CDC_MODE == Cdc.FULL_ASYNC:
    # use rif clock for resync as hw is fully asynchronous with unknown clock
    # no extra signal/ce used in this case
    current_cdc_in_signal_name = None
    cdc_input_signal = None

# ----------------------------------------------------------------------------
# Create an example register shortname "status"
reg_status = Register(shortname="status")
# register description can be added after constructor call
reg_status.desc="Status"

# Create an example field to put in register r1.register
# shortname f1
# First call the constructor of field, then set all wanted feature
# Only mandatory parameter of constructor is shortname (or name) and bitfield
fld_s1 = Field(shortname="fld_s1", bit_range='1:0')
fld_s1.desc="fld_s1 field description"

# read access from software, write from hw (default for direct status connection)
fld_s1.sw=Access.R
fld_s1.hw=Access.W
fld_s1.cdc_in = CURRENT_CDC_MODE
fld_s1.cdc_in_signal_name = current_cdc_in_signal_name

reg_status.add(fld_s1)

if CURRENT_CDC_MODE == Cdc.HOST_CE:
    fld_storage2 = Field(shortname="fld_storage2", bit_range='8:7')
    fld_storage2.desc="fld_storage2 field description"

    # read/write access from software, read/write from hw
    fld_storage2.sw=Access.RW
    fld_storage2.hw=Access.RW
    fld_storage2.cdc_in = Cdc.HOST_CE # NO FULL_ASYNC supported yet for hwwe pulse
    fld_storage2.cdc_in_signal_name = current_cdc_in_signal_name

    # fld_storage2.cdc_out = Cdc.HOST_CE
    # fld_storage2.cdc_out_signal_name = current_cdc_in_signal_name

    reg_status.add(fld_storage2)

if CURRENT_CDC_MODE == Cdc.HOST_CE:
    fld_hwset3 = Field(shortname="fld_hwset3", bit_range='10:9')
    fld_hwset3.desc="fld_hwset3 field description"

    # read/write access from software, read/write from hw
    fld_hwset3.sw=Access.R
    fld_hwset3.hw=Access.NA
    fld_hwset3.hwset = True
    fld_hwset3.cdc_in = Cdc.HOST_CE # NO FULL_ASYNC supported yet for hwwe pulse
    fld_hwset3.cdc_in_signal_name = current_cdc_in_signal_name

    reg_status.add(fld_hwset3)

if CURRENT_CDC_MODE == Cdc.HOST_CE:
    fld_hwclr4 = Field(shortname="fld_hwclr4", bit_range='12')
    fld_hwclr4.desc="fld_hwclr4 field description"

    # read/write access from software, read/write from hw
    fld_hwclr4.sw=Access.R
    fld_hwclr4.hw=Access.NA
    fld_hwclr4.hwset = True
    fld_hwclr4.cdc_in = Cdc.HOST_CE # NO FULL_ASYNC supported yet for hwwe pulse
    fld_hwclr4.cdc_in_signal_name = current_cdc_in_signal_name

    reg_status.add(fld_hwclr4)


# ----------------------------------------------------------------------------
# Create a table as array of register shortname "table"
reg_table = Register(shortname="table", array=3)
# register description can be added after constructor call
reg_table.desc="Table_3"

# Create an example field to put in register r1.register
# shortname f1
# First call the constructor of field, then set all wanted feature
# Only mandatory parameter of constructor is shortname (or name) and bitfield
f1 = Field(shortname="f1", bit_range='1:0')
f1.desc="f1 field description"

# read access from software, write from hw (default for direct status connection)
f1.sw=Access.R
f1.hw=Access.W
f1.cdc_in = CURRENT_CDC_MODE
f1.cdc_in_signal_name = current_cdc_in_signal_name

reg_table.add(f1)

# ----------------------------------------------------------------------------
# Check CDC on multipart register
# test_id_part0 register

fld_test_id_part0 = Field(shortname="test_id_part0", bit_range='31:0')
fld_test_id_part0.sw = Access.R
fld_test_id_part0.hw = Access.W
fld_test_id_part0.multipart_if_pattern = r"(.*)_part\d*"
fld_test_id_part0.if_reshape = (2,)
fld_test_id_part0.desc = """\
    TEST ID register 1
"""
fld_test_id_part0.cdc_in = CURRENT_CDC_MODE
fld_test_id_part0.cdc_in_signal_name = current_cdc_in_signal_name
reg_test_id_part0 = Register(fld_test_id_part0)

# ----------------------------------------------------------------------------
# test_id_part1 register

fld_test_id_part1 = Field(shortname="test_id_part1", bit_range='31:0')
fld_test_id_part1.sw = Access.R
fld_test_id_part1.hw = Access.W
fld_test_id_part1.multipart_if_slave = True
fld_test_id_part1.desc = """\
    TEST ID register 2
"""
fld_test_id_part1.cdc_in = CURRENT_CDC_MODE
fld_test_id_part1.cdc_in_signal_name = current_cdc_in_signal_name
reg_test_id_part1 = Register(fld_test_id_part1)


# ----------------------------------------------------------------------------
# reshaped register
reg_reshaped = Register(
    Field(  shortname="reshaped", bit_range='31:0',
            sw = Access.R, hw = Access.W, if_reshape = (2,),
            cdc_in = CURRENT_CDC_MODE, cdc_in_signal_name = current_cdc_in_signal_name))

# ----------------------------------------------------------------------------
# create a register map to collect the available set of register
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
# set the address range to be decoded on APB
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant

test_regmap.add(reg_status)
test_regmap.add(reg_table)
test_regmap.add(reg_test_id_part0)
test_regmap.add(reg_test_id_part1)
test_regmap.add(reg_reshaped)
if cdc_input_signal:
    test_regmap.add_signal(cdc_input_signal)


# validate consistency of register map content, compute the map table
# (address lookup for register in address space decoded on the host bus)
test_regmap.validate()

# generate output file
# if directory is missing, it will be created
# Existing file is to be overwriten without warning
test_regmap.write_html("output/rif_test.html")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
# test_regmap.write_verilog_package("output/test_pkg.sv") # pkg for enum definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")

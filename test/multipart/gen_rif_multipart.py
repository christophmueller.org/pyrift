# -----------------------------------------------------
# gen_rif_multpart.py
# this is a basic testing for multipart options
# have  32 x 2 bit fit in 2 registers (32bit contain only 16 x 2 bit fields)
# -----------------------------------------------------

# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# ----------------------------------------------------------------------------
# test_id_part0 register

fld_test_id_part0 = Field(shortname="test_id_part0", bit_range='31:0')
fld_test_id_part0.sw = Access.RW
fld_test_id_part0.hw = Access.R
fld_test_id_part0.multipart_if_pattern = r"(.*)_part\d*"
fld_test_id_part0.if_reshape = (2,)
fld_test_id_part0.desc = """\
    TEST ID register 1
"""
reg_test_id_part0 = Register(fld_test_id_part0)

# ----------------------------------------------------------------------------
# test_id_part1 register

fld_test_id_part1 = Field(shortname="test_id_part1", bit_range='31:0')
fld_test_id_part1.sw = Access.RW
fld_test_id_part1.hw = Access.R
fld_test_id_part1.multipart_if_slave = True
fld_test_id_part1.desc = """\
    TEST ID register 2
"""
reg_test_id_part1 = Register(fld_test_id_part1)

# ----------------------------------------------------------------------------
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant
test_regmap.add(reg_test_id_part0)
test_regmap.add(reg_test_id_part1)

test_regmap.validate()

test_regmap.write_html("output/rif_test.html")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
# test_regmap.write_verilog_package("output/test_pkg.sv") # pkg for enum definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")

# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# ----------------------------------------------------------------------------
# R1 register
reg_r1 = Register(
    Field(shortname="f11", bit_range='3:0', desc="f11", sw=Access.RW, hw=Access.R, latch=True),
    desc="R1")
reg_r1.add(Field(shortname="f12", bit_range='7:4', desc="f12", sw=Access.RW, hw=Access.RW, latch=True))
reg_r1.add(Field(shortname="f13", bit_range='15:8',  desc="f13", sw=Access.RW, hw=Access.R, latch=True, reset=None))
reg_r1.add(Field(shortname="f14", bit_range='19:16', desc="f14", sw=Access.RW, hw=Access.R, latch=True, reset=5))

# ----------------------------------------------------------------------------
# R2 register
reg_r2 = Register(
    Field(shortname="f21", bit_range='7:4', desc="f21", sw=Access.RW, hw=Access.R, latch=True),
    desc="R2")

# ----------------------------------------------------------------------------
# R3 register : LDCE/LDPE primitive used
reg_r3 = Register(shortname="R3", desc="R3")
f31=Field(shortname="f31", bit_range='1:0', desc="f31", sw=Access.RW, hw=Access.R, latch=True,
          reset=2)
f32=Field(shortname="f32", bit_range='2', desc="f32", sw=Access.RW, hw=Access.R, latch=True,
          reset=1)
f33=Field(shortname="f33", bit_range='9:8', desc="f33", sw=Access.RW, hw=Access.R, latch=True,
          reset=None)
f31.latch_use_lib_ldce = True
f32.latch_use_lib_ldce = True
f33.latch_use_lib_ldce = True
reg_r3.add(f31)
reg_r3.add(f32)
reg_r3.add(f33)

# ----------------------------------------------------------------------------
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant
test_regmap.add(reg_r1)
test_regmap.add(reg_r2, pad=12)
test_regmap.add(reg_r3)

test_regmap.validate()

test_regmap.write_html("output/rif_test.html")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
# test_regmap.write_verilog_package("output/test_pkg.sv") # pkg for enum definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")
# test_regmap.write_word("output/rif_test.docx", template="")

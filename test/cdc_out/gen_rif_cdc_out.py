# -----------------------------------------------------
# gen_rif_cdc.py
# this is a basic testing for cdc options
# decoding in generated verilog
# -----------------------------------------------------

# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# Choice of CDC mode : Please uncomment exactly one line of the next 2 lines
#CURRENT_CDC_MODE = Cdc.NA
CURRENT_CDC_MODE = Cdc.HOST_CE
#CURRENT_CDC_MODE = Cdc.FULL_ASYNC

CURRENT_CDC_SWXXX_MODE = Cdc.HOST_CE

#SWMOD = False
SWMOD = True
SWACC = False
#SWACC = True

if CURRENT_CDC_MODE == Cdc.HOST_CE:
    current_cdc_out_signal_name = "hwce"
    cdc_output_signal = Signal("hwce", activelow=False, sync=True)
else:
    current_cdc_out_signal_name = "hw_clk"
    cdc_output_signal = Signal("hw_clk", activelow=False, sync=True)

# ----------------------------------------------------------------------------
# Create an example register shortname "config"
reg_config = Register(shortname="config")
# register description can be added after constructor call
reg_config.desc="CFG"

# Create an example field to put in register r1.register
# shortname f1
# First call the constructor of field, then set all wanted feature
# Only mandatory parameter of constructor is shortname (or name) and bitfield
cfg1 = Field(shortname="cfg1", bit_range='1:0')
cfg1.desc="cfg1 field description"

cfg1.sw=Access.RW # read/write access from software (default if not specified)
cfg1.hw=Access.R # hardware get read access (typical of configuration field)
cfg1.cdc_out = CURRENT_CDC_MODE
cfg1.cdc_out_signal_name = current_cdc_out_signal_name

cfg2 = Field(shortname="cfg2", bit_range='3:2')
cfg2.desc="cfg2 field description"

cfg2.sw=Access.RW # read/write access from software (default if not specified)
cfg2.hw=Access.R # hardware get read access (typical of configuration field)
cfg2.cdc_out = CURRENT_CDC_MODE
if SWMOD: cfg2.cdc_swmod = CURRENT_CDC_SWXXX_MODE
if SWACC: cfg2.cdc_swacc = CURRENT_CDC_SWXXX_MODE
cfg2.cdc_out_signal_name = current_cdc_out_signal_name
cfg2.swmod = SWMOD
cfg2.swacc = SWACC

if CURRENT_CDC_MODE == Cdc.HOST_CE:
    # single pulse works only with HOST_CE
    singlepulse = Field(shortname="singlepulse", bit_range='4', singlepulse=True)
    singlepulse.desc="singlepulse field description"
    singlepulse.cdc_out = CURRENT_CDC_MODE
    singlepulse.cdc_out_signal_name = current_cdc_out_signal_name

reg_config.add(cfg1)
reg_config.add(cfg2)
reg_config.add(singlepulse)

# ----------------------------------------------------------------------------
# Create an table as array of register shortname "config"
reg_table = Register(shortname="table", array=3)
# register description can be added after constructor call
reg_table.desc="Table_3"

# Create an example field to put in register r1.register
# shortname f1
# First call the constructor of field, then set all wanted feature
# Only mandatory parameter of constructor is shortname (or name) and bitfield
f1 = Field(shortname="f1", bit_range='1:0')
f1.desc="f1 field description"

f1.sw=Access.RW # read/write access from software (default if not specified)
f1.hw=Access.R # hardware get read access (typical of configuration field)
f1.cdc_out = CURRENT_CDC_MODE
f1.cdc_out_signal_name = current_cdc_out_signal_name

f2 = Field(shortname="f2", bit_range='3:2')
f2.desc="f2 field description"

f2.sw=Access.RW # read/write access from software (default if not specified)
f2.hw=Access.R # hardware get read access (typical of configuration field)
f2.cdc_out = CURRENT_CDC_MODE
f2.cdc_out_signal_name = current_cdc_out_signal_name
f2.swmod = SWMOD
f2.swacc = SWACC

reg_table.add(f1)
reg_table.add(f2)

# ----------------------------------------------------------------------------
# Check CDC on multipart register
# test_id_part0 register

fld_test_id_part0 = Field(shortname="test_id_part0", bit_range='31:0')
fld_test_id_part0.sw = Access.RW
fld_test_id_part0.hw = Access.R
fld_test_id_part0.multipart_if_pattern = r"(.*)_part\d*"
fld_test_id_part0.if_reshape = (2,)
fld_test_id_part0.desc = """\
    TEST ID register 1
"""
fld_test_id_part0.cdc_out = CURRENT_CDC_MODE
fld_test_id_part0.cdc_out_signal_name = current_cdc_out_signal_name
reg_test_id_part0 = Register(fld_test_id_part0)

# ----------------------------------------------------------------------------
# test_id_part1 register

fld_test_id_part1 = Field(shortname="test_id_part1", bit_range='31:0')
fld_test_id_part1.sw = Access.RW
fld_test_id_part1.hw = Access.R
fld_test_id_part1.multipart_if_slave = True
fld_test_id_part1.desc = """\
    TEST ID register 2
"""
fld_test_id_part1.cdc_out = CURRENT_CDC_MODE
fld_test_id_part1.cdc_out_signal_name = current_cdc_out_signal_name
reg_test_id_part1 = Register(fld_test_id_part1)


# ----------------------------------------------------------------------------
# reshaped register
reg_reshaped = Register(
    Field(  shortname="reshaped", bit_range='31:0', if_reshape = (2,),
            cdc_out = CURRENT_CDC_MODE, cdc_out_signal_name = current_cdc_out_signal_name))

# ----------------------------------------------------------------------------
# create a register map to collect the available set of register
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
# set the address range to be decoded on APB
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant

test_regmap.add(reg_config)
test_regmap.add(reg_table)
test_regmap.add(reg_test_id_part0)
test_regmap.add(reg_test_id_part1)
test_regmap.add(reg_reshaped)

test_regmap.add_signal(cdc_output_signal)


# validate consistency of register map content, compute the map table
# (address lookup for register in address space decoded on the host bus)
test_regmap.validate()

# generate output file
# if directory is missing, it will be created
# Existing file is to be overwriten without warning
test_regmap.write_html("output/rif_test.html")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
# test_regmap.write_verilog_package("output/test_pkg.sv") # pkg for enum definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")

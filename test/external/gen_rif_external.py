# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# Field.default_support_hwbe = True

# ----------------------------------------------------------------------------
# R1 register
reg_r1 = Register(shortname='R1', desc="R1") # construct reg with different external field

reg_r1.add(Field(
    shortname="f11r", bit_range='0',
    sw=Access.R, hw=Access.NA,
    external=True,
    desc="R1[0] readonly"))

reg_r1.add(Field(
    shortname="f12r", bit_range='3:2',
    sw=Access.R, hw=Access.NA,
    # cdc_in = Cdc.HOST_CE, # CDC not yet supported on external field
    # cdc_out = Cdc.HOST_CE, # CDC not yet supported on external field
    external=True,
    desc="R1[3:2] multibit readonly"))

reg_r1.add(Field(
    shortname="f13w", bit_range='4',
    sw=Access.W, hw=Access.NA,
    external=True,
    desc="R1[4] write only"))

reg_r1.add(Field(
    shortname="f14w", bit_range='7:6',
    sw=Access.W, hw=Access.NA,
    external=True,
    desc="R1[7:6] multibit writeonly"))

reg_r1.add(Field(
    shortname="f15rw", bit_range='8',
    sw=Access.RW, hw=Access.NA,
    external=True,
    desc="R1[8] read/write"))

reg_r1.add(Field(
    shortname="f16rw", bit_range='11:10',
    sw=Access.RW, hw=Access.NA,
    external=True,
    desc="R1[11:10] multibit writeonly"))

# ----------------------------------------------------------------------------
# R2 register : have extready option
reg_r2 = Register(shortname='R2', desc="R2") # construct reg with different external field

reg_r2.add(Field(
    shortname="f21r", bit_range='0',
    sw=Access.R, hw=Access.NA,
    external=True,
    extready_rd=True,
    desc="R2[0] readonly + rdy"))

reg_r2.add(Field(
    shortname="f22r", bit_range='3:2',
    sw=Access.R, hw=Access.NA,
    external=True,
    extready_rd=True,
    desc="R2[3:2] multibit readonly + rdy"))

reg_r2.add(Field(
    shortname="f23w", bit_range='4',
    sw=Access.W, hw=Access.NA,
    external=True,
    extready_wr=True,
    desc="R2[4] write only + rdy"))

reg_r2.add(Field(
    shortname="f24w", bit_range='7:6',
    sw=Access.W, hw=Access.NA,
    external=True,
    extready_wr=True,
    desc="R2[7:6] multibit writeonly + rdy"))

reg_r2.add(Field(
    shortname="f25rw", bit_range='8',
    sw=Access.RW, hw=Access.NA,
    external=True,
    extready_rd=True,
    extready_wr=True,
    desc="R2[8] read/write + rdy"))

reg_r2.add(Field(
    shortname="f26rw", bit_range='11:10',
    sw=Access.RW, hw=Access.NA,
    external=True,
    extready_rd=True,
    extready_wr=True,
    desc="R2[11:10] multibit writeonly + rdy"))

reg_r2.add(Field(
    shortname="f27rw", bit_range='12',
    sw=Access.RW, hw=Access.NA,
    external=True,
    extready_rd=True,
    desc="R2[12] read/write + rdy_rd"))

reg_r2.add(Field(
    shortname="f28rw", bit_range='13',
    sw=Access.RW, hw=Access.NA,
    external=True,
    extready_wr=True,
    desc="R2[1] read/write + rdy_wr"))

# ----------------------------------------------------------------------------
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant
test_regmap.add(reg_r1)
test_regmap.add(reg_r2)

test_regmap.validate()

test_regmap.write_html("output/rif_test.html")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
# test_regmap.write_verilog_package("output/test_pkg.sv") # pkg for enum definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")

# -----------------------------------------------------
# gen_rif_word.py
# this is an example script to illustrate the generation of word documentation
# This is using a basic quick start example with word output added
# -----------------------------------------------------

# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *


class my_enum(FieldEncode):
    E_IDLE = 0
    E_READ = 1
    E_WRITE = 2
    E_ACK = 3


# ----------------------------------------------------------------------------
# Create an example register shortname "config"
reg_config = Register(shortname="config")
# register description can be added after constructor call
reg_config.desc="CFG"

# Create an example field to put in register r1.register
# shortname f1
# First call the constructor of field, then set all wanted feature
# Only mandatory parameter of constructor is shortname (or name) and bitfield
cfg1 = Field(shortname="cfg1", bit_range='1:0')
# add field description, can be multiline
cfg1.desc="cfg1 field description" # single line description

cfg1.sw=Access.RW # read/write access from software (default if not specified)
cfg1.hw=Access.R # hardware get read access (typical of configuration field)
cfg1.reset = my_enum.E_WRITE # Enum type can be used as reset value
cfg1.encode = my_enum

cfg2 = Field(shortname="cfg2", bit_range='8', sw=Access.RW, hw=Access.R)
cfg2.desc="cfg2 field description" # single line description

reg_config.add(cfg1)
reg_config.add(cfg2)

# ----------------------------------------------------------------------------
# Create an example register shortname "status"
reg_status = Register(shortname="STAT", desc="status register of my IP")
# field is status register is readable by software, writable by hardware
# This makes a non storage, software accessible signal in hardware
reg_status.add(Field(shortname="f_stat", bit_range='15:0', desc="stat field in STAT register",\
                     sw=Access.R, hw=Access.W))

# ----------------------------------------------------------------------------
# create a register map to collect the available set of register
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
# set the address range to be decoded on APB
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant

test_regmap.add(reg_config)
test_regmap.add(reg_status, align=32) # re-align status register to address multiple of 32
                                      # will allow adding more config register
                                      # without shifting the status register in address map

# validate consistency of register map content, compute the map table
# (address lookup for register in address space decoded on the host bus)
test_regmap.validate()

# generate output file
# if directory is missing, it will be created
# Existing file is to be overwriten without warning
test_regmap.write_html("output/rif_test.html")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
test_regmap.write_verilog_package("output/test_pkg.sv") # pkg for enum definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")

# Generate the word document with register map information
# template can be an empty word document (.docx) provided by user
# requirement is to have a table stype defined with name = "pyrift_register"
# if template="", then pyrift try to provide its own basic template
test_regmap.write_word("output/rif_test.docx", template="")

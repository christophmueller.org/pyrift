# -----------------------------------------------------
# gen_rif_single_reg.py
# this is a basic testing setup to check side effect of 0 bit address
# decoding in generated verilog
# -----------------------------------------------------

# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# ----------------------------------------------------------------------------
# Create an example register shortname "status"
reg_status = Register(shortname="STAT", desc="status register of my IP")
# field is status register is readable by software, writable by hardware
# This makes a non storage, software accessible signal in hardware
reg_status.add(Field(shortname="f_stat", bit_range='15:0', desc="stat field in STAT register",\
                     sw=Access.R, hw=Access.W))
                     # sw=Access.RW, hw=Access.NA)) # check for empty interface (no hw signal at all)
reg_status.add(Field(shortname="f_disabled", bit_range='23:16',
                     desc="Field not present in STAT register",\
                     sw=Access.R, hw=Access.W, ispresent=True))

reg_disabled = Register(shortname="reg_disabled", desc="reg_disabled is NOT present",
                        ispresent=True)
reg_disabled.add(Field(shortname="f_cfg", bit_range='15:0',
                       desc="f_cfg is a true field enabled, but inside a disabled register",\
                       sw=Access.RW, hw=Access.R))

# ----------------------------------------------------------------------------
# create a register map to collect the available set of register
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
# set the address range to be decoded on APB
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant

test_regmap.add(reg_status)
test_regmap.add(reg_disabled)
# validate consistency of register map content, compute the map table
# (address lookup for register in address space decoded on the host bus)
test_regmap.validate()

# first validate, then change configuration and disable register and field
# Directly setting the ispresent member to False
reg_status.get_field_by_name("f_disabled").ispresent = False
reg_disabled.ispresent = False
test_regmap.validate()

# generate output file
# if directory is missing, it will be created
# Existing file is to be overwriten without warning
test_regmap.write_html("output/rif_test.html")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
# test_regmap.write_verilog_package("output/test_pkg.sv") # pkg for enum definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")

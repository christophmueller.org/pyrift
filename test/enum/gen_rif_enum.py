# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

class Fsm_State(FieldEncode):
    FSM_RESET = 0
    FSM_INIT = 1
    FSM_IDLE = 2
    FSM_RUN = 3
    FSM_FINISHING = 4
    FSM_STOP = 5

    def get_desc():
        return """ \
            Fsm_State is symbolic encoding
            This will apply either for verilog package or cheader generation
        """
class Cycle_Request(FieldEncode):
    BUS_IDLE = 0
    BUS_READ = 1
    BUS_WRITE = 2
    BUS_ACK = 3

    def get_desc():
        return """ \
            Symbolic encoding for the configuration of a request to HW
            This will apply either for verilog package or cheader generation
            0: BUS_IDLE
            1: BUS_READ
            2: BUS_WRITE
            3: BUS_ACK
        """
class Cycle_Request2(FieldEncode):
    BUS2_IDLE = 0
    BUS2_READ = 1
    BUS2_WRITE = 2
    BUS2_ACK = 3

    def get_desc():
        return """ \
            Symbolic encoding for the configuration of a request to HW
            This will apply either for verilog package or cheader generation
        """
class Unused_Encoding(FieldEncode):
    UNUSED_0 = 0
    UNUSED_1 = 1
    UNUSED_2 = 2
    UNUSED_3 = 3

    def get_desc():
        return """ \
            Enum encoding can be defined but not used
        """

# ----------------------------------------------------------------------------
# R1 register contain a field with enum type
reg_r1 = Register(shortname="r1", desc="R1")
reg_r1.add(Field(shortname="f1",
                 bit_range='2:0', # Bit range width is to match the # bit of enum type
                                  # here 6 value encoded require 3 bit for field
                 desc="f1 : access hardware FSM status as enum encoding",
                 sw=Access.R,
                 hw=Access.W,
                 encode = Fsm_State))
reg_r1.add(Field(shortname="f2",
                 bit_range='9:8', # Bit range width is to match the # bit of enum type
                                  # here 4 value encoded require 2 bit for field
                 desc="f2 encoded bus state",
                 sw=Access.RW,
                 hw=Access.R,
                 reset = Cycle_Request.BUS_ACK, # reset can be an symbolic enum type
                 encode = Cycle_Request))
reg_r1.add(Field(shortname="f3",
                 bit_range='11:10', # Bit range width is to match the # bit of enum type
                                  # here 4 value encoded require 2 bit for field
                 desc="f3 encoded bus state",
                 sw=Access.RW,
                 hw=Access.R,
                 encode = Cycle_Request2))

# ----------------------------------------------------------------------------
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant
test_regmap.add(reg_r1)

test_regmap.add_param(Param("TEST_PARAM", value=0x1234, desc="Extra parameter for system verilog package"))
test_regmap.add_param(Param("TEST_PARAM2", value=0x4567))
test_regmap.add_param(Param("USER_PARAM", value=0x12, desc="Comment ..."))

test_regmap.validate()

test_regmap.write_html("output/rif_test.html")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
test_regmap.write_verilog_package("output/test_pkg.sv") # pkg for enum definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")

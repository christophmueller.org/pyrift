# -----------------------------------------------------
# gen_rif_egmap.py
# this is an example script to illustrate howto nest regmap in pyrift
# this instanciate register in 2 registermap and
# then create a top regmap to instanciate each low level regmap
# -----------------------------------------------------

# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# ----------------------------------------------------------------------------
# Create one config and stat register for regmap 1
reg_cfg1 = Register(shortname="cfg1", desc="cfg1", array=(2,3))
reg_cfg1.add(Field(shortname="cfg1_f1", bit_range='7:0',  reset = 0x23, desc="cfg1_f1", sw=Access.RW, hw=Access.R))
reg_cfg1.add(Field(shortname="cfg1_f2", bit_range='15:8', reset = 0x45, desc="cfg1_f2", sw=Access.RW, hw=Access.R))

reg_stat1 = Register(shortname="stat1", desc="stat1")
reg_stat1.add(Field(shortname="stat1_f1", bit_range='7:0',  reset = 0x23, desc="stat1_f1", sw=Access.R, hw=Access.W))
reg_stat1.add(Field(shortname="stat1_f2", bit_range='15:8', reset = 0x45, desc="stat1_f2", sw=Access.R, hw=Access.W))

# ----------------------------------------------------------------------------

# regmap1 is containing {cfg1, stat1}
regmap1 = RegisterMap("regmap1",array=(2,3))
regmap1.interface_name = "IF_RM1"
regmap1.align = 0x100
regmap1.post_pad = 12
regmap1.pre_pad = 4

regmap1.add(reg_cfg1)
regmap1.add(reg_stat1)

# ----------------------------------------------------------------------------
# Create one config and stat register for regmap 2
reg_cfg2 = Register(shortname="cfg2", desc="cfg2")
reg_cfg2.add(Field(shortname="cfg2_f1", bit_range='7:0',  reset = 0x23, desc="cfg2_f1", sw=Access.RW, hw=Access.R))
reg_cfg2.add(Field(shortname="cfg2_f2", bit_range='15:8', reset = 0x45, desc="cfg2_f2", sw=Access.RW, hw=Access.R))

reg_stat2 = Register(shortname="stat2", desc="stat2")
reg_stat2.add(Field(shortname="stat2_f1", bit_range='7:0',  reset = 0x23, desc="stat2_f1", sw=Access.R, hw=Access.W))
reg_stat2.add(Field(shortname="stat2_f2", bit_range='15:8', reset = 0x45, desc="stat2_f2", sw=Access.R, hw=Access.W))

# ----------------------------------------------------------------------------
# Create one register for regmap 2_1
reg_const2_1 = Register(shortname="const2_1", desc="const2_1", array=2)
reg_const2_1.add(Field(shortname="const2_1_f1", bit_range='7:0',
                       reset = (0xC0, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7), # size == regmap x reg
                       # reset = (0xC0, 0xC1, 0xC2, 0xC3), # illegal size
                       # reset = (0xC0, 0xC1), # size == reg, alias on all regmap
                       # reset = (0xC0), # size == 1 same for all reg
                       desc="const2_1_f1", sw=Access.R, hw=Access.NA))
# regmap2_1 is containing {const2_1}
regmap2_1 = RegisterMap("regmap2_1", array=2)
regmap2_1.interface_name = "IF_RM2"

regmap2_1.align = 0x10
regmap2_1.add(reg_const2_1)

# ----------------------------------------------------------------------------

# regmap2 is containing {cfg1, stat1}
regmap2 = RegisterMap("regmap2", array=2)
regmap2.interface_name = "IF_RM2"

regmap2.align = 0x100
regmap2.add(reg_cfg2)
regmap2.add(reg_stat2)
regmap2.add(regmap2_1)

# ----------------------------------------------------------------------------
# create a register map to collect the available set of register
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
# set the address range to be decoded on APB
test_regmap.rif_host_bus_msb = 11 # 2048B allocated to TEST, 11 bit address decode are significant
test_regmap.base_address = 0x10000

test_regmap.add(regmap1)
test_regmap.add(regmap2, align=0x100) # re-align regmap to address multiple of 0x100

# validate consistency of register map content, compute the map table
# (address lookup for register in address space decoded on the host bus)
test_regmap.validate()

# generate output file
# if directory is missing, it will be created
# Existing file is to be overwriten without warning
test_regmap.write_html("output/rif_test.html")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
test_regmap.write_verilog_package("output/test_pkg.sv") # pkg for enum definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")

// ----------------------------------------------------------------------------
// interface definition of generic AMBA APB bus
// This intended to support AMBA APB v2.0 (aka APB4)
// (previous version in AMBA 3 APB v1.0 = APB3)
// - APB4 is adding PSTRB to support byte enable on write
// - APB4 is adding PPROT signal (not supported here)
// PCLK, PRESETn is handled outside the interface
// ----------------------------------------------------------------------------

interface IF_APB_BUS(input logic clk);

   // Address, Data
   logic [31:0]      PADDR;
   logic [31:0]      PWDATA;
   logic [31:0]      PRDATA;

   // master control
   logic             PSEL;
   logic             PENABLE;
   logic             PWRITE;
   logic [3:0]       PSTRB; // byte enable : 0 for LSB, 3 for MSB

   // slave control
   logic             PREADY;
   logic             PSLVERR;



   // Master Side
   //***************************************
   modport master (
      output PADDR,
      output PWDATA,
      output PSEL,
      output PENABLE,
      output PWRITE,
      output PSTRB,

      input  PREADY,
      input  PRDATA,
      input  PSLVERR
   );

   // Slave Side
   //***************************************
   modport slave (
      input PADDR,
      input PWDATA,
      input PSEL,
      input PENABLE,
      input PWRITE,
      input PSTRB,

      output  PREADY,
      output  PRDATA,
      output  PSLVERR
  );

`ifndef SYNTHESIS
   // Used for SVA checker
   modport monitor(
      // input clk,

      // Master driven
      input PADDR,
      input PWDATA,
      input PSEL,
      input PENABLE,
      input PWRITE,
      input PSTRB,

      //Slave driven
      input  PREADY,
      input  PRDATA,
      input  PSLVERR
  );
`endif

endinterface // IF_APB_BUS
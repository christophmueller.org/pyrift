// ----------------------------------------------------------------------------
// Pyrift library to implement clock domain crossing at APB level
// This file: pyrift_apb_bridge_cdc.sv is provided as addon of pyrift project
// There is no copyright requirement, and can be re-used as see fit by user
// Please use it "AS IS"
// This file is not covered by GPL license of pyrift project
// --
// Pyrift library to implement apb bridge between 2 different clock domain
// This make no assumption of relative clock frequency (fast to slow or slow to fast)
// It is using brute force dual flop resync on both req/ack protocol
// Can be used as help to apb rif generated with pyrift when system and user hw
// are not synchronous.
// TODO: verification (no CDC analysis tool run on this bridge yet)
// Basic simulation with test master and test slave, random frequency on cpu and periph
// (frequency range 1 to 10 on both side) clock phase randomly exercised
// ----------------------------------------------------------------------------

`default_nettype none

module pyrift_apb_bridge_cdc (
   input var logic  rst_n             , // Async active low reset
   input var logic  ck_cpu_side_i     , // clock used on master APB on cpu side
   input var logic  ck_periph_side_i  , // clock used on slave APB on peripheral side
   // 2 APB used, one on each side of the bridge
   IF_APB_BUS.slave  if_apb_cpu_side   ,
   IF_APB_BUS.master if_apb_periph_side
);

logic request_cpu; // APB cycle request on cpu side, cpu clock domain
                   // Set on PSEL detect for a new cycle
                   // Reset when Ack resync in cpu clock is detected
                   // This is single bit domain crossing signal to be resync in periph domain

logic request_meta; // request_cpu first resync in periph clock domain
                    // can be meta-stable
logic request_periph; // request_cpu second resync in periph clock domain
                      // Usable in fsm of periph side

logic ack_periph; // APB cycle ack on periph side, periph clock domain
                  // Set on request detection for a new cycle
                  // Reset when cycle complete
                  // This is single bit domain crossing signal to be resync in cpu domain
logic ack_meta; // ack_periph first resync in cpu clock domain
                // can be meta-stable
logic ack_cpu; // ack_periph second resync in cpu clock domain
               // Usable in fsm of cpu side

// ----------------------------------------------------------------------------

// control signal from cpu side that are used in periph side
// These signals are static through out full cycle
// Just forward to periph side, path to periph clock domain are false path
logic [31:0] paddr_cpu_to_periph_static;
logic [31:0] pwdata_cpu_to_periph_static;
logic        pwrite_cpu_to_periph_static;
logic [3:0]  pstrb_cpu_to_periph_static;

// ----------------------------------------------------------------------------

// control signal from periph side that are used in cpu side
// These signals are static after periph cycle is complete
// when the cpu side is using these values
// Just forward to cpu side, path to cpu clock domain are false path
logic [31:0] rdata_periph_to_cpu_static;
logic        slverr_periph_to_cpu_static;

// ----------------------------------------------------------------------------
//     ######  ########  ##     ##     ######  ##        #######   ######  ##    ##
//    ##    ## ##     ## ##     ##    ##    ## ##       ##     ## ##    ## ##   ##
//    ##       ##     ## ##     ##    ##       ##       ##     ## ##       ##  ##
//    ##       ########  ##     ##    ##       ##       ##     ## ##       #####
//    ##       ##        ##     ##    ##       ##       ##     ## ##       ##  ##
//    ##    ## ##        ##     ##    ##    ## ##       ##     ## ##    ## ##   ##
//     ######  ##         #######      ######  ########  #######   ######  ##    ##
// ----------------------------------------------------------------------------

always_ff @(posedge ck_cpu_side_i or negedge rst_n) begin : proc_request_cpu
   if(~rst_n) begin
      request_cpu <= 0;
   end else begin
      if(if_apb_cpu_side.PSEL == 1 && if_apb_cpu_side.PENABLE==0) begin
         // First period in cycle : APB PSEL active, PENABLE to come in next period
         request_cpu <= !request_cpu; // Invert so the transient is the trig event to cross CDC
      end
   end
end

// ack resync in cpu domain : 2 flop, single bit resync to cross periph-->cpu
always_ff @(posedge ck_cpu_side_i or negedge rst_n) begin : proc_ack_resync
   if(~rst_n) begin
      ack_meta <= 0;
      ack_cpu <= 0;
   end else begin
      ack_meta <= ack_periph;
      ack_cpu <= ack_meta;
   end
end

// APB cpu ready generation
// activate ready when :
// - cycle is on-going : PSEL && PEN
// - ack with cdc resync is matching the request_cpu
assign if_apb_cpu_side.PREADY =
   if_apb_cpu_side.PSEL == 1 &&
   if_apb_cpu_side.PENABLE==1 &&
   (request_cpu == ack_cpu);

// ----------------------------------------------------------------------------

// CPU apb PRDATA generation
assign if_apb_cpu_side.PRDATA = rdata_periph_to_cpu_static;

// CPU apb PSLVERR generation
assign if_apb_cpu_side.PSLVERR = slverr_periph_to_cpu_static;

assign paddr_cpu_to_periph_static = if_apb_cpu_side.PADDR;
assign pwdata_cpu_to_periph_static = if_apb_cpu_side.PWDATA;
assign pwrite_cpu_to_periph_static = if_apb_cpu_side.PWRITE;
assign pstrb_cpu_to_periph_static = if_apb_cpu_side.PSTRB;

// ----------------------------------------------------------------------------
//    ########  ######## ########  #### ########  ##     ##     ######  ##        #######   ######  ##    ##
//    ##     ## ##       ##     ##  ##  ##     ## ##     ##    ##    ## ##       ##     ## ##    ## ##   ##
//    ##     ## ##       ##     ##  ##  ##     ## ##     ##    ##       ##       ##     ## ##       ##  ##
//    ########  ######   ########   ##  ########  #########    ##       ##       ##     ## ##       #####
//    ##        ##       ##   ##    ##  ##        ##     ##    ##       ##       ##     ## ##       ##  ##
//    ##        ##       ##    ##   ##  ##        ##     ##    ##    ## ##       ##     ## ##    ## ##   ##
//    ##        ######## ##     ## #### ##        ##     ##     ######  ########  #######   ######  ##    ##
// ----------------------------------------------------------------------------


// request resync in periph domain : 2 flop, single bit resync to cross cpu-->periph
always_ff @(posedge ck_periph_side_i or negedge rst_n) begin : proc_request_resync
   if(~rst_n) begin
      request_meta <= 0;
      request_periph <= 0;
   end else begin
      request_meta <= request_cpu;
      request_periph <= request_meta;
   end
end

// end of cycle result sampling : RDATA, PSLVERR
always_ff @(posedge ck_periph_side_i or negedge rst_n) begin : proc_periph_eoc
   if(~rst_n) begin
      rdata_periph_to_cpu_static <= 0;
      slverr_periph_to_cpu_static <= 0;
   end else begin
      if(if_apb_periph_side.PSEL && if_apb_periph_side.PENABLE && if_apb_periph_side.PREADY) begin
         // Last period in cycle : ready from peripheral detected, sample rdata, pslverr for CDC
         rdata_periph_to_cpu_static <= if_apb_periph_side.PRDATA;
         slverr_periph_to_cpu_static <= if_apb_periph_side.PSLVERR;
      end
   end
end

// the ack of request at periph clock domain
// The ack value is changed just after the READY completion of APB cycle on periph
// The ack value is copy of request signal
always_ff @(posedge ck_periph_side_i or negedge rst_n) begin : proc_ack_periph
   if(~rst_n) begin
      ack_periph <= 0;
   end else begin
      if(if_apb_periph_side.PSEL && if_apb_periph_side.PENABLE && if_apb_periph_side.PREADY) begin
         // Last period in cycle : send ack signal for CDC
         ack_periph <= request_periph;
      end
   end
end

// the apb cycle sequence at periph clock domain
// The request/ack transition overlap is used to PSEL
assign if_apb_periph_side.PSEL = (request_periph ^ ack_periph);
always_ff @(posedge ck_periph_side_i or negedge rst_n) begin : proc_apb_periph
   if(~rst_n) begin
      if_apb_periph_side.PENABLE <= 0;
   end else begin
      if (if_apb_periph_side.PSEL && !if_apb_periph_side.PENABLE) begin
         // set at cycle start
         if_apb_periph_side.PENABLE <= 1;
      end else if (if_apb_periph_side.PREADY) begin
         // reset at cycle end (ready)
         if_apb_periph_side.PENABLE <= 0;
      end
   end
end

// direct re-used of signal from CPU side to drive control of periph side
// This is intended direct connect through cross clock domain
// Control are toggling outside to full APB cycle : these are false path
assign if_apb_periph_side.PADDR  = paddr_cpu_to_periph_static;
assign if_apb_periph_side.PWDATA = pwdata_cpu_to_periph_static;
assign if_apb_periph_side.PWRITE = pwrite_cpu_to_periph_static;
assign if_apb_periph_side.PSTRB  = pstrb_cpu_to_periph_static;

// ----------------------------------------------------------------------------

endmodule // pyrift_apb_bridge_cdc

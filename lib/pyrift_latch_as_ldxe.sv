// ---------------------------------------------------------------------------- -
// Pyrift library to implement latch as LDCE/LDPE in Xilinx fpga
// This achive a low level mapping of latch template to match Xilinx LDCE/LDPE
// Workaround to limitation of vivado that fail to get timing clean mapping of LDCE/LDPE
// THis module support bus width D-->Q
// ----------------------------------------------------------------------------

`default_nettype none

module pyrift_latch_as_ldxe #(
   parameter WIDTH = 1,
   parameter RESET_VALUE = 0 // reset value input
) (
   input  var logic             RESET, // Async reset : active high
   input  var logic             G  , // Latch gating : active high
   input  var logic             GE , // Latch gating enable : active high
   input  var logic [WIDTH-1:0] D, // data input
   output var logic [WIDTH-1:0] Q  // data output
);

genvar I;
generate
   for (I = 0; I < WIDTH; I++) begin : latch
      if(RESET_VALUE[I]) begin : latch1
         pyrift_ldpe i_pyrift_lpce (
            .PRE(RESET), // reset value is 1
            .G(G),
            .GE(GE),
            .D(D[I]),
            .Q(Q[I])
         );
      end else begin : latch0
         pyrift_ldce i_pyrift_ldce (
            .CLR(RESET), // reset value is 0
            .G(G),
            .GE(GE),
            .D(D[I]),
            .Q(Q[I])
         );
      end
   end
endgenerate

endmodule // pyrift_latch_as_ldxe

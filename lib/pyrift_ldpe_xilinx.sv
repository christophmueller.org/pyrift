// ---------------------------------------------------------------------------- -
// Pyrift library to implement latch as LDPE
// This achive a low level mapping of latch template to match Xilinx LDPE
// Workaround to limitation of vivado that fail to get timing clean mapping of LDPE
// This module is 1 bit width LDPE (interface compatible with Xilinx LDPE primitive)
// Can be used for vivado synthesis flow, or simulation if Xilinx unisim library are available
// In simulation flow or IC synthesis, should be replaced by pyrift_ldpe simulation model
// ----------------------------------------------------------------------------

`default_nettype none

module pyrift_ldpe (
   input  var logic PRE, // Async reset : active high
   input  var logic G  , // Latch gating : active high
   input  var logic GE , // Latch gating enable : active high
   input  var logic D  , // data input
   output var logic Q    // data output
);

// This is mapping to xilinx LDPE primitive, should be for vivado synthesis, or simulation if unisim library are available
LDPE i_ldpe (
   .PRE(PRE),
   .G(G),
   .GE(GE),
   .D(D),
   .Q(Q)
);

endmodule // pyrift_ldpe

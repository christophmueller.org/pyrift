// ---------------------------------------------------------------------------- -
// Pyrift library to implement latch as LDCE
// This achive a low level mapping of latch template to match Xilinx LDCE
// Workaround to limitation of vivado that fail to get timing clean mapping of LDCE
// This module is 1 bit width LDCE (interface compatible with Xilinx LDCE primitive)
// Can be used for vivado synthesis flow, or simulation if Xilinx unisim library are available
// In simulation flow or IC synthesis, should be replaced by pyrift_ldce simulation model
// ----------------------------------------------------------------------------

`default_nettype none

module pyrift_ldce (
   input  var logic CLR, // Async reset : active high
   input  var logic G  , // Latch gating : active high
   input  var logic GE , // Latch gating enable : active high
   input  var logic D  , // data input
   output var logic Q    // data output
);

// This is mapping to xilinx LDCE primitive, should be for vivado synthesis, or simulation if unisim library are available
LDCE i_ldce (
   .CLR(CLR),
   .G(G),
   .GE(GE),
   .D(D),
   .Q(Q)
);

endmodule // pyrift_ldce

// ---------------------------------------------------------------------------- -
// Pyrift library to implement latch as LDPE
// This achive a low level mapping of latch template to match Xilinx LDPE
// Workaround to limitation of vivado that fail to get timing clean mapping of LDPE
// This module is 1 bit width LDPE (interface compatible with Xilinx LDPE primitive)
// Can be used for simulation or IC synthesis
// In Xilinx fpga vivado flow, should be replaced by using directly Xilinx primitive : LDPE
// ----------------------------------------------------------------------------

`default_nettype none

module pyrift_ldpe (
   input  var logic PRE, // Async reset : active high
   input  var logic G  , // Latch gating : active high
   input  var logic GE , // Latch gating enable : active high
   input  var logic D  , // data input
   output var logic Q    // data output
);

// behavioural model for latch bit cell
always_latch begin : proc_ldpe
   if(PRE) begin
      Q <= 'h1;
   end else begin
      // /!\ latch is transparent if G==1 (masked by gate enable : GE==1)
      if (G==1 && GE==1) begin
         Q <= D;
      end
    end
end

endmodule // pyrift_ldpe

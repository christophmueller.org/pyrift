# ----------------------------------------------------------------------------
# Copyright 2019 Regis Cattenoz, regis@ip666.org
#
# This file is part of pyrift : https://gitlab.com/regisc/pyrift
#
# pyrift is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pyrift is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pyrift.  If not, see <https://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------------
# File: pyrift_apb.py
# Python Register InterFace Translation
# ----------------------------------------------------------------------------

# convert APB signal to generic signal used to decode RIF access

# ----------------------------------------------------------------------------


def host_apb_to_verilog():
    verilog = ""
    verilog += (
        "// APB processing to generic host interfacing\n"
        "//\n"
        "\n"
        "logic apb_setup;\n"
        "logic apb_access;\n"
        "logic host_access_wr;\n"
        # write enable is provided as byte enable or bit mask
        "logic [3:0] host_access_wr_be;\n"
        "logic [31:0] host_access_wr_bmask;\n"

        "logic host_access_rd;\n"
        "logic host_access_ready;\n"
        "logic [31:0] rif_rdata;\n"
        "logic [31:0] sw_wrdata;\n"
        "logic [31:0] host_addr;\n"
        "\n"
        "assign if_apb.PREADY = host_access_ready; // Wait state APB handling\n"
        "                                          // for external register access\n"
        "                                          // either a read or a write\n"
        "assign if_apb.PSLVERR = 0; // No error supported so far\n"
        "\n"
        "assign apb_setup = if_apb.PSEL && !if_apb.PENABLE;\n"
        "assign apb_access = if_apb.PSEL && if_apb.PENABLE;\n"
        "assign host_addr = if_apb.PADDR;\n"
        "assign sw_wrdata = if_apb.PWDATA;\n"
        "assign host_access_wr = apb_access && if_apb.PWRITE; // Simple IF, use APB access phase for host\n"
        "assign host_access_wr_be = {4{host_access_wr}} & if_apb.PSTRB ; // byte enable usage defined per register/field\n"
        "assign host_access_wr_bmask = { {8{host_access_wr_be[3]}}, {8{host_access_wr_be[2]}},\n"
        "                                {8{host_access_wr_be[1]}}, {8{host_access_wr_be[0]}} } ; // bitmask enable usage defined per register/field\n"
        "assign host_access_rd = apb_access && !if_apb.PWRITE; // Simple IF, use APB access phase for host\n"
        "\n"
        "// 32 bit only access, PSTRB are ignored\n"
        "\n"
    )
    return verilog

def host_apb_read_to_verilog():
    verilog = ""
    verilog += (
        "// APB processing to generic host interfacing : rdata to host APB\n"
        "//\n"
        "\n"
        # mux all read register such as : rm_register_map_test__reg_R1_swrdata, ...
        "always_comb begin : proc_apb_rdata\n"
        "   if(rif_access_swrd) begin\n"
        "      if_apb.PRDATA = rif_rdata;\n"
        "   end else begin\n"
        "      if_apb.PRDATA = 0;\n"
        "   end\n"
        "end\n"
        "\n"
    )
    return verilog

def host_apb_unwrap_port(with_cdc=False):
    """
        Define apb port for unwrap module port (no interface allowed)
    """
    verilog = ""
    verilog += f"   // unwrap module, APB IO port definition\n"
    verilog += f"   // APB used to control all peripherals (synchronous to {'apb_cdc_clk' if with_cdc else 'clk'})\n"
    if with_cdc:
        verilog += f"   input  var logic apb_cdc_clk,\n"
        verilog += f"\n"
    verilog += f"   input  var logic        psel,\n"
    verilog += f"   input  var logic        penable,\n"
    verilog += f"   input  var logic        pwrite,\n"
    verilog += f"   input  var logic [31:0] paddr,\n"
    verilog += f"   input  var logic [31:0] pwdata,\n"
    verilog += f"   input  var logic  [3:0] pstrb,\n"
    verilog += f"\n"
    verilog += f"   output var logic [31:0] prdata,\n"
    verilog += f"   output var logic        pslverr,\n"
    verilog += f"   output var logic        pready\n" # no comma as APB is last port of module
    verilog += f"\n"
    return verilog

def host_apb_unwrap_connect(with_cdc=False):
    """
        Connect apb port for unwrap module port to apb actual interface
        Code is part of the body of unwrap module
    """
    apb_cdc_ext = '_cdc' if with_cdc else ''
    verilog = ""
    verilog += f"// APB connection of unwrap ports to APB interface\n"
    verilog += f"//\n"
    verilog += f"assign if_apb{apb_cdc_ext}.PADDR = paddr;\n"
    verilog += f"assign if_apb{apb_cdc_ext}.PSEL = psel;\n"
    verilog += f"assign if_apb{apb_cdc_ext}.PENABLE = penable;\n"
    verilog += f"assign if_apb{apb_cdc_ext}.PWDATA = pwdata;\n"
    verilog += f"assign if_apb{apb_cdc_ext}.PWRITE = pwrite;\n"
    verilog += f"assign if_apb{apb_cdc_ext}.PSTRB = pstrb;\n"
    verilog += f"//\n"
    verilog += f"assign pready  = if_apb{apb_cdc_ext}.PREADY;\n"
    verilog += f"assign pslverr = if_apb{apb_cdc_ext}.PSLVERR;\n"
    verilog += f"assign prdata  = if_apb{apb_cdc_ext}.PRDATA;\n"
    verilog += f"\n"

    if with_cdc:
        verilog += f"//  apb_cdc_clk is connected directly to instanciated module\n"
        verilog += f"\n"
    return verilog


# ----------------------------------------------------------------------------

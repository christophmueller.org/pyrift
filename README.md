################################################
# pyrift : Python Register InterFace Translate
################################################

Pyrift is a collection of python script to help generating  
Systemverilog hardware description of Register InterFace (RIF).
Generated rtl connect to APB (Advanced Peripheral Bus from AMBA)

Supported features are inspired from SystemRDL.
This includes readonly/writeonly (either hw or sw side), 
or mode like "write one clear".

Collect register in a regmap (with addressing table generation for register)  
Register is a collection of fields (with bit range) within the 32 bit width of register.

Pyrift object are python class constructed in user scripts.
Then the script generated following files:
- Systemverilog rtl implementation of register, host bus interface
- Systemverilog interface between RIF and user hardware
- systemverilog package definition (for enum type)
- C header file (help software access to generated RIF)
- Html documentation
- Uvm reg model
 
# Status/Roadmap

This code is provided as GPL licence.
It is available as is. This is still work in progress.
Generated code is still experimental for several features.
If you use it for real hardware project, you better have strong testing
in your environment.
It is starting to be really usable, and saved me a lot of time in hand writing
verilog code for RIF.
I already get generated code running in both simulation and fpga platform.  

Please consider the user script API as subject to change.
I plan to finetune Field/Register attribute, contructor parameter as see fit 
to improve the overall functionality.  
Upcoming may have collateral damage to user script, and finetuning may be needed.
The user API stability will be considered as important criteria to reach v1.0.
Now is only around v0.2, so be aware of risk for your user scripts.

Pyrift feature is improving according to my needs for the moment.
Should you have need requirement, ideas, ... let me know.
I will see what I can do to improve the tool. 

Documentation is planned to be added in wiki (very minimum for the moment).

Sample user code in test directory (very minimum for the moment).
